using Fijo.Infrastructure.DesignPattern.Repository;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Settings;
using Newtonsoft.Json;

namespace Fijo.X2OnlineMod.Repositories {
	public class XOnlineJsonSettings : SingletonRepositoryBase<JsonSerializerSettings>, IJsonSettings {
		#region Overrides of SingletonRepositoryBase<JsonSerializerSettings>
		protected override JsonSerializerSettings Create() {
			return new JsonSerializerSettings
			{
				TypeNameHandling = TypeNameHandling.Auto
			};
		}
		#endregion
	}
}