using System.Collections.Generic;
using System.IO;
using System.Linq;
using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.Infrastructure.DesignPattern.Repository.Collection;
using Fijo.X2OnlineMod.Interfaces;
using Fijo.XGame.Dto;

namespace Fijo.X2OnlineMod.Repositories {
	public class InputRepository : CollectionRepositoryBase<string[]> {
		private readonly IInputFileService _inputFileService;

		public InputRepository(IInputFileService inputFileService) {
			_inputFileService = inputFileService;
		}

		#region Overrides of CollectionRepositoryBase<string[]>
		protected override IEnumerable<string[]> GetAll() {
			return _inputFileService.GetDefaultFiles().Select(x => {
				var content = File.ReadAllLines(x);
				File.Delete(x);
				return content;
			});
		}
		#endregion
	}
}