using System;
using System.Collections.Generic;
using System.Linq;
using Fijo.Infrastructure.DesignPattern.Events;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic;

namespace Fijo.X2OnlineMod.Services {
	public class EventStack : IEventStack {
		private readonly IOut _out;
		private readonly IDictionary<Type, Stack<IEvent>> _content = new Dictionary<Type, Stack<IEvent>>();

		public EventStack(IOut @out) {
			_out = @out;
		}

		#region Implementation of IEventStack
		public void Push(Type eventType, IEvent @event) {
			GetStackFor(eventType).Push(@event);
		}

		public void Push<T>(T @event) where T : IEvent {
			Push(typeof (T), @event);
		}

		public bool TryPop<T>(out T @event) where T : IEvent {
			var stack = GetStackFor(typeof(T));
			lock(stack) return _out.Generic(out @event,	stack.Any(), () => (T) stack.Pop());
		}

		public bool TryPop(Type eventType, out IEvent @event) {
			var stack = GetStackFor(eventType);
			lock(stack) return _out.Generic(out @event, stack.Any(), stack.Pop);
		}

		public bool Any(Type eventType) {
			return GetStackFor(eventType).Any();
		}

		public bool Any<T>() {
			return Any(typeof (T));
		}
		#endregion

		private Stack<IEvent> GetStackFor(Type eventType) {
			lock(_content)
				return _content.GetOrCreate(eventType);
		}
	}
}