using System;
using Fijo.Infrastructure.DesignPattern.Events;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.Infrastructure.Documentation.Enums;
using JetBrains.Annotations;

namespace Fijo.X2OnlineMod.Services {
	[ThreadSafety(ThreadSafety.Save)]
	public interface IEventStack {
		void Push([NotNull] Type @eventType, [NotNull] IEvent @event);
		void Push<T>([NotNull] T @event) where T : IEvent;
		bool TryPop<T>([NotNull] out T @event) where T : IEvent;
		bool TryPop([NotNull] Type @eventType, [NotNull] out IEvent @event);
		bool Any([NotNull] Type @eventType);
		bool Any<T>();
	}
}