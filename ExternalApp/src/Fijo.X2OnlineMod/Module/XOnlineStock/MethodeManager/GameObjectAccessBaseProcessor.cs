using System;
using Fijo.X2OnlineMod.Dto;
using FijoCore.Infrastructure.LightContrib.Module.MethodeManagers.InterruptibleMethodeManager.Interface;

namespace Fijo.X2OnlineMod {
	public class GameObjectAccessBaseProcessor : IInterruptibleMethode<GameObject> {
		protected readonly IGameObjectAccessProcessor GameObjectAcessProcessor;
		protected readonly Action<IGameObjectAccessProcessor, GameObject> ProcessAction;

		public GameObjectAccessBaseProcessor(IGameObjectAccessProcessor gameObjectAcessProcessor, Action<IGameObjectAccessProcessor, GameObject> processAction) {
			GameObjectAcessProcessor = gameObjectAcessProcessor;
			ProcessAction = processAction;
		}
		#region Implementation of IInterruptibleMethode<GameObject>
		public virtual bool Process(GameObject source) {
			var isAffected = IsAffected(source);
			if(isAffected) InternalProcess(source);
			return isAffected;
		}

		protected void InternalProcess(GameObject source) {
			ProcessAction(GameObjectAcessProcessor, source);
		}

		private bool IsAffected(GameObject source) {
			return GameObjectAcessProcessor.TargetedGameObjectTypes.Contains(source.GetType());
		}
		#endregion
	}
}