using Fijo.X2OnlineMod.Dto;
using FijoCore.Infrastructure.LightContrib.Module.MethodeManagers.InterruptibleMethodeManager.Interface;

namespace Fijo.X2OnlineMod {
	public interface IInteruptableMethodeManagerFactory {
		IInterruptibleMethodeManager<GameObject> CreateForGet();
		IInterruptibleMethodeManager<GameObject> CreateForAdd();
		IInterruptibleMethodeManager<GameObject> CreateForRemove();
	}
}