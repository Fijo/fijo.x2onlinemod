using Fijo.X2OnlineMod.Dto;
using FijoCore.Infrastructure.LightContrib.Module.MethodeManagers.InterruptibleMethodeManager;

namespace Fijo.X2OnlineMod {
	public class GameObjectInterruptibleMethodeManager : InterruptibleMethodeManager<GameObject> {}
}