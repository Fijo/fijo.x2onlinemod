using System;
using System.Collections.Generic;
using System.Linq;
using Fijo.X2OnlineMod.Dto;
using FijoCore.Infrastructure.LightContrib.Module.MethodeManagers.InterruptibleMethodeManager.Interface;

namespace Fijo.X2OnlineMod {
	public class InteruptableMethodeManagerFactory : IInteruptableMethodeManagerFactory {
		private readonly IList<IGameObjectAccessProcessor> _gameObjectAcessProcessor;

		public InteruptableMethodeManagerFactory(IGameObjectAccessProcessor[] gameObjectAcessProcessor) {
			_gameObjectAcessProcessor = gameObjectAcessProcessor;
		}

		#region Implementation of IInteruptableMethodeManagerFactory
		public IInterruptibleMethodeManager<GameObject> CreateForGet() {
			return Create((processor, gameObject) => processor.Get(gameObject));
		}

		public IInterruptibleMethodeManager<GameObject> CreateForAdd() {
			return Create((processor, gameObject) => processor.Add(gameObject));
		}

		public IInterruptibleMethodeManager<GameObject> CreateForRemove() {
			return Create((processor, gameObject) => processor.Remove(gameObject));
		}

		private IInterruptibleMethodeManager<GameObject> Create(Action<IGameObjectAccessProcessor, GameObject> processAction) {
			var interruptibleMethodeManager = new GameObjectInterruptibleMethodeManager();
			interruptibleMethodeManager.AddMethodes(_gameObjectAcessProcessor.AsParallel()
				                                        .Select(x => CreateGameObjectAccessBaseProcessor(x, processAction)));
			return interruptibleMethodeManager;
		}

		private GameObjectAccessBaseProcessor CreateGameObjectAccessBaseProcessor(IGameObjectAccessProcessor x, Action<IGameObjectAccessProcessor, GameObject> processAction) {
			return new GameObjectAccessBaseProcessor(x, processAction);
		}
		#endregion
	}
}