using System.Collections.Generic;
using System.Linq;
using Fijo.Infrastructure.DesignPattern.Stock;
using Fijo.X2OnlineMod.Dto;
using Fijo.X2OnlineMod.Repositories;
using FijoCore.Data.Database.MySql.Interfaces;
using FijoCore.Infrastructure.LightContrib.Enums;
using FijoCore.Infrastructure.LightContrib.Module.Serialization;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Settings;

namespace Fijo.X2OnlineMod {
	public class XOnlineStock<T> : StockBase<T> where T : GameObject {
		private readonly IMySqlQueryService _mySqlQueryService;
		private readonly ISerializationFacade _serializationFacade;
		private readonly IJsonSettings _jsonSettings = new XOnlineJsonSettings();

		public XOnlineStock(IMySqlQueryService mySqlQueryService, ISerializationFacade serializationFacade) {
			_mySqlQueryService = mySqlQueryService;
			_serializationFacade = serializationFacade;
		}

		#region Overrides of StockBase<Ship>
		public override void Add(T item) {
			_mySqlQueryService.Insert(string.Format(@"INSERT INTO ships (ship_id, ship_content) VALUES (""{0}"", ""{1}"") ON DUPLICATE KEY UPDATE ship_content = ""{1}""",
			                                        EscapeSlashes(item.OnlineId),
			                                        EscapeSlashes(_serializationFacade.Serialize(SerializationFormat.JSON, item, SerializationFormatting.Default, serializationSettings: _jsonSettings))));
		}

		private string EscapeSlashes(string value) {
			return value.Replace("\"", "\\\"");
		}

		public override IEnumerable<T> GetAll() {
			return GetQuerryResult().Select(x => _serializationFacade.Deserialize<T>(SerializationFormat.JSON, x, serializationSettings: _jsonSettings));
		}

		private IEnumerable<string> GetQuerryResult() {
			var reader = _mySqlQueryService.Query("SELECT ship_content FROM ships");
			while (reader.Read()) {
				yield return (string) reader.GetValue(0);
			}
			reader.Close();
		}

		public override void Remove(T item) {
			throw new System.NotImplementedException();
		}
		#endregion
	}
}