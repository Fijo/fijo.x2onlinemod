using System;
using System.Collections.Generic;
using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.X2OnlineMod.Dto;
using Fijo.XGame.Script.Enums;

namespace Fijo.X2OnlineMod {
	public class ShipAccessProcessor : IGameObjectAccessProcessor {
		private readonly XOnlineConfiguration _xOnlineConfiguration;
		private readonly ISet<string> _knownShipOnlineIds = new HashSet<string>();

		public ShipAccessProcessor(IRepository<XOnlineConfiguration> xOnlineConfigurationRepository) {
			_xOnlineConfiguration = xOnlineConfigurationRepository.Get();
			TargetedGameObjectTypes = new List<Type> {typeof (Ship)};
		}

		#region Implementation of IGameObjectAccessProcessor
		public IList<Type> TargetedGameObjectTypes { get; private set; }

		public void Get(GameObject gameObject) {
			var ship = (Ship) gameObject;
			var race = _xOnlineConfiguration.Auth.Race;
			if(ship.Race == race) ship.Race = Race.Player;
			ship.IsNew = _knownShipOnlineIds.Add(ship.OnlineId);
		}

		public void Add(GameObject gameObject) {
			var ship = (Ship) gameObject;
			ship.LastUpdate = DateTime.Now;
			if(ship.Race == Race.Player) ship.Race = _xOnlineConfiguration.Auth.Race;
		}

		public void Remove(GameObject gameObject) {}
		#endregion
	}
}