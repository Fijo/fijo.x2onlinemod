using System;
using System.Collections.Generic;
using Fijo.X2OnlineMod.Dto;
using JetBrains.Annotations;

namespace Fijo.X2OnlineMod {
	public interface IGameObjectAccessProcessor {
		[NotNull] IList<Type> TargetedGameObjectTypes { get; }
		void Get(GameObject gameObject);
		void Add(GameObject gameObject);
		void Remove(GameObject gameObject);
	}
}