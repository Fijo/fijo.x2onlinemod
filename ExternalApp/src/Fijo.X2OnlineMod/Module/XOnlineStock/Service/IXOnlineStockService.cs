using System.Collections.Generic;
using Fijo.X2OnlineMod.Dto;
using JetBrains.Annotations;

namespace Fijo.X2OnlineMod {
	public interface IXOnlineStockService<T> where T : GameObject {
		[PublicAPI]
		void Remove([NotNull] T item);
		[PublicAPI]
		void Add([NotNull] T item);
		[PublicAPI, NotNull]
		IEnumerable<T> GetAll();
	}
}