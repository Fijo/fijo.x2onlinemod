using System.Collections.Generic;
using System.Linq;
using Fijo.Infrastructure.DesignPattern.Stock;
using Fijo.X2OnlineMod.Dto;
using FijoCore.Infrastructure.LightContrib.Module.MethodeManagers.InterruptibleMethodeManager.Interface;

namespace Fijo.X2OnlineMod {
	public class XOnlineStockService<T> : IXOnlineStockService<T> where T : GameObject {
		private readonly IStock<T> _shipStock;
		private readonly IInterruptibleMethodeManager<GameObject> _getInteruptableMethodeManager;
		private readonly IInterruptibleMethodeManager<GameObject> _addInteruptableMethodeManager;
		private readonly IInterruptibleMethodeManager<GameObject> _removeInteruptableMethodeManager;

		public XOnlineStockService(IStock<T> shipStock, IInteruptableMethodeManagerFactory interuptableMethodeManagerFactory) {
			_shipStock = shipStock;
			_getInteruptableMethodeManager = interuptableMethodeManagerFactory.CreateForGet();
			_addInteruptableMethodeManager = interuptableMethodeManagerFactory.CreateForAdd();
			_removeInteruptableMethodeManager = interuptableMethodeManagerFactory.CreateForRemove();
		}

		#region Implementation of IXOnlineStockService
		public void Remove(T item) {
			_removeInteruptableMethodeManager.Process(item);
			_shipStock.Remove(item);
		}

		public void Add(T item) {
			_addInteruptableMethodeManager.Process(item);
			_shipStock.Add(item);
		}

		public IEnumerable<T> GetAll() {
			return _shipStock.GetAll().AsParallel().Select(x => {
				                                               _getInteruptableMethodeManager.Process(x);
				                                               return x;
			                                               });
		}
		#endregion
	}
}