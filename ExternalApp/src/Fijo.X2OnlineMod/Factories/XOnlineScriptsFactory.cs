using System;
using System.Collections.Generic;
using System.Linq;
using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.Infrastructure.DesignPattern.Stock;
using Fijo.X2OnlineMod.Commands;
using Fijo.X2OnlineMod.Dto;
using Fijo.X2OnlineMod.Enums;
using Fijo.X2OnlineMod.Enums.Constants;
using Fijo.X2OnlineMod.Services;
using Fijo.XGame.Script.Commands;
using Fijo.XGame.Script.Dto;
using Fijo.XGame.Script.Enums;
using Fijo.XGame.Script.Interfaces;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;

namespace Fijo.X2OnlineMod {
	public class XOnlineScriptsFactory : IXOnlineScriptsFactory {
		private readonly IScriptFactory _scriptFactory;
		private readonly IScriptService _scriptService;
		private readonly IScriptUtil _scriptUtil;
		private readonly IXOnlineStockService<Ship> _xOnlineStockService;
		private readonly IEventService _eventService;
		private readonly XOnlineConfiguration _xOnlineConfiguration;

		public XOnlineScriptsFactory(IScriptFactory scriptFactory, IScriptService scriptService, IScriptUtil scriptUtil, IXOnlineStockService<Ship> xOnlineStockService, IRepository<XOnlineConfiguration> xOnlineConfiguration, IEventService eventService) {
			_scriptFactory = scriptFactory;
			_scriptService = scriptService;
			_scriptUtil = scriptUtil;
			_xOnlineStockService = xOnlineStockService;
			_eventService = eventService;
			_xOnlineConfiguration = xOnlineConfiguration.Get();
		}

		public Script CreateInitPlayerShipScript() {
			var script = _scriptFactory.CreateScript(DynScriptNames.InitPlayerShip, "testDesc", 1);
			var playerShipVar = _scriptService.DefineVariable(script, "playerShip");
			var shipVar = _scriptService.DefineVariable(script, "ship");
			var sectorVar = _scriptService.DefineVariable(script, "sector");
			var targetPosVar = _scriptService.DefineVariable(script, "targetPos");
			var rotationVar = _scriptService.DefineVariable(script, "rotation");
			var waresVar = _scriptService.DefineVariable(script, "wares");
			var wareVar = _scriptService.DefineVariable(script, "ware");
			var wareTypeVar = _scriptService.DefineVariable(script, "wareType");
			var taskIdVar = _scriptService.DefineVariable(script, "taskId");
			
			var ships = _xOnlineStockService.GetAll().ToList();
			var playerShip = ships.SingleOrDefault(x => x.Race == Race.Player && x.IsActive);
			if(playerShip == null) {
				ships.Add(playerShip = GetNewShip());
				ships.Add(new Ship
				          {
					          Name = "Buster",
					          Position = new SpacePosition(-10000, 2000, 0, 0, 0, 0),
					          Sector = new SectorCoordinate(1, 2),
					          Type = ShipType.ArgonBuster,
					          Race = Race.Player,
					          OnlineId = Guid.NewGuid().ToString(),
					          Wares = new Wares(new List<Ware>
								{
									new Ware(new WareType(WareMainType.Tech, (int) Tech.BoostExtension), 1),
									new Ware(new WareType(WareMainType.Tech, (int) Tech.Freightscanner), 1),
									new Ware(new WareType(WareMainType.Tech, (int) Tech.MineralScanner), 1),
									new Ware(new WareType(WareMainType.Tech, (int) Tech.DuplexScanner), 1),
									new Ware(new WareType(WareMainType.Tech, (int) Tech.Cargobayextension), 5),
									new Ware(new WareType(WareMainType.Tech, (int) Tech.EngineTuning), 20),
									new Ware(new WareType(WareMainType.Shields, (int) Shields.Shield5MW), 3),
									new Ware(new WareType(WareMainType.Weapons, (int) Weapons.BetaParticleAcceleratorCannon), 2),
									new Ware(new WareType(WareMainType.Rockets, (int) Rockets.DragonflyMissile), 10),
								}),
								DockedTo = playerShip
				          });
			}
			ships.ForEach(ship => _scriptUtil.CreateShip(script, shipVar, sectorVar, waresVar, wareVar, wareTypeVar, targetPosVar, ship));

			_scriptService.AddCommand(script, new GetShipByIdCommand {Arg1 = playerShipVar, Arg2 = playerShip.OnlineId});
			AdjustPlayerShipPosition(script, targetPosVar, rotationVar, sectorVar, playerShip, taskIdVar, playerShipVar);
			return script;
		}

		private Ship GetNewShip() {
			return new Ship
			{
				Name = "Oddy",
				IsActive = true,
				Position = new SpacePosition(-10000, 0, 0, 0, 0, 0),
				Sector = new SectorCoordinate(1, 2),
				Type = ShipType.ParanidOdysseus,
				Race = Race.Player,
				OnlineId = Guid.NewGuid().ToString(),
				Wares = new Wares(new List<Ware>
				{
					new Ware(new WareType(WareMainType.Tech, (int) Tech.BoostExtension), 1),
					new Ware(new WareType(WareMainType.Tech, (int) Tech.Freightscanner), 1),
					new Ware(new WareType(WareMainType.Tech, (int) Tech.MineralScanner), 1),
					new Ware(new WareType(WareMainType.Tech, (int) Tech.DuplexScanner), 1),
					new Ware(new WareType(WareMainType.Tech, (int) Tech.RudderOptimisation), 10),
					new Ware(new WareType(WareMainType.Tech, (int) Tech.Cargobayextension), 5),
					new Ware(new WareType(WareMainType.Tech, (int) Tech.EngineTuning), 8),
					new Ware(new WareType(WareMainType.Shields, (int) Shields.Shield125MW), 6),
					new Ware(new WareType(WareMainType.Weapons, (int) Weapons.BetaPhasedShockwaveGenerator), 18),
					new Ware(new WareType(WareMainType.Tech, (int) Tech.NavigationRelaySatellite), 8),
					new Ware(new WareType(WareMainType.Energy, (int) Energy.EnergyCells), 10),
					new Ware(new WareType(WareMainType.Bio, (int) Bio.Argnubeef), 4),
					new Ware(new WareType(WareMainType.Bio, (int) Bio.Plankton), 2),
					new Ware(new WareType(WareMainType.Bio, (int) Bio.SojaBeans), 1),
					new Ware(new WareType(WareMainType.Bio, (int) Bio.DelexianWheat), 10),
					new Ware(new WareType(WareMainType.Natural, (int) Natural.Water), 10),
					new Ware(new WareType(WareMainType.Bio, (int) Bio.ScruffinFruits), 10),
					new Ware(new WareType(WareMainType.Minerals, (int) Minerals.Ore), 10),
					new Ware(new WareType(WareMainType.Minerals, (int) Minerals.SiliconWafers), 10),
					new Ware(new WareType(WareMainType.Rockets, (int) Rockets.HornetMissile), 10),
					new Ware(new WareType(WareMainType.Rockets, (int) Rockets.DragonflyMissile), 10),
				})
			};
		}

		private void AdjustPlayerShipPosition(Script script, Variable targetPosVar, Variable rotationVar, Variable sectorVar, Ship playerShip, Variable taskIdVar, Variable playerShipVar) {
			_scriptUtil.SetPositionVar(script, targetPosVar, sectorVar, playerShip.Sector, playerShip.Position);
			_scriptService.AddCommand(script, new RandomValueCommand {Arg1 = taskIdVar, Arg2 = 9990, Arg3 = 10000});
			
			CreateRotationArray(script, rotationVar, playerShip);
			_scriptService.AddCommand(script, new StartTaskCommand {Arg1 = playerShipVar, Arg2 = ScriptsNames.InitPlayerShipPos, Arg3 = taskIdVar, Arg4 = 0, Arg5 = targetPosVar, Arg6 = rotationVar});
			_scriptService.AddCommand(script, new ReturnCommand {Arg1 = playerShipVar});
		}

		private void CreateRotationArray(Script script, Variable rotationVar, Ship playerShip) {
			var rotation = playerShip.Position.Rotation;
			_scriptService.AddCommand(script, new CreateArrayCommand {Arg1 = rotationVar, Arg2 = 3});
			_scriptService.AddCommand(script, new SetArrayPositionToValueCommand {Arg1 = rotationVar, Arg2 = 0, Arg3 = rotation.X});
			_scriptService.AddCommand(script, new SetArrayPositionToValueCommand {Arg1 = rotationVar, Arg2 = 1, Arg3 = rotation.Y});
			_scriptService.AddCommand(script, new SetArrayPositionToValueCommand {Arg1 = rotationVar, Arg2 = 2, Arg3 = rotation.Z});
		}

		public Script CreateUpdateShipsScript(ICollection<Ship> ships) {
			var script = _scriptFactory.CreateScript(DynScriptNames.UpdateShips, "testDesc", 1);
			var shipVar = _scriptService.DefineVariable(script, "ship");
			var targetPosVar = _scriptService.DefineVariable(script, "targetPos");
			var sectorVar = _scriptService.DefineVariable(script, "sector");
			var waresVar = _scriptService.DefineVariable(script, "wares");
			var wareVar = _scriptService.DefineVariable(script, "ware");
			var wareTypeVar = _scriptService.DefineVariable(script, "wareType");

			AdjustOnlineId(script, shipVar);
			ships.Where(x => x.IsNew).ForEach(ship => _scriptUtil.CreateShip(script, shipVar, sectorVar, waresVar, wareVar, wareTypeVar, targetPosVar, ship));
			ships.Where(x => !x.IsNew).ForEach(ship => _scriptUtil.UpdateShip(script, shipVar, targetPosVar, sectorVar, waresVar, wareVar, wareTypeVar, ship));
			SerializeState(script);
			return script;
		}

		private void SerializeState(Script script) {
			_scriptService.AddCommand(script, new SerializeStateCommand {Arg1 = InputStateTypes.Import, Arg2 = InputState.Finished});
		}

		private void AdjustOnlineId(Script script, Variable shipVar) {
			_eventService.Process<AdjustOnlineIdEvent>(@event => _scriptUtil.AdjustOnlineId(script, shipVar, @event));
		}
	}
}