using Fijo.X2OnlineMod.Commands;
using Fijo.X2OnlineMod.Dto;
using Fijo.X2OnlineMod.Services;
using Fijo.XGame.Script.Commands;
using Fijo.XGame.Script.Dto;
using Fijo.XGame.Script.Enums;
using Fijo.XGame.Script.Interfaces;

namespace Fijo.X2OnlineMod {
	public class ScriptUtil : IScriptUtil {
		private readonly IScriptService _scriptService;

		public ScriptUtil(IScriptService scriptService) {
			_scriptService = scriptService;
		}

		public void CreateShip(Script script, Variable shipVar, ShipType shipType, Race race, Variable sectorVar, Position position) {
			_scriptService.AddCommand(script, new CreateShipCommand {Arg1 = shipVar, Arg2 = shipType, Arg3 = race, Arg4 = sectorVar, Arg5 = position});
		}
		
		public void CreateShip(Script script, Variable shipVar, Variable sectorVar, Variable waresVar, Variable wareVar, Variable wareTypeVar, Variable haveHangerVar, Ship ship) {
			GetSectorToVar(script, sectorVar, ship.Sector);
			CreateShip(script, shipVar, ship.Type, ship.Race, sectorVar, GetInitialPosition(ship));
			SetShipName(script, shipVar, ship.Name);
			SetWare(script, shipVar, waresVar, wareVar, wareTypeVar, ship.Wares);
			var dockedTo = ship.DockedTo;
			if(dockedTo != null) IntoStation(script, shipVar, haveHangerVar, dockedTo);
			else SetRotation(script, shipVar, ship.Position.Rotation);
			_scriptService.AddCommand(script, new SetShipIdCommand {Arg1 = shipVar, Arg2 = ship.OnlineId});
		}

		private Position GetInitialPosition(Ship ship) {
			var dockedTo = ship.DockedTo;
			if(dockedTo == null) return ship.Position;
			var spacePosition = dockedTo.Position;
			return new Position(spacePosition.X + 3000, spacePosition.Y, spacePosition.Z);
		}

		private void GetSectorToVar(Script script, Variable sectorVar, SectorCoordinate sectorCoordinate) {
			_scriptService.AddCommand(script, new GetSectorByCoordinatesCommand {Arg1 = sectorVar, Arg2 = sectorCoordinate});
		}

		public void UpdateShip(Script script, Variable shipVar, Variable targetPosVar, Variable sectorVar, Variable waresVar, Variable wareVar, Variable wareTypeVar, Ship ship) {
			_scriptService.AddCommand(script, new GetShipByIdCommand {Arg1 = shipVar, Arg2 = ship.OnlineId});
			SetShipName(script, shipVar, ship.Name);
			SetWare(script, shipVar, waresVar, wareVar, wareTypeVar, ship.Wares);
			var dockedTo = ship.DockedTo;
			if(dockedTo != null) IntoStation(script, shipVar, targetPosVar, dockedTo);
			else {
				SetRotation(script, shipVar, ship.Position.Rotation);
				SetPositionVar(script, targetPosVar, sectorVar, ship.Sector, ship.Position);
				_scriptService.AddCommand(script, new JumpToPositionCommand {Arg1 = shipVar, Arg2 = targetPosVar});
			}
		}

		private void IntoStation(Script script, Variable shipVar, Variable targetPosVar, IHaveHanger dockedTo) {
			_scriptService.AddCommand(script, new GetShipByIdCommand {Arg1 = targetPosVar, Arg2 = dockedTo.OnlineId});
			_scriptService.AddCommand(script, new JumpToStationCommand {Arg1 = shipVar, Arg2 = targetPosVar});
		}

		public void SetPositionVar(Script script, Variable variableToSet, Variable sectorVar, SectorCoordinate sectorCoordinate, SpacePosition spacePosition) {
			_scriptService.AddCommand(script, new CreateArrayCommand {Arg1 = variableToSet, Arg2 = 4});
			GetSectorToVar(script, sectorVar, sectorCoordinate);
			SetPositionToArray(script, variableToSet, spacePosition);
			_scriptService.AddCommand(script, new SetArrayPositionToVariableCommand {Arg1 = variableToSet, Arg2 = 3, Arg3 = sectorVar});
		}

		private void SetPositionToArray(Script script, Variable variableToSet, SpacePosition spacePosition) {
			_scriptService.AddCommand(script, new SetArrayPositionToValueCommand {Arg1 = variableToSet, Arg2 = 0, Arg3 = spacePosition.X});
			_scriptService.AddCommand(script, new SetArrayPositionToValueCommand {Arg1 = variableToSet, Arg2 = 1, Arg3 = spacePosition.Y});
			_scriptService.AddCommand(script, new SetArrayPositionToValueCommand {Arg1 = variableToSet, Arg2 = 2, Arg3 = spacePosition.Z});
		}
		
		private void SetRotation(Script script, Variable shipVar, Position rotation) {
			_scriptService.AddCommand(script, new SetRotationCommand {Arg1 = shipVar, Arg2 = rotation.X, Arg3 = rotation.Y, Arg4 = rotation.Z});
		}
		
		private void SetShipName(Script script, Variable shipVar, string name) {
			_scriptService.AddCommand(script, new SetNameCommand {Arg1 = shipVar, Arg2 = name});
		}
		
		#region Ware
		private void SetWare(Script script, Variable objectSourceVar, Variable waresVar, Variable wareVar, Variable wareTypeVar, Wares wares) {
			CreateWaresArray(script, waresVar, wareVar, wareTypeVar, wares);
			_scriptService.AddCommand(script, new SetWaresCommand {Arg1 = objectSourceVar, Arg2 = waresVar});
		}

		private void CreateWaresArray(Script script, Variable waresVar, Variable wareVar, Variable wareTypeVar, Wares wares) {
			var content = wares.Content;
			_scriptService.AddCommand(script, new CreateArrayCommand {Arg1 = waresVar, Arg2 = content.Count});
			var index = 0;
			foreach (var ware in content) {
				CreateWareArray(script, wareVar, wareTypeVar, ware);
				_scriptService.AddCommand(script, new SetArrayPositionToVariableCommand {Arg1 = waresVar, Arg2 = index++, Arg3 = wareVar});
			}
		}

		private void CreateWareArray(Script script, Variable wareVar, Variable wareTypeVar, Ware ware) {
			_scriptService.AddCommand(script, new CreateArrayCommand {Arg1 = wareVar, Arg2 = 2});
			GetWareType(script, wareTypeVar, ware);
			_scriptService.AddCommand(script, new SetArrayPositionToVariableCommand {Arg1 = wareVar, Arg2 = 0, Arg3 = wareTypeVar});
			_scriptService.AddCommand(script, new SetArrayPositionToValueCommand {Arg1 = wareVar, Arg2 = 1, Arg3 = ware.Amount});
		}

		private void GetWareType(Script script, Variable wareTypeVar, Ware ware) {
			_scriptService.AddCommand(script, new GetWareByTypes {Arg1 = wareTypeVar, Arg2 = ware.Type});
		}
		#endregion
		
		public void AdjustOnlineId(Script script, Variable shipVar, AdjustOnlineIdEvent @event) {
			_scriptService.AddCommand(script, new GetShipByInGameIdCommand {Arg1 = shipVar, Arg2 = @event.InGameShipId});
			_scriptService.AddCommand(script, new SetShipIdCommand {Arg1 = shipVar, Arg2 = @event.OnlineId});
		}
	}
}