using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.Infrastructure.DesignPattern.Stock;
using Fijo.X2OnlineMod.Dto;
using Fijo.X2OnlineMod.Enums;
using Fijo.X2OnlineMod.Enums.Constants;
using Fijo.X2OnlineMod.Interfaces;
using Fijo.XGame.Dto;
using Fijo.XGame.Interfaces;
using Fijo.XGame.Script.Enums;
using Fijo.XGame.Script.Interfaces;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;

namespace Fijo.X2OnlineMod.Services {
	public class GameService : IGameService {
		private readonly IGameFactory _gameFactory;
		private readonly GameEnvironment _gameEnvironment;
		private readonly IGameEnvironmentService _gameEnvironmentService;
		private readonly IGameAccessSerivce _gameAccessService;
		private readonly IScriptFactory _scriptFactory;
		private readonly IScriptStoreService _scriptStoreService;
		private readonly IScriptService _scriptService;
		private readonly IScriptUtil _scriptUtil;
		private readonly IXOnlineStockService<Ship> _xOnlineStockService;
		private readonly IXOnlineScriptsFactory _xOnlineScriptsFactory;
		private readonly IRepository<IEnumerable<GameObject>> _inputProvider;
		private readonly IInputFileService _inputFileService;
		private readonly IEventStack _eventStack;
		private readonly XOnlineConfiguration _xOnlineConfiguration;
		private readonly Game _game;

		public GameService(IGameFactory gameFactory, IRepository<GameEnvironment> gameEnvironment, IGameEnvironmentService gameEnvironmentService, IGameAccessSerivce gameAccessService, IScriptFactory scriptFactory, IScriptStoreService scriptStoreService, IRepository<XOnlineConfiguration> xOnlineConfiguration, IScriptService scriptService, IScriptUtil scriptUtil, IXOnlineStockService<Ship> xOnlineStockService, IXOnlineScriptsFactory xOnlineScriptsFactory, IRepository<IEnumerable<GameObject>> inputProvider, IInputFileService inputFileService, IEventStack eventStack) {
			_gameFactory = gameFactory;
			_gameEnvironment = gameEnvironment.Get();
			_gameEnvironmentService = gameEnvironmentService;
			_gameAccessService = gameAccessService;
			_scriptFactory = scriptFactory;
			_scriptStoreService = scriptStoreService;
			_scriptService = scriptService;
			_scriptUtil = scriptUtil;
			_xOnlineStockService = xOnlineStockService;
			_xOnlineScriptsFactory = xOnlineScriptsFactory;
			_inputProvider = inputProvider;
			_inputFileService = inputFileService;
			_eventStack = eventStack;
			_xOnlineConfiguration = xOnlineConfiguration.Get();
			_game = _gameFactory.Create(_gameEnvironment);
		}

		private void InitResources() {
			_inputFileService.SetState(InputStateTypes.Export, InputState.NotStarted);
			InitScripts();
		}

		private void InitScripts() {
			CleanUpScripts();
			_scriptStoreService.Store(_xOnlineScriptsFactory.CreateInitPlayerShipScript());
			CopyInitSerializeScript();
		}

		private void CleanUpScripts() {
			_gameEnvironmentService.CleanUpScript(_gameEnvironment, DynScriptNames.UpdateShips);
		}

		private void CopyInitSerializeScript() {
			_gameEnvironmentService.IncludeResources(_gameEnvironment, ResourceFolderNames.General);
			_gameEnvironmentService.IncludeResources(_gameEnvironment, ResourceFolderNames.XOnline);
		}

		public void Start() {
			InitResources();
			_gameAccessService.Start(_game);
			_gameAccessService.StartGame(_game, 3);
			_gameAccessService.UnlockScriptEditor(_game);
			Begin();
		}

		public void Begin() {
			var lastUpdate = DateTime.MinValue;
			while(true) {
				Thread.Sleep(_xOnlineConfiguration.SyncIntervall);
				Sync(lastUpdate);
				lastUpdate = DateTime.Now;
			}
		}

		private void Sync(DateTime lastUpdate) {
			Save();
			Update(lastUpdate);
			_inputFileService.SetState(InputStateTypes.Export, InputState.NotStarted);
		}

		private void Update(DateTime lastUpdate) {
			var toUpdate = _xOnlineStockService.GetAll().Where(x => x.Race != Race.Player && x.LastUpdate > lastUpdate).Execute();
			if (toUpdate.None() && !_eventStack.Any<AdjustOnlineIdEvent>()) return;
			_inputFileService.SetState(InputStateTypes.Import, InputState.NotFinised);
			_scriptStoreService.Store(_xOnlineScriptsFactory.CreateUpdateShipsScript(toUpdate));
			lock (_game) {
				_gameAccessService.GotoScriptEditor(_game);
				_gameAccessService.LoadScript(_game, DynScriptNames.UpdateShips, CultureInfo.InvariantCulture);
				_gameAccessService.RunScript(_game, DynScriptNames.UpdateShips);
				RetryUntilSuccessfull();
				_gameAccessService.DeleteScript(_game, DynScriptNames.UpdateShips);
				_gameAccessService.GotoShipCockpit(_game);
			}
		}

		private void RetryUntilSuccessfull() {
			var check = 0;
			while (_inputFileService.GetState(InputStateTypes.Import) != InputState.Finished) {
				Thread.Sleep(50);
				if (check++ != 100) continue;
				_gameAccessService.GotoScriptEditor(_game);
				_gameAccessService.RunScript(_game, DynScriptNames.UpdateShips);
				check = 0;
			}
		}

		#region Save
		private void Save() {
			while(_inputFileService.GetState(InputStateTypes.Export) != InputState.Finished) Thread.Sleep(50);
			var ships = GetShipsToSave().Execute();
			ships.Where(HasNoOnlineId).ForEach(AdjustOnlineId);
			ships.ForEach(_xOnlineStockService.Add);
		}

		private bool HasNoOnlineId(IHaveOnlineId x) {
			return string.IsNullOrEmpty(x.OnlineId) || x.OnlineId == "null";
		}

		private void AdjustOnlineId(Ship ship) {
			ship.OnlineId = Guid.NewGuid().ToString();
			_eventStack.Push(GetAdjustOnlineIdEvent(ship));
		}

		private AdjustOnlineIdEvent GetAdjustOnlineIdEvent(Ship ship) {
			return new AdjustOnlineIdEvent {InGameShipId = ship.InGameId, OnlineId = ship.OnlineId};
		}

		private IEnumerable<Ship> GetShipsToSave() {
			return _inputProvider.Get().OfType<Ship>().Where(x => x.Race == Race.Player);
		}
		#endregion
	}
}