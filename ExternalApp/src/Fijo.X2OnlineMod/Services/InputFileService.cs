using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.X2OnlineMod.Enums;
using Fijo.X2OnlineMod.Interfaces;
using Fijo.XGame.Dto;
using FijoCore.Infrastructure.LightContrib.Extentions.IList.Generic;

namespace Fijo.X2OnlineMod.Services {
	public class InputFileService : IInputFileService {
		private readonly GameEnvironment _gameEnvironment;
		private const int PossibleMaxFileNumber = 99999;
		private const int StateStartFileNumber = UsedMaxFileNumber + 1;
		private const int UsedMaxFileNumber = PossibleMaxFileNumber - 10;

		public InputFileService(IRepository<GameEnvironment> gameEnvironmentRepository) {
			_gameEnvironment = gameEnvironmentRepository.Get();
		}
		
		public IEnumerable<string> GetDefaultFiles() {
			return Enumerable.Range(0, UsedMaxFileNumber).Select(GetFileName).TakeWhile(File.Exists);
		}

		#region StateFile
		public InputState GetState(InputStateTypes stateTypes) {
			var lastOrDefault = GetStateContent(stateTypes);
			return !string.IsNullOrEmpty(lastOrDefault)
				       ? (InputState) int.Parse(lastOrDefault)
				       : default(InputState);
		}

		private string GetStateContent(InputStateTypes stateTypes) {
			return GetAllLines(GetStateFileName(stateTypes));
		}

		private string GetAllLines(string path) {
			return File.Exists(path) ? File.ReadAllLines(path).LastOrDefault() : string.Empty;
		}

		public void SetState(InputStateTypes stateTypes, InputState state) {
			var path = GetStateFileName(stateTypes);
			File.WriteAllLines(path, new[] {((int) state).ToString(CultureInfo.InvariantCulture)});
		}

		private string GetStateFileName(InputStateTypes stateTypes) {
			var number = StateStartFileNumber + ((int) stateTypes);
			Debug.Assert(number <= PossibleMaxFileNumber);
			var path = GetFileName(number);
			return path;
		}
		#endregion

		private string GetNumberAtPos(int num) {
			return string.Format("{0:00000}", num);
		}

		private string GetFileName(int index) {
			return Path.Combine(_gameEnvironment.GameDirForRead, string.Format("log{0}.txt", GetNumberAtPos(index)));
		}
	}
}