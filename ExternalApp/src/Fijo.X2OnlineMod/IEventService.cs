using System;
using Fijo.Infrastructure.DesignPattern.Events;
using JetBrains.Annotations;

namespace Fijo.X2OnlineMod.Services {
	public interface IEventService {
		void Process<T>([NotNull] Action<T> @eventProcessor) where T : IEvent;
		void Process([NotNull] Type @eventType, [NotNull] Action<IEvent> @eventProcessor);
	}
}