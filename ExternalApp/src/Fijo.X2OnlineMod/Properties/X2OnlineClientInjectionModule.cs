using System.Collections.Generic;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Controller;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Provider;
using Fijo.Infrastructure.DesignPattern.Mapping;
using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.Infrastructure.DesignPattern.Stock;
using Fijo.X2OnlineMod.Dto;
using Fijo.X2OnlineMod.Interfaces;
using Fijo.X2OnlineMod.Mappings;
using Fijo.X2OnlineMod.Mappings.GameObject;
using Fijo.X2OnlineMod.Provider;
using Fijo.X2OnlineMod.Provider.Algorithm;
using Fijo.X2OnlineMod.Repositories;
using Fijo.X2OnlineMod.Services;
using Fijo.XGame.Script.Enums;
using FijoCore.Data.Database.MySql.Properties;
using FijoCore.Infrastructure.DependencyInjection.Extentions.IKernel;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Base;
using FijoCore.Infrastructure.LightContrib.Module.MethodeManagers.InterruptibleMethodeManager.Interface;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;

namespace Fijo.X2OnlineMod.Properties {
	public class X2OnlineClientInjectionModule : ExtendedNinjectModule {
		public override void AddModule(IKernel kernel) {
			kernel.Load(new XGameScriptInjectionModule());
			kernel.Load(new MySqlInjectionModule());
		}

		public override void OnLoad(IKernel kernel) {
			kernel.Bind<IFinder<IGameObjectMapping, GameObjectType>>().To<Finder<IGameObjectMapping, GameObjectType>>().InSingletonScope();
			kernel.Bind<IAlgorithmProvider<IGameObjectMapping, GameObjectType>>().To<GameObjectMappingAlgorithmProvider>().InSingletonScope();
			kernel.Bind<IGameObjectMapping>().To<ShipMapping>().InSingletonScope();
			kernel.Bind<IGameObjectMapping>().To<WareShipBundleMapping>().InSingletonScope();

			kernel.Bind<IMapping<string[], GameObject>>().To<GameObjectsMapping>().InSingletonScope();

			kernel.Bind<IInputFileService>().To<InputFileService>().InSingletonScope();

			kernel.Bind<IRepository<IEnumerable<string[]>>>().To<InputRepository>().InSingletonScope();
			kernel.Bind<IRepository<IEnumerable<GameObject>>>().To<InputProvider>().InSingletonScope();

			kernel.Bind<IScriptUtil>().To<ScriptUtil>().InSingletonScope();

			kernel.Bind<IRepository<XOnlineConfiguration>>().To<XOnlineConfigurationProvider>().InSingletonScope();

			kernel.Bind<IGameService>().To<GameService>().InSingletonScope();
			kernel.Bind<IXOnlineScriptsFactory>().To<XOnlineScriptsFactory>().InSingletonScope();

			BindStockFor<Ship>(kernel);

			kernel.Bind<IInteruptableMethodeManagerFactory>().To<InteruptableMethodeManagerFactory>().InSingletonScope();
			kernel.Bind<IInterruptibleMethodeManager<GameObject>>().To<GameObjectInterruptibleMethodeManager>().InSingletonScope();

			kernel.Bind<IGameObjectAccessProcessor>().To<ShipAccessProcessor>().InSingletonScope();

			kernel.Bind<IEventStack>().To<EventStack>().InSingletonScope();
			kernel.Bind<IEventService>().To<EventService>().InSingletonScope();
		}

		private void BindStockFor<T>(IKernel kernel) where T : GameObject {
			kernel.Bind<IStock<T>>().To<XOnlineStock<T>>().InSingletonScope();
			kernel.Bind<IXOnlineStockService<T>>().To<XOnlineStockService<T>>().InSingletonScope();
		}
	}
}