using FijoCore.Infrastructure.DependencyInjection.InitKernel.Init;

namespace Fijo.X2OnlineMod.Properties {
	public class InternalInitKernel : ExtendedInitKernel
	{
		public override void PreInit()
		{
			LoadModules(new X2OnlineClientInjectionModule());
		}
	}
}