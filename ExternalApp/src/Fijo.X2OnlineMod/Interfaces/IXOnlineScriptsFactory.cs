using System.Collections.Generic;
using Fijo.X2OnlineMod.Dto;
using Fijo.XGame.Script.Dto;

namespace Fijo.X2OnlineMod {
	public interface IXOnlineScriptsFactory {
		Script CreateInitPlayerShipScript();
		Script CreateUpdateShipsScript(ICollection<Ship> ships);
	}
}