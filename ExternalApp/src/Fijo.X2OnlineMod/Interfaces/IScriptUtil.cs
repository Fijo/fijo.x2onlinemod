using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.X2OnlineMod.Dto;
using Fijo.X2OnlineMod.Enums;
using Fijo.X2OnlineMod.Services;
using Fijo.XGame.Script.Dto;
using Fijo.XGame.Script.Enums;

namespace Fijo.X2OnlineMod {
	public interface IScriptUtil {
		void CreateShip(Script script, Variable shipVar, ShipType shipType, Race race, Variable sectorVar, Position position);
		void CreateShip(Script script, Variable shipVar, Variable sectorVar, Variable waresVar, Variable wareVar, Variable wareTypeVar, Variable haveHangerVar, Ship ship);
		void UpdateShip(Script script, Variable shipVar, Variable targetPosVar, Variable sectorVar, Variable waresVar, Variable wareVar, Variable wareTypeVar, Ship ship);
		void SetPositionVar(Script script, [About("positionVariableToSet")] Variable variableToSet, Variable sectorVar, SectorCoordinate sectorCoordinate, SpacePosition spacePosition);
		void AdjustOnlineId(Script script, Variable shipVar, AdjustOnlineIdEvent @event);
	}
}