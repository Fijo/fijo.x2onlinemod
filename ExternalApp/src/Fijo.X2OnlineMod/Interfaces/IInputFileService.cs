using System.Collections.Generic;
using Fijo.X2OnlineMod.Enums;

namespace Fijo.X2OnlineMod.Interfaces {
	public interface IInputFileService {
		IEnumerable<string> GetDefaultFiles();
		InputState GetState(InputStateTypes stateTypes);
		void SetState(InputStateTypes stateTypes, InputState state);
	}
}