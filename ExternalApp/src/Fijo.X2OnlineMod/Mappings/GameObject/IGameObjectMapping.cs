using Fijo.Infrastructure.DesignPattern.Mapping;
using Fijo.XGame.Script.Enums;

namespace Fijo.X2OnlineMod.Mappings.GameObject {
	public interface IGameObjectMapping : IMapping<string[], Dto.GameObject> {
		GameObjectType Type { get; }
	}
}