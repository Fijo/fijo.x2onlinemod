using System;
using System.Collections.Generic;
using System.Linq;
using Fijo.X2OnlineMod.Dto;
using Fijo.X2OnlineMod.Enums;
using Fijo.XGame.Script.Dto;
using Fijo.XGame.Script.Enums;

namespace Fijo.X2OnlineMod.Mappings.GameObject {
	public class WareShipBundleMapping : IGameObjectMapping {
		public GameObjectType Type { get { return GameObjectType.Ware; } }

		#region Implementation of IMapping<in string[],out GameObject>
		#region MapWareShipBundle
		public Dto.GameObject Map(string[] source) {
			return new WareShipBundle
			{
				ShipId = GetString(source, InputWares.ForId),
				Wares = MapWares(source)
			};
		}

		private string GetString(IList<string> source, InputWares index) {
			return source[(int) index];
		}
		#endregion
		
		#region MapWares
		private Wares MapWares(string[] source) {
			const int contentBegin = (int) InputWares.ContentBegin;
			return GetWares(GetIndexes(source, contentBegin).Select(index => MapWare(source, index)).ToList());
		}

		private Wares GetWares(IList<Ware> wares) {
			return new Wares(wares);
		}

		private IEnumerable<int> GetIndexes(IEnumerable<string> source, int contentBegin) {
			return Enumerable.Range(0, (source.Count() - contentBegin) / 3).Select(x => x * 3 + contentBegin);
		}
		#endregion
		
		#region MapWare
		private Ware MapWare(string[] source, int index) {
			return new Ware(GetWareType(source, index), GetInt(source, InputWare.Amount, index));
		}

		private WareType GetWareType(IList<string> source, int index) {
			return new WareType((WareMainType) GetInt(source, InputWare.WareMainType, index),
			                    GetInt(source, InputWare.WareSubType, index));
		}

		private string GetString(IList<string> source, InputWare index, int startIndex) {
			return source[(int) index + startIndex];
		}

		private int GetInt(IList<string> source, InputWare index, int startIndex) {
			return int.Parse(GetString(source, index, startIndex));
		}
		#endregion
		#endregion
	}
}