using System;
using System.Linq;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Controller;
using Fijo.Infrastructure.DesignPattern.Mapping;
using Fijo.XGame.Script.Enums;

namespace Fijo.X2OnlineMod.Mappings.GameObject {
	public class GameObjectsMapping : IMapping<string[], Dto.GameObject> {
		private readonly IFinder<IGameObjectMapping, GameObjectType> _gameObjectMappingFinder;

		public GameObjectsMapping(IFinder<IGameObjectMapping, GameObjectType> gameObjectMappingFinder) {
			_gameObjectMappingFinder = gameObjectMappingFinder;
		}

		#region Implementation of IMapping<in string[],out Ship>
		public Dto.GameObject Map(string[] source) {
			return _gameObjectMappingFinder.Get(GetObjectType(source)).Map(GetArrayWithoutHead(source));
		}

		private string[] GetArrayWithoutHead(string[] source) {
			var size = source.Count() - 1;
			var newSource = new string[size];
			Array.Copy(source, 1, newSource, 0, size);
			return newSource;
		}

		private GameObjectType GetObjectType(string[] source) {
			return (GameObjectType) Enum.Parse(typeof(GameObjectType), source[0]);
		}
		#endregion
	}
}