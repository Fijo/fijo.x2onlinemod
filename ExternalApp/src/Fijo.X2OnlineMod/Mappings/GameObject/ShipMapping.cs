using System;
using System.Collections.Generic;
using System.Linq;
using Fijo.X2OnlineMod.Dto;
using Fijo.X2OnlineMod.Enums;
using Fijo.XGame.Script.Dto;
using Fijo.XGame.Script.Enums;

namespace Fijo.X2OnlineMod.Mappings.GameObject {
	public class HaveHangerMapping {
		protected void AdjustHaveHanger(IHaveHanger haveHanger, string[] input, int index) {
			haveHanger.LandedShipIds = input.Skip(index).ToList();
		}
	}

	public class ShipMapping : HaveHangerMapping, IGameObjectMapping {
		private readonly int _haveHangerStartIndex = Enum.GetValues(typeof (InputShip)).Cast<InputShip>().Max(x => (int) x) + 1;

		public GameObjectType Type { get { return GameObjectType.Ship; } }

		#region Implementation of IMapping<in string[],out Ship>
		public Dto.GameObject Map(string[] source) {
			var mapShip = MapShip(source);
			AdjustHaveHanger(mapShip, source, _haveHangerStartIndex);
			return mapShip;
		}

		private Ship MapShip(string[] source) {
			return new Ship
			{
				OnlineId = GetString(source, InputShip.OnlineId),
				InGameId = GetString(source, InputShip.InGameShipId),
				Position = new SpacePosition(GetInt(source, InputShip.ShipX), GetInt(source, InputShip.ShipY), GetInt(source, InputShip.ShipZ), GetInt(source, InputShip.RotationAlpha), GetInt(source, InputShip.RotationBeta), GetInt(source, InputShip.RotationGamma)),
				Sector = new SectorCoordinate(GetInt(source, InputShip.SectorX), GetInt(source, InputShip.SectorY)),
				Name = GetString(source, InputShip.ShipName),
				Type = (ShipType) GetInt(source, InputShip.WareSubType), //muss mir aus den standart namen die typen ermitteln und ann n mapping schreiben
				Race = (Race) Enum.Parse(typeof(Race), GetString(source, InputShip.ShipRace).Replace(" ", "")),
				HomebaseId = GetString(source, InputShip.ShipHomebaseId),
				ShipHangerLanded = GetInt(source, InputShip.ShipHangerLanded),
				Hull = GetInt(source, InputShip.ShipNowHull),
				Shield = GetInt(source, InputShip.ShipNowShield),
				Command = GetString(source, InputShip.ShipCommand),
				IsActive = GetBool(source, InputShip.IsActive),
			};
		}

		private string GetString(IList<string> source, InputShip index) {
			return source[(int) index];
		}

		private int GetInt(IList<string> source, InputShip index) {
			return int.Parse(source[(int) index]);
		}
		
		private bool GetBool(IList<string> source, InputShip index) {
			return source[(int) index] == "1";
		}
		#endregion
	}
}