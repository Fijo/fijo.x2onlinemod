using Fijo.X2OnlineMod.Mappings.GameObject;
using Fijo.XGame.Script.Enums;
using FijoCore.Infrastructure.LightContrib.Module.AlgorithmProvider;

namespace Fijo.X2OnlineMod.Provider.Algorithm {
	public class GameObjectMappingAlgorithmProvider : InjectionAlgorithmProvider<IGameObjectMapping, GameObjectType> {
		#region Overrides of InjectionAlgorithmProviderBase<IGameObjectMapping,GameObjectType>
		protected override GameObjectType KeySelector(IGameObjectMapping algorithm) {
			return algorithm.Type;
		}
		#endregion
	}
}