using System.Collections.Generic;
using System.Linq;
using Fijo.Infrastructure.DesignPattern.Mapping;
using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.Infrastructure.DesignPattern.Repository.Collection;
using Fijo.X2OnlineMod.Dto;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic.KeyValuePair;
using FijoCore.Infrastructure.LightContrib.Extentions.IList.Generic;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;

namespace Fijo.X2OnlineMod.Provider {
	public class InputProvider : CollectionRepositoryBase<GameObject> {
		private readonly IRepository<IEnumerable<string[]>> _repository;
		private readonly IMapping<string[], GameObject> _shipMapping;
		private readonly IPairFactory _pairFactory;

		public InputProvider(IRepository<IEnumerable<string[]>> repository, IMapping<string[], GameObject> shipMapping, IPairFactory pairFactory) {
			_repository = repository;
			_shipMapping = shipMapping;
			_pairFactory = pairFactory;
		}

		#region Overrides of CollectionRepositoryBase<Ship>
		protected override IEnumerable<GameObject> GetAll() {
			var gameObjects = GetGameObjects().ToList();
			AdjustWare(gameObjects);
			AdjustDockables(gameObjects);
			return gameObjects;
		}

		private void AdjustDockables(List<GameObject> gameObjects) {
			var haveHangers = gameObjects.OfType<IHaveHanger>()
				.SelectMany(x => x.LandedShipIds.Select(y => _pairFactory.KeyValuePair(y, x)))
				.ToDictionary();
			gameObjects.OfType<IDockable>().ForEach(x => x.DockedTo = haveHangers.TryGet(x.InGameId));
		}

		private void AdjustWare(List<GameObject> gameObjects) {
			var ships = gameObjects.OfType<Ship>().ToDictionary(x => x.InGameId, x => x);
			var wareShipBundles = gameObjects.OfType<WareShipBundle>().ToArray();
			wareShipBundles.ForEach(x => ships[x.ShipId].Wares = x.Wares);
			gameObjects.RemoveRange(wareShipBundles);
		}

		private IEnumerable<GameObject> GetGameObjects() {
			return _repository.Get().Select(_shipMapping.Map);
		}
		#endregion
	}
}