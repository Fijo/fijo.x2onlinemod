using System;
using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.XGame.Script.Enums;
using FijoCore.Infrastructure.LightContrib.Default.Service.CheckName;
using FijoCore.Infrastructure.LightContrib.Module.Configuration;

namespace Fijo.X2OnlineMod {
	public class XOnlineConfigurationProvider : SingletonRepositoryBase<XOnlineConfiguration> {
		private readonly IConfigurationService _configurationService;

		public XOnlineConfigurationProvider(IConfigurationService configurationService) {
			_configurationService = configurationService;
		}

		#region Overrides of SingletonRepositoryBase<XOnlineConfiguration>
		protected override XOnlineConfiguration Create() {
			return new XOnlineConfiguration
			{
				SyncIntervall = _configurationService.Get<int, XOnlineConfiguration>(CN.Get<XOnlineConfiguration, int>(x => x.SyncIntervall)),
				Auth = GetAuth()
			};
		}

		private XOnlineAuth GetAuth() {
			return new XOnlineAuth
			{
				User = _configurationService.Get<string, XOnlineAuth>(CN.Get<XOnlineAuth, string>(x => x.User)),
				Password = _configurationService.Get<string, XOnlineAuth>(CN.Get<XOnlineAuth, string>(x => x.Password)),
				Race = (Race) Enum.Parse(typeof(Race), _configurationService.Get<string, XOnlineAuth>(CN.Get<XOnlineAuth, Race>(x => x.Race)))
			};
		}
		#endregion
	}
}