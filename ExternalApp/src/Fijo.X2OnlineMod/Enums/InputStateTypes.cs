using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.X2OnlineMod.Enums {
	[Desc("Row indexes of the file log99999.txt")]
	[ModificationNote("the indexes are the same as in the x2 export to file script (the numbers are the line numbers +1 of the output file) - only change them if you change the x2 export data script")]
	public enum InputStateTypes {
		[Note("import data into the X game")]
		Import = 1,
		[Note("export data out of the X game")]
		Export = 2
	}
}