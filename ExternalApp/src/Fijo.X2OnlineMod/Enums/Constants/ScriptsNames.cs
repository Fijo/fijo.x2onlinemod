namespace Fijo.X2OnlineMod.Enums.Constants {
	public static class ScriptsNames {
		public const string ExportWareSubTypes = "Fijo.Export.WareSubTypes";
		public const string SetWares = "Fijo.Services.Obj.SetWares";
		public const string ShipById = "Fijo.Store.Ship.Get";
		public const string StationById = "Fijo.Repos.GetStation";
		public const string ShipByInGameId = "Fijo.Repos.GetShip";
		public const string JumpToStation = "Fijo.Services.Move.ToStation";
		public const string JumpToPosition = "Fijo.Services.Move.ToPosition";
		public const string CorrectShip = "io.correct.ship";
		public const string SerializeShip = "Fijo.Services.Ship.Serialize";
		public const string InitPlayerShipPos = "Fijo.Task.InitPlayerShipPos";
		public const string SetShipId = "Fijo.Store.Ship.Set";
		public const string SerializeState = "Fijo.Services.State.Serialize";
	}
}