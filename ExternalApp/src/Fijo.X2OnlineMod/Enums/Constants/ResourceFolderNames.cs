namespace Fijo.X2OnlineMod.Enums.Constants {
	public static class ResourceFolderNames {
		public const string General = "General";
		public const string XOnline = "XOnline";
		public const string Export = "Export";
		public const string Tests = "Tests";
		public const string Tools = "Tools";
	}
}