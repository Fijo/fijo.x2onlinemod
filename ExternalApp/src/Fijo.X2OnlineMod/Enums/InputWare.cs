using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.X2OnlineMod.Enums {
	[Desc("Rowcontents of the files logXXXXX.txt")]
	[ModificationNote("the indexes are the same as in the x2 export to file script (the numbers are the line numbers +1 of the output file) - only change them if you change the x2 export data script")]
	public enum InputWare {
		WareMainType = 0,
		WareSubType = 1,
		Amount = 2
	}
}