using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace Fijo.X2OnlineMod.Enums {
	[Desc("Rowcontents of the file log99999.txt")]
	[ModificationNote("the indexes are the same as in the x2 export to file script (the numbers are the line numbers +1 of the output file) - only change them if you change the x2 export data script"), UsedImplicitly]
	public enum InputState {
		NotFinised = 0,
		Finished = 1,
		NotStarted = 2
	}
}