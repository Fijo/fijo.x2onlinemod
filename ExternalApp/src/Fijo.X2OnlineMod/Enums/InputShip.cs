using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.X2OnlineMod.Enums {
	[Desc("Rowcontents of the files logXXXXX.txt")]
	[ModificationNote("the indexes are the same as in the x2 export to file script (the numbers are the line numbers +1 of the output file) - only change them if you change the x2 export data script")]
	public enum InputShip {
		ShipX = 0,
		ShipY = 1,
		ShipZ = 2,
		ShipName = 3,
		[Desc("WareSubType of the ship (WareMainType is always Ship(7))")]
		WareSubType = 4,
		ShipRace = 5,
		ShipHomebaseId = 6,
		SectorX = 7,
		SectorY = 8,
		ShipHangerLanded = 9,
		ShipNowHull = 10,
		ShipNowShield = 11,
		ShipCommand = 12,
		OnlineId = 13,
		IsActive = 14,
		RotationAlpha = 15,
		RotationBeta = 16,
		RotationGamma = 17,
		InGameShipId = 18
	}
}