using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.X2OnlineMod.Dto;
using Fijo.X2OnlineMod.Enums;
using Fijo.X2OnlineMod.Enums.Constants;
using Fijo.XGame.Script.Commands;
using Fijo.XGame.Script.Dto;
using Fijo.XGame.Script.Enums;
using Fijo.XGame.Script.Enums.Constants;

namespace Fijo.X2OnlineMod.Commands {
	public class CorrectShipCommand : ICommand<string, Variable, ShipType, Race, string> {
		[About("ShipId")]
		public string Arg1 { get; set; }
		[About("TargetPositionArrayVariable")]
		public Variable Arg2 { get; set; }
		[About("ShipType")]
		public ShipType Arg3 { get; set; }
		[About("Race")]
		public Race Arg4 { get; set; }
		[About("ShipName")]
		public string Arg5 { get; set; }

		#region Implementation of ICommand
		public IEnumerable<object> ToObjects() {
			yield return ScriptMethodes.CallScript;
			yield return ScriptsNames.CorrectShip;
			yield return -2147483647;
			yield return 131075;
			yield return ScriptsConstants.ScriptParam;
			yield return 5;
			yield return ScriptsConstants.StringParam;
			yield return Arg1;
			yield return ScriptsConstants.VariableParam;
			yield return Arg2;
			yield return ScriptsConstants.ShipTypeParam;
			yield return Arg3;
			yield return ScriptsConstants.RaceParam;
			yield return Arg4;
			yield return ScriptsConstants.StringParam;
			yield return Arg5;
		}
		#endregion
	}
}