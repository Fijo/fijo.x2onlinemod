using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.X2OnlineMod.Dto;
using Fijo.X2OnlineMod.Enums;
using Fijo.X2OnlineMod.Enums.Constants;
using Fijo.XGame.Script.Commands;
using Fijo.XGame.Script.Dto;
using Fijo.XGame.Script.Enums;
using Fijo.XGame.Script.Enums.Constants;

namespace Fijo.X2OnlineMod.Commands {
	public class GetShipByIdCommand : ICommand<Variable, string> {
		[About("SetToVariable")]
		public Variable Arg1 { get; set; }
		[About("ShipId")]
		public string Arg2 { get; set; }

		#region Implementation of ICommand
		public IEnumerable<object> ToObjects() {
			yield return ScriptMethodes.CallScript;
			yield return ScriptsNames.ShipById;
			yield return Arg1;
			yield return 131075;
			yield return ScriptsConstants.ScriptParam;
			yield return 1;
			yield return ScriptsConstants.StringParam;
			yield return Arg2;
		}
		#endregion
	}
}