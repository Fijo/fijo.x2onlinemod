using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.X2OnlineMod.Dto;
using Fijo.X2OnlineMod.Enums;
using Fijo.X2OnlineMod.Enums.Constants;
using Fijo.XGame.Script.Commands;
using Fijo.XGame.Script.Dto;
using Fijo.XGame.Script.Enums;
using Fijo.XGame.Script.Enums.Constants;

namespace Fijo.X2OnlineMod.Commands {
	public class JumpToPositionCommand : ICommand<Variable, Variable> {
		[About("ShipVariable")]
		public Variable Arg1 { get; set; }
		[About("TargetPositionArrayVariable")]
		public Variable Arg2 { get; set; }

		#region Implementation of ICommand
		public IEnumerable<object> ToObjects() {
			yield return ScriptMethodes.CallScript;
			yield return ScriptsNames.JumpToPosition;
			yield return -2147483647;
			yield return ScriptsConstants.VariableParam;
			yield return Arg1;
			yield return 1;
			yield return ScriptsConstants.VariableParam;
			yield return Arg2;
		}
		#endregion
	}
}