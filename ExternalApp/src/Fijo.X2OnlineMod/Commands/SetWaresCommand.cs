using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.X2OnlineMod.Enums.Constants;
using Fijo.XGame.Script.Commands;
using Fijo.XGame.Script.Dto;
using Fijo.XGame.Script.Enums;
using Fijo.XGame.Script.Enums.Constants;

namespace Fijo.X2OnlineMod.Commands {
	public class SetWaresCommand : ICommand<Variable, Variable> {
		[About("ObjectVariable")]
		public Variable Arg1 { get; set; }
		[About("WaresArray"), Note("an array of an array [wareType, amount]")]
		public Variable Arg2 { get; set; }

		#region Implementation of ICommand
		public IEnumerable<object> ToObjects() {
			yield return ScriptMethodes.CallScript;
			yield return ScriptsNames.SetWares;
			yield return ScriptsConstants.This;
			yield return 131074;
			yield return Arg1;
			yield return 1;
			yield return ScriptsConstants.VariableParam;
			yield return Arg2;
		}
		#endregion
	}
}