using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.X2OnlineMod.Enums.Constants;
using Fijo.XGame.Script.Commands;
using Fijo.XGame.Script.Dto;
using Fijo.XGame.Script.Enums;
using Fijo.XGame.Script.Enums.Constants;

namespace Fijo.X2OnlineMod.Commands {
	public class SetShipIdCommand : ICommand<Variable, string> {
		[About("ShipVariable")]
		public Variable Arg1 { get; set; }
		[About("OnlineId"), Note("although called non ingame id")]
		public string Arg2 { get; set; }

		#region Implementation of ICommand
		public IEnumerable<object> ToObjects() {
			yield return ScriptMethodes.CallScript;
			yield return ScriptsNames.SetShipId;
			yield return -2147483647;
			yield return 131075;
			yield return ScriptsConstants.ScriptParam;
			yield return 2;
			yield return ScriptsConstants.VariableParam;
			yield return Arg1;
			yield return ScriptsConstants.StringParam;
			yield return Arg2;
		}
		#endregion
	}
}