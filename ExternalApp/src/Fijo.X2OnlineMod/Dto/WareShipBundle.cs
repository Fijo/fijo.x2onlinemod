using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.X2OnlineMod.Dto {
	[Dto]
	public class WareShipBundle : GameObject {
		[About("InGameShipId")]
		public string ShipId { get; set; }
		public Wares Wares { get; set; }
	}
}