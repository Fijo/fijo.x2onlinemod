using System;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace Fijo.X2OnlineMod.Dto {
	[Dto, Serializable]
	public abstract class GameObject : IHaveOnlineId {
		#region Implementation of IHaveOnlineId
		public string OnlineId { get; set; }
		#endregion
	}
}