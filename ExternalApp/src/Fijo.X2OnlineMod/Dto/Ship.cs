using System;
using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.XGame.Script.Dto;
using Fijo.XGame.Script.Enums;

namespace Fijo.X2OnlineMod.Dto {
	public interface IHaveOnlineId {
		string OnlineId { get; set; }
	}

	public interface IFlying {
		SpacePosition Position { get; set; }
		SectorCoordinate Sector { get; set; }
	}

	public interface IDockable {
		string InGameId { get; set; }
		IHaveHanger DockedTo { get; set; }
	}

	public interface IHaveHanger : IFlying, IHaveOnlineId {
		[About("LandedShipsInGameIds")]
		IList<string> LandedShipIds { get; set; }
	}

	[Dto, Serializable]
	public class Ship : GameObject, IHaveHanger, IDockable {
		public string InGameId { get; set; }
		public SpacePosition Position { get; set; }
		public SectorCoordinate Sector { get; set; }
		public string Name { get; set; }
		public ShipType Type { get; set; }
		public Race Race { get; set; }
		public string HomebaseId { get; set; }
		public int ShipHangerLanded { get; set; }
		public int Hull { get; set; }
		public int Shield { get; set; }
		// todo make an enum out of this
		public string Command { get; set; }
		public Wares Wares { get; set; }
		public bool IsActive { get; set; }
		public DateTime LastUpdate { get; set; }
		public bool IsNew { get; set; }
		#region Implementation of IHaveHanger
		public IList<string> LandedShipIds { get; set; }
		#endregion
		#region Implementation of IDockable
		public IHaveHanger DockedTo { get; set; }
		#endregion
	}
}