using System;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using Fijo.XGame.Script.Dto;
using Fijo.XGame.Script.Enums;

namespace Fijo.X2OnlineMod.Dto {
	[Dto, Serializable]
	public class Ware : GameObject {
		public WareType Type { get; set; }
		public int Amount { get; set; }

		public Ware(WareType type, int amount) {
			Type = type;
			Amount = amount;
		}
	}
}