namespace Fijo.X2OnlineMod {
	public class XOnlineConfiguration {
		public XOnlineAuth Auth { get; set; }
		public int SyncIntervall { get; set; }
	}
}