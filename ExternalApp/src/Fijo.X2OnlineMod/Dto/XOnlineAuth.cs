using Fijo.X2OnlineMod.Enums;
using Fijo.XGame.Script.Enums;

namespace Fijo.X2OnlineMod {
	public class XOnlineAuth {
		public string User { get; set; }
		public string Password { get; set; }
		public Race Race { get; set; }
	}
}