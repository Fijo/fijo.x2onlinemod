using System;
using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace Fijo.X2OnlineMod.Dto {
	[Dto, Serializable]
	public class Wares : GameObject {
		public IList<Ware> Content { get; set; }

		public Wares(IList<Ware> content) {
			Content = content;
		}
	}
}