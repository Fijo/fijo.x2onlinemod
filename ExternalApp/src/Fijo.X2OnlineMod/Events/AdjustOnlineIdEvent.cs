using Fijo.Infrastructure.DesignPattern.Events;

namespace Fijo.X2OnlineMod.Services {
	public class AdjustOnlineIdEvent : Event {
		public string InGameShipId { get; set; }
		public string OnlineId { get; set; }
	}
}