using System;
using Fijo.Infrastructure.DesignPattern.Events;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.Infrastructure.Documentation.Enums;

namespace Fijo.X2OnlineMod.Services {
	[ThreadSafety(ThreadSafety.Save)]
	public class EventService : IEventService {
		private readonly IEventStack _eventStack;

		public EventService(IEventStack eventStack) {
			_eventStack = eventStack;
		}

		#region Implementation of IEventService
		public void Process<T>(Action<T> eventProcessor) where T : IEvent {
			T @event;
			while (_eventStack.TryPop(out @event)) eventProcessor(@event);
		}

		public void Process(Type eventType, Action<IEvent> eventProcessor) {
			IEvent @event;
			while (_eventStack.TryPop(out @event)) eventProcessor(@event);
		}
		#endregion
	}
}