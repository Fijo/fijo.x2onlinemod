using System.Globalization;
using Fijo.XGame.Dto;

namespace Fijo.XGame.Interfaces {
	public interface IGameAccessSerivce {
		void GotoShipCockpit(Game game);
		void GotoScriptEditor(Game game);
		void RunScript(Game game, string name);
		void DeleteScript(Game game, string name);
		void LoadScript(Game game, string name, CultureInfo cultureInfo);
		void LoadSavegame(Game game, int index);
		void Start(Game game);
		void StartGame(Game game, int index);
		void UnlockScriptEditor(Game game);
		void Exit(Game game);
	}
}