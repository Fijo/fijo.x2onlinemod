using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.XGame.Dto;

namespace Fijo.XGame.Interfaces {
	public interface IGameEnvironmentService {
		int GetScriptPosition(GameEnvironment gameEnvironment, string name);
		void UpdateLoadedScripts(GameEnvironment gameEnvironment);
		void AddLoadedScript(GameEnvironment gameEnvironment, string name);
		void RemoveLoadedScript(GameEnvironment gameEnvironment, string name);
		IEnumerable<string> GetScriptDirs(GameEnvironment gameEnvironment);
		string GetScriptDirForWrite(GameEnvironment gameEnvironment);
		IEnumerable<string> GetGameDirs(GameEnvironment gameEnvironment);
		[Note("Please call this only before starting the game")]
		void InstallScript(GameEnvironment gameEnvironment, string scriptFilePath);
		[Note("Please call this only before starting the game")]
		void CleanUpScript(GameEnvironment gameEnvironment, string scriptName);
		[Note("Please call this only before starting the game")]
		void IncludeResources(GameEnvironment gameEnvironment, string folderName);
	}
}