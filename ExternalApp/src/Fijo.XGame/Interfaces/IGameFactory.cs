using System.Collections.Generic;
using System.Text;
using Fijo.XGame.Dto;

namespace Fijo.XGame.Interfaces {
	public interface IGameFactory {
		GameEnvironment Create(string gameDir, IList<string> additionalVirtualGameDirs, string gameDirForWrite, string gameDirForRead, string x2BinaryRelativePath, Encoding logFileEncoding, Encoding scriptEncoding, IList<string> additionalResourcesFolders, GameConfig config);
		Game Create(GameEnvironment gameEnvironment);
	}
}