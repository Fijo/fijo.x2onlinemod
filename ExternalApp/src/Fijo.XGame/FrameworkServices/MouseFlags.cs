using System;

namespace Fijo.XGame.FrameworkServices {
	[Flags]
	public enum MouseFlags : byte {
		Left = 4,
		Right = 8,
		Middle = 12,
		Up = 1,
		Down = 2,
		Both = Up | Down,
	}
}