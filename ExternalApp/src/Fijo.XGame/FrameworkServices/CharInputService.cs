using System.Globalization;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;

namespace Fijo.XGame.FrameworkServices {
	public class CharInputService : ICharInputService {
		private readonly ICharInputProvider _charInputProvider;
		private readonly IInputService _inputService;
		private readonly IInputHelper _inputHelper;

		public CharInputService(ICharInputProvider charInputProvider, IInputService inputService, IInputHelper inputHelper) {
			_charInputProvider = charInputProvider;
			_inputService = inputService;
			_inputHelper = inputHelper;
		}
		
		#region Implementation of IStringInputService
		public void SendKeys(char key, CultureInfo cultureInfo) {
			var rule = GetRule(key, cultureInfo);
			rule.SendKeys.ForEachAndBetween(SendKey, _inputHelper.WaitBetweenSendInput);
		}

		private void SendKey(SendKey sendKey) {
			_inputService.SendKey(sendKey.KeyCode, sendKey.KeyEventFlags);
		}

		private CharInputRule GetRule(char key, CultureInfo cultureInfo) {
			return _charInputProvider.Get(key, cultureInfo);
		}
		#endregion
	}
}