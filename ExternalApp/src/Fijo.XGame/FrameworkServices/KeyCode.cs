using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.XGame.FrameworkServices {
	public enum KeyCode : short {
		Backspace = 0x08,
		Tab = 0x09,
		Clear = 0x0C,
		[About("RETURN")]
		Enter = 0x0D,
		Shift = 0x10,
		[About("CTRL")]
		Control = 0x11,
		Alt = 0x12,
		Pause = 0x13,
		[About("CAPITAL")]
		CapsLock = 0x14,
		[Desc("Input Method Editor (IME) Kana mode")]
		Kana = 0x15,
		[Desc("IME Junja mode")]
		Junja = 0x17,
		[Desc("IME final mode")]
		Final = 0x18,
		[Desc("IME Hanja mode")]
		Hanja = 0x19,
		[About("ESC")]
		Escape = 0x1B,
		[Desc("IME convert")]
		Convert = 0x1C,
		[Desc("IME nonconvert")]
		Nonconvert = 0x1D,
		[Desc("IME accept")]
		Accept = 0x1E,
		[Desc("IME mode change request")]
		Modechange = 0x1F,
		[About("SPACEBAR")]
		Space = 0x20,
		PageUp = 0x21,
		PageDown = 0x22,
		End = 0x23,
		Home = 0x24,
		Left = 0x25,
		Up = 0x26,
		Right = 0x27,
		Down = 0x28,
		Select = 0x29,
		Print = 0x2A,
		Execute = 0x2B,
		[About("PrintScreen")]
		Snapshot = 0x2C,
		[About("INS")]
		Insert = 0x2D,
		[About("DEL")]
		Delete = 0x2E,
		Help = 0x2F,
		Key0 = 0x30,
		Key1 = 0x31,
		Key2 = 0x32,
		Key3 = 0x33,
		Key4 = 0x34,
		Key5 = 0x35,
		Key6 = 0x36,
		Key7 = 0x37,
		Key8 = 0x38,
		Key9 = 0x39,
		KeyA = 0x41,
		KeyB = 0x42,
		KeyC = 0x43,
		KeyD = 0x44,
		KeyE = 0x45,
		KeyF = 0x46,
		KeyG = 0x47,
		KeyH = 0x48,
		KeyI = 0x49,
		KeyJ = 0x4A,
		KeyK = 0x4B,
		KeyL = 0x4C,
		KeyM = 0x4D,
		KeyN = 0x4E,
		KeyO = 0x4F,
		KeyP = 0x50,
		KeyQ = 0x51,
		KeyR = 0x52,
		KeyS = 0x53,
		KeyT = 0x54,
		KeyU = 0x55,
		KeyV = 0x56,
		KeyW = 0x57,
		KeyX = 0x58,
		KeyY = 0x59,
		KeyZ = 0x5A,
		[Desc("Left Windows key (Microsoft Natural keyboard)")]
		LeftWin = 0x5B,
		[Desc("Right Windows key (Natural keyboard)")]
		RightWin = 0x5C,
		[Desc("Applications key (Natural keyboard)")]
		Apps = 0x5D,
		Sleep = 0x5F,
		Numpad0 = 0x60,
		Numpad1 = 0x61,
		Numpad2 = 0x62,
		Numpad3 = 0x63,
		Numpad4 = 0x64,
		Numpad5 = 0x65,
		Numpad6 = 0x66,
		Numpad7 = 0x67,
		Numpad8 = 0x68,
		Numpad9 = 0x69,
		Multiply = 0x6A,
		Add = 0x6B,
		Separator = 0x6C,
		Subtract = 0x6D,
		Decimal = 0x6E,
		Divide = 0x6F,
		F1 = 0x70,
		F2 = 0x71,
		F3 = 0x72,
		F4 = 0x73,
		F5 = 0x74,
		F6 = 0x75,
		F7 = 0x76,
		F8 = 0x77,
		F9 = 0x78,
		F10 = 0x79,
		F11 = 0x7A,
		F12 = 0x7B,
		F13 = 0x7C,
		F14 = 0x7D,
		F15 = 0x7E,
		F16 = 0x7F,
		F17 = 0x80,
		F18 = 0x81,
		F19 = 0x82,
		F20 = 0x83,
		F21 = 0x84,
		[Desc("F22 key, (PPC only) Key used to lock device.")]
		F22 = 0x85,
		F23 = 0x86,
		F24 = 0x87,
		NumLock = 0x90,
		ScrollLock = 0x91,
		LeftShift = 0xA0,
		RightShift = 0xA1,
		LeftControl = 0xA2,
		RightControl = 0xA3,
		LeftMenu = 0xA4,
		RightMenu = 0xA5,
		[Desc("Windows 2000/XP: Browser Back key")]
		BrowserBack = 0xA6,
		[Desc("Windows 2000/XP: Browser Forward key")]
		BrowserForward = 0xA7,
		[Desc("Windows 2000/XP: Browser Refresh key")]
		BrowserRefresh = 0xA8,
		[Desc("Windows 2000/XP: Browser Stop key")]
		BrowserStop = 0xA9,
		[Desc("Windows 2000/XP: Browser Search key")]
		BrowserSearch = 0xAA,
		[Desc("Windows 2000/XP: Browser Favorites key")]
		BrowserFavorites = 0xAB,
		[Desc("Windows 2000/XP: Browser Start and Home key")]
		BrowserHome = 0xAC,
		[Desc("Windows 2000/XP: Volume Mute key")]
		VolumeMute = 0xAD,
		[Desc("Windows 2000/XP: Volume Down key")]
		VolumeDown = 0xAE,
		[Desc("Windows 2000/XP: Volume Up key")]
		VolumeUp = 0xAF,
		[Desc("Windows 2000/XP: Next Track key")]
		MediaNextTrack = 0xB0,
		[Desc("Windows 2000/XP: Previous Track key")]
		MediaPrevTrack = 0xB1,
		[Desc("Windows 2000/XP: Stop Media key")]
		MediaStop = 0xB2,
		[Desc("Windows 2000/XP: Play/Pause Media key")]
		MediaPlayPause = 0xB3,
		[Desc("Windows 2000/XP: Start Mail key")]
		LaunchMail = 0xB4,
		[Desc("Windows 2000/XP: Start Media key")]
		LaunchMediaSelect = 0xB5,
		[Desc("Windows 2000/XP: Start Application 1 key")]
		LaunchApp1 = 0xB6,
		[Desc("Windows 2000/XP: Start Application 2 key")]
		LaunchApp2 = 0xB7,
		[Desc("Used for miscellaneous characters; it can vary by keyboard.")]
		Oem1 = 0xBA,
		[Desc("Windows 2000/XP: For any country/region, the '+' key")]
		OemPlus = 0xBB,
		[Desc("Windows 2000/XP: For any country/region, the ',' key")]
		OemComma = 0xBC,
		[Desc("Windows 2000/XP: For any country/region, the '-' key")]
		OemMinus = 0xBD,
		[Desc("Windows 2000/XP: For any country/region, the '.' key")]
		[About("OemPeriod")]
		OemPoint = 0xBE,
		[Desc("Used for miscellaneous characters; it can vary by keyboard.")]
		Oem2 = 0xBF,
		[Desc("Used for miscellaneous characters; it can vary by keyboard.")]
		Oem3 = 0xC0,
		[Desc("Used for miscellaneous characters; it can vary by keyboard.")]
		Oem4 = 0xDB,
		[Desc("Used for miscellaneous characters; it can vary by keyboard.")]
		Oem5 = 0xDC,
		[Desc("Used for miscellaneous characters; it can vary by keyboard.")]
		Oem6 = 0xDD,
		[Desc("Used for miscellaneous characters; it can vary by keyboard.")]
		Oem7 = 0xDE,
		[Desc("Used for miscellaneous characters; it can vary by keyboard.")]
		Oem8 = 0xDF,
		[Desc("Windows 2000/XP: Either the angle bracket key or the backslash key on the RT 102-key keyboard")]
		Oem102 = 0xE2,
		[Desc("Windows 95/98/Me, Windows NT 4.0, Windows 2000/XP: IME PROCESS key")]
		Processkey = 0xE5,
		[Desc("Windows 2000/XP: Used to pass Unicode characters as if they were keystrokes. The VK_PACKET key is the low word of a 32-bit Virtual Key value used for non-keyboard input methods. For more information, see Remark in KEYBDINPUT, SendInput, WM_KEYDOWN, and WM_KEYUP")]
		Packet = 0xE7,
		Attn = 0xF6,
		CrSel = 0xF7,
		ExSel = 0xF8,
		[Desc("Erase EOF key")]
		ErEOF = 0xF9,
		Play = 0xFA,
		Zoom = 0xFB,
		[Desc("Reserved")]
		Noname = 0xFC,
		Pa1 = 0xFD,
		[About("Clear")]
		OemClear = 0xFE
	}
}