using System.Globalization;
using JetBrains.Annotations;

namespace Fijo.XGame.FrameworkServices {
	public interface ICharInputProvider {
		[NotNull] CharInputRule Get(char c, [NotNull] CultureInfo cultureInfo);
	}
}