using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace Fijo.XGame.FrameworkServices {
	[Dto]
	public class SendKey {
		public readonly KeyCode KeyCode;
		public readonly KeyEventFlags KeyEventFlags;

		public SendKey(KeyCode keyCode, KeyEventFlags keyEventFlags) {
			KeyCode = keyCode;
			KeyEventFlags = keyEventFlags;
		}
	}
}