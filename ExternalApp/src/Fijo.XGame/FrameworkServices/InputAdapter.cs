using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Fijo.Infrastructure.DesignPattern.Mapping;

namespace Fijo.XGame.FrameworkServices {
	public class InputAdapter : IInputAdapter {
		private readonly IMapping<MouseFlags, MouseEventFlags> _mouseFlagsMapping = new MouseFlagMapping();
		private readonly IMapping<ScrollDirection, MouseEventFlags> _scrollDirectionMapping = new ScrollDirectionMapping();
		private readonly IMapping<KeyCode, InternalKeyCode> _keyCodeMapping = new KeyCodeMapping();
		private readonly IMapping<FrameworkServices.KeyEventFlags, KeyEventFlags> _keyEventFlags = new KeyEventFlagsMapping();
		private readonly string _movementOutOfRangeMessage = string.Format("has to be at least {0} and less than or equal with {1}", MinMovement, MaxMovement);
		private const int MinMovement = int.MinValue / InputConstants.WheelDelta;
		private const int MaxMovement = int.MaxValue / InputConstants.WheelDelta;

		#region Implementation of IMouseService
		public void SetPosition(Point target) {
			Cursor.Position = target;
		}

		public Point GetPosition() {
			return Cursor.Position;
		}

		public void SendClick(MouseFlags mouseFlags) {
			SendInput(_mouseFlagsMapping.Map(mouseFlags));
		}

		public void SendScroll(ScrollDirection scrollDirection, int movement) {
			SendInput(_scrollDirectionMapping.Map(scrollDirection), movement);
		}

		public void SendKey(KeyCode keyCode, FrameworkServices.KeyEventFlags keyEventFlags) {
			var internalKeyCode = _keyCodeMapping.Map(keyCode);
			SendInput(internalKeyCode.ScanCodeShort, internalKeyCode.VirtualKeyShort, _keyEventFlags.Map(keyEventFlags));
		}
		#endregion
		
		#region SendScroll
		private void SendInput(MouseEventFlags mouseButton, int movement) {
			SendInput(GetInput(mouseButton, movement));
		}

		private Input GetInput(MouseEventFlags mouseButton, int movement) {
			return GetInput(new MouseInput {MouseEventFlags = mouseButton, MouseData = GetMovement(movement)});
		}

		private int GetMovement(int movement) {
			#region PreCondition
			if(MinMovement > movement || MaxMovement < movement) throw new ArgumentOutOfRangeException("movement", movement, _movementOutOfRangeMessage);
			#endregion
			return movement * InputConstants.WheelDelta;
		}
		#endregion

		#region SendClick
		private void SendInput(MouseEventFlags mouseButton) {
			SendInput(GetInput(mouseButton));
		}

		private Input GetInput(MouseEventFlags mouseButton) {
			return GetInput(new MouseInput {MouseEventFlags = mouseButton});
		}
		#endregion

		#region SendKey
		private void SendInput(ScanCodeShort scanCodeShort, VirtualKeyShort virtualKeyShort, KeyEventFlags keyEventFlags) {
			SendInput(GetInput(scanCodeShort, virtualKeyShort, keyEventFlags));
		}

		private Input GetInput(ScanCodeShort scanCodeShort, VirtualKeyShort virtualKeyShort, KeyEventFlags keyEventFlags) {
			return GetInput(new KeyboardInput {wScan = scanCodeShort, wVk = virtualKeyShort, dwFlags = keyEventFlags});
		}
		#endregion

		#region SendInput
		#region GetInput
		private Input GetInput(EventType eventType, InputUnion inputUnion) {
			return new Input
			{
				DwType = eventType,
				InputUnion = inputUnion
			};
		}
		
		private Input GetInput(MouseInput mouseInput) {
			return GetInput(EventType.MouseEvent, GetInputUnion(mouseInput));
		}
		
		private Input GetInput(KeyboardInput keyboardInput) {
			return GetInput(EventType.KeyboardEvent, GetInputUnion(keyboardInput));
		}
		
		#region GetInputUnion
		private InputUnion GetInputUnion(MouseInput mouseInput) {
			return new InputUnion {MouseInput = mouseInput};
		}

		private InputUnion GetInputUnion(KeyboardInput keyboardInput) {
			return new InputUnion {KeyboardInput = keyboardInput};
		}
		#endregion
		#endregion

		private void SendInput(Input input) {
			if (SendInput(1, input, InputSize()) == 0)
				throw new Exception("Faild to send input.");
		}

		private int InputSize() {
			return Marshal.SizeOf(typeof (Input));
		}
		#endregion

		#region private classes
		static class InputConstants {
			public const int WheelDelta = 120;
		}

		class MouseFlagMapping : IMapping<MouseFlags, MouseEventFlags> {
			private readonly IDictionary<MouseFlags, MouseEventFlags> _content;

			public MouseFlagMapping() {
				_content = new Dictionary<MouseFlags, MouseEventFlags>
				{
					{MouseFlags.Left | MouseFlags.Up, MouseEventFlags.LeftUp},
					{MouseFlags.Left | MouseFlags.Down, MouseEventFlags.LeftDown},
					{MouseFlags.Middle | MouseFlags.Up, MouseEventFlags.MiddleUp},
					{MouseFlags.Middle | MouseFlags.Down, MouseEventFlags.MiddleDown},
					{MouseFlags.Right | MouseFlags.Up, MouseEventFlags.RightUp},
					{MouseFlags.Right | MouseFlags.Down, MouseEventFlags.RightDown}
				};
			}

			#region Implementation of IMapping<in MouseFlags,out MouseEventFlags>
			public MouseEventFlags Map(MouseFlags source) {
				MouseEventFlags result;
				if (!_content.TryGetValue(source, out result)) throw new ArgumentException(string.Format("invalid MouseFlags enum value {0}", source), "source");
				return result;
			}
			#endregion
		}

		class ScrollDirectionMapping : IMapping<ScrollDirection, MouseEventFlags> {
			private readonly IDictionary<ScrollDirection, MouseEventFlags> _content;

			public ScrollDirectionMapping() {
				_content = new Dictionary<ScrollDirection, MouseEventFlags>
				{
					{ScrollDirection.Vertical, MouseEventFlags.Wheel},
					{ScrollDirection.Horizontal, MouseEventFlags.HWheel},
				};
			}

			#region Implementation of IMapping<in ScrollDirection,out MouseEventFlags>
			public MouseEventFlags Map(ScrollDirection source) {
				MouseEventFlags result;
				if (!_content.TryGetValue(source, out result)) throw new ArgumentException(string.Format("invalid ScrollDirection enum value {0}", source), "source");
				return result;
			}
			#endregion
		}
		
		class KeyCodeMapping : IMapping<KeyCode, InternalKeyCode> {
			private readonly IDictionary<KeyCode, InternalKeyCode> _content;

			public KeyCodeMapping() {
				_content = new Dictionary<KeyCode, InternalKeyCode>
				{
					{KeyCode.Accept, Create(ScanCodeShort.ACCEPT, VirtualKeyShort.ACCEPT)},
					{KeyCode.Add, Create(ScanCodeShort.ADD, VirtualKeyShort.ADD)},
					{KeyCode.Alt, Create(ScanCodeShort.MENU, VirtualKeyShort.MENU)},
					{KeyCode.Apps, Create(ScanCodeShort.APPS, VirtualKeyShort.APPS)},
					{KeyCode.Attn, Create(ScanCodeShort.ATTN, VirtualKeyShort.ATTN)},
					{KeyCode.Backspace, Create(ScanCodeShort.BACK, VirtualKeyShort.BACK)},
					{KeyCode.BrowserBack, Create(ScanCodeShort.BROWSER_BACK, VirtualKeyShort.BROWSER_BACK)},
					{KeyCode.BrowserFavorites, Create(ScanCodeShort.BROWSER_FAVORITES, VirtualKeyShort.BROWSER_FAVORITES)},
					{KeyCode.BrowserForward, Create(ScanCodeShort.BROWSER_FORWARD, VirtualKeyShort.BROWSER_FORWARD)},
					{KeyCode.BrowserHome, Create(ScanCodeShort.BROWSER_HOME, VirtualKeyShort.BROWSER_HOME)},
					{KeyCode.BrowserRefresh, Create(ScanCodeShort.BROWSER_REFRESH, VirtualKeyShort.BROWSER_REFRESH)},
					{KeyCode.BrowserSearch, Create(ScanCodeShort.BROWSER_SEARCH, VirtualKeyShort.BROWSER_SEARCH)},
					{KeyCode.BrowserStop, Create(ScanCodeShort.BROWSER_STOP, VirtualKeyShort.BROWSER_STOP)},
					{KeyCode.CapsLock, Create(ScanCodeShort.CAPITAL, VirtualKeyShort.CAPITAL)},
					{KeyCode.Clear, Create(ScanCodeShort.CLEAR, VirtualKeyShort.CLEAR)},
					{KeyCode.Control, Create(ScanCodeShort.CONTROL, VirtualKeyShort.CONTROL)},
					{KeyCode.Convert, Create(ScanCodeShort.CONVERT, VirtualKeyShort.CONVERT)},
					{KeyCode.CrSel, Create(ScanCodeShort.CRSEL, VirtualKeyShort.CRSEL)},
					{KeyCode.Decimal, Create(ScanCodeShort.DECIMAL, VirtualKeyShort.DECIMAL)},
					{KeyCode.Delete, Create(ScanCodeShort.DELETE, VirtualKeyShort.DELETE)},
					{KeyCode.Divide, Create(ScanCodeShort.DIVIDE, VirtualKeyShort.DIVIDE)},
					{KeyCode.Down, Create(ScanCodeShort.DOWN, VirtualKeyShort.DOWN)},
					{KeyCode.End, Create(ScanCodeShort.END, VirtualKeyShort.END)},
					{KeyCode.Enter, Create(ScanCodeShort.RETURN, VirtualKeyShort.RETURN)},
					{KeyCode.ErEOF, Create(ScanCodeShort.EREOF, VirtualKeyShort.EREOF)},
					{KeyCode.Escape, Create(ScanCodeShort.ESCAPE, VirtualKeyShort.ESCAPE)},
					{KeyCode.Execute, Create(ScanCodeShort.EXECUTE, VirtualKeyShort.EXECUTE)},
					{KeyCode.ExSel, Create(ScanCodeShort.EXSEL, VirtualKeyShort.EXSEL)},
					{KeyCode.F1, Create(ScanCodeShort.F1, VirtualKeyShort.F1)},
					{KeyCode.F2, Create(ScanCodeShort.F2, VirtualKeyShort.F2)},
					{KeyCode.F3, Create(ScanCodeShort.F3, VirtualKeyShort.F3)},
					{KeyCode.F4, Create(ScanCodeShort.F4, VirtualKeyShort.F4)},
					{KeyCode.F5, Create(ScanCodeShort.F5, VirtualKeyShort.F5)},
					{KeyCode.F6, Create(ScanCodeShort.F6, VirtualKeyShort.F6)},
					{KeyCode.F7, Create(ScanCodeShort.F7, VirtualKeyShort.F7)},
					{KeyCode.F8, Create(ScanCodeShort.F8, VirtualKeyShort.F8)},
					{KeyCode.F9, Create(ScanCodeShort.F9, VirtualKeyShort.F9)},
					{KeyCode.F10, Create(ScanCodeShort.F10, VirtualKeyShort.F10)},
					{KeyCode.F11, Create(ScanCodeShort.F11, VirtualKeyShort.F11)},
					{KeyCode.F12, Create(ScanCodeShort.F12, VirtualKeyShort.F12)},
					{KeyCode.F13, Create(ScanCodeShort.F13, VirtualKeyShort.F13)},
					{KeyCode.F14, Create(ScanCodeShort.F14, VirtualKeyShort.F14)},
					{KeyCode.F15, Create(ScanCodeShort.F15, VirtualKeyShort.F15)},
					{KeyCode.F16, Create(ScanCodeShort.F16, VirtualKeyShort.F16)},
					{KeyCode.F17, Create(ScanCodeShort.F17, VirtualKeyShort.F17)},
					{KeyCode.F18, Create(ScanCodeShort.F18, VirtualKeyShort.F18)},
					{KeyCode.F19, Create(ScanCodeShort.F19, VirtualKeyShort.F19)},
					{KeyCode.F20, Create(ScanCodeShort.F20, VirtualKeyShort.F20)},
					{KeyCode.F21, Create(ScanCodeShort.F21, VirtualKeyShort.F21)},
					{KeyCode.F22, Create(ScanCodeShort.F22, VirtualKeyShort.F22)},
					{KeyCode.F23, Create(ScanCodeShort.F23, VirtualKeyShort.F23)},
					{KeyCode.F24, Create(ScanCodeShort.F24, VirtualKeyShort.F24)},
					{KeyCode.Final, Create(ScanCodeShort.FINAL, VirtualKeyShort.FINAL)},
					{KeyCode.Hanja, Create(ScanCodeShort.HANJA, VirtualKeyShort.HANJA)},
					{KeyCode.Help, Create(ScanCodeShort.HELP, VirtualKeyShort.HELP)},
					{KeyCode.Home, Create(ScanCodeShort.HOME, VirtualKeyShort.HOME)},
					{KeyCode.Insert, Create(ScanCodeShort.INSERT, VirtualKeyShort.INSERT)},
					{KeyCode.Junja, Create(ScanCodeShort.JUNJA, VirtualKeyShort.JUNJA)},
					{KeyCode.Kana, Create(ScanCodeShort.KANA, VirtualKeyShort.KANA)},
					{KeyCode.Key0, Create(ScanCodeShort.KEY_0, VirtualKeyShort.KEY_0)},
					{KeyCode.Key1, Create(ScanCodeShort.KEY_1, VirtualKeyShort.KEY_1)},
					{KeyCode.Key2, Create(ScanCodeShort.KEY_2, VirtualKeyShort.KEY_2)},
					{KeyCode.Key3, Create(ScanCodeShort.KEY_3, VirtualKeyShort.KEY_3)},
					{KeyCode.Key4, Create(ScanCodeShort.KEY_4, VirtualKeyShort.KEY_4)},
					{KeyCode.Key5, Create(ScanCodeShort.KEY_5, VirtualKeyShort.KEY_5)},
					{KeyCode.Key6, Create(ScanCodeShort.KEY_6, VirtualKeyShort.KEY_6)},
					{KeyCode.Key7, Create(ScanCodeShort.KEY_7, VirtualKeyShort.KEY_7)},
					{KeyCode.Key8, Create(ScanCodeShort.KEY_8, VirtualKeyShort.KEY_8)},
					{KeyCode.Key9, Create(ScanCodeShort.KEY_9, VirtualKeyShort.KEY_9)},
					{KeyCode.KeyA, Create(ScanCodeShort.KEY_A, VirtualKeyShort.KEY_A)},
					{KeyCode.KeyB, Create(ScanCodeShort.KEY_B, VirtualKeyShort.KEY_B)},
					{KeyCode.KeyC, Create(ScanCodeShort.KEY_C, VirtualKeyShort.KEY_C)},
					{KeyCode.KeyD, Create(ScanCodeShort.KEY_D, VirtualKeyShort.KEY_D)},
					{KeyCode.KeyE, Create(ScanCodeShort.KEY_E, VirtualKeyShort.KEY_E)},
					{KeyCode.KeyF, Create(ScanCodeShort.KEY_F, VirtualKeyShort.KEY_F)},
					{KeyCode.KeyG, Create(ScanCodeShort.KEY_G, VirtualKeyShort.KEY_G)},
					{KeyCode.KeyH, Create(ScanCodeShort.KEY_H, VirtualKeyShort.KEY_H)},
					{KeyCode.KeyI, Create(ScanCodeShort.KEY_I, VirtualKeyShort.KEY_I)},
					{KeyCode.KeyJ, Create(ScanCodeShort.KEY_J, VirtualKeyShort.KEY_J)},
					{KeyCode.KeyK, Create(ScanCodeShort.KEY_K, VirtualKeyShort.KEY_K)},
					{KeyCode.KeyL, Create(ScanCodeShort.KEY_L, VirtualKeyShort.KEY_L)},
					{KeyCode.KeyM, Create(ScanCodeShort.KEY_M, VirtualKeyShort.KEY_M)},
					{KeyCode.KeyN, Create(ScanCodeShort.KEY_N, VirtualKeyShort.KEY_N)},
					{KeyCode.KeyO, Create(ScanCodeShort.KEY_O, VirtualKeyShort.KEY_O)},
					{KeyCode.KeyP, Create(ScanCodeShort.KEY_P, VirtualKeyShort.KEY_P)},
					{KeyCode.KeyQ, Create(ScanCodeShort.KEY_Q, VirtualKeyShort.KEY_Q)},
					{KeyCode.KeyR, Create(ScanCodeShort.KEY_R, VirtualKeyShort.KEY_R)},
					{KeyCode.KeyS, Create(ScanCodeShort.KEY_S, VirtualKeyShort.KEY_S)},
					{KeyCode.KeyT, Create(ScanCodeShort.KEY_T, VirtualKeyShort.KEY_T)},
					{KeyCode.KeyU, Create(ScanCodeShort.KEY_U, VirtualKeyShort.KEY_U)},
					{KeyCode.KeyV, Create(ScanCodeShort.KEY_V, VirtualKeyShort.KEY_V)},
					{KeyCode.KeyW, Create(ScanCodeShort.KEY_W, VirtualKeyShort.KEY_W)},
					{KeyCode.KeyX, Create(ScanCodeShort.KEY_X, VirtualKeyShort.KEY_X)},
					{KeyCode.KeyY, Create(ScanCodeShort.KEY_Y, VirtualKeyShort.KEY_Y)},
					{KeyCode.KeyZ, Create(ScanCodeShort.KEY_Z, VirtualKeyShort.KEY_Z)},
					{KeyCode.LaunchApp1, Create(ScanCodeShort.LAUNCH_APP1, VirtualKeyShort.LAUNCH_APP1)},
					{KeyCode.LaunchApp2, Create(ScanCodeShort.LAUNCH_APP2, VirtualKeyShort.LAUNCH_APP2)},
					{KeyCode.LaunchMail, Create(ScanCodeShort.LAUNCH_MAIL, VirtualKeyShort.LAUNCH_MAIL)},
					{KeyCode.LaunchMediaSelect, Create(ScanCodeShort.LAUNCH_MEDIA_SELECT, VirtualKeyShort.LAUNCH_MEDIA_SELECT)},
					{KeyCode.Left, Create(ScanCodeShort.LEFT, VirtualKeyShort.LEFT)},
					{KeyCode.LeftControl, Create(ScanCodeShort.LCONTROL, VirtualKeyShort.LCONTROL)},
					{KeyCode.LeftMenu, Create(ScanCodeShort.LMENU, VirtualKeyShort.LMENU)},
					{KeyCode.LeftShift, Create(ScanCodeShort.LSHIFT, VirtualKeyShort.LSHIFT)},
					{KeyCode.LeftWin, Create(ScanCodeShort.LWIN, VirtualKeyShort.LWIN)},
					{KeyCode.MediaNextTrack, Create(ScanCodeShort.MEDIA_NEXT_TRACK, VirtualKeyShort.MEDIA_NEXT_TRACK)},
					{KeyCode.MediaPlayPause, Create(ScanCodeShort.MEDIA_PLAY_PAUSE, VirtualKeyShort.MEDIA_PLAY_PAUSE)},
					{KeyCode.MediaPrevTrack, Create(ScanCodeShort.MEDIA_PREV_TRACK, VirtualKeyShort.MEDIA_PREV_TRACK)},
					{KeyCode.MediaStop, Create(ScanCodeShort.MEDIA_STOP, VirtualKeyShort.MEDIA_STOP)},
					{KeyCode.Modechange, Create(ScanCodeShort.MODECHANGE, VirtualKeyShort.MODECHANGE)},
					{KeyCode.Multiply, Create(ScanCodeShort.MULTIPLY, VirtualKeyShort.MULTIPLY)},
					{KeyCode.Noname, Create(ScanCodeShort.NONAME, VirtualKeyShort.NONAME)},
					{KeyCode.Nonconvert, Create(ScanCodeShort.NONCONVERT, VirtualKeyShort.NONCONVERT)},
					{KeyCode.NumLock, Create(ScanCodeShort.NUMLOCK, VirtualKeyShort.NUMLOCK)},
					{KeyCode.Numpad0, Create(ScanCodeShort.NUMPAD0, VirtualKeyShort.NUMPAD0)},
					{KeyCode.Numpad1, Create(ScanCodeShort.NUMPAD1, VirtualKeyShort.NUMPAD1)},
					{KeyCode.Numpad2, Create(ScanCodeShort.NUMPAD2, VirtualKeyShort.NUMPAD2)},
					{KeyCode.Numpad3, Create(ScanCodeShort.NUMPAD3, VirtualKeyShort.NUMPAD3)},
					{KeyCode.Numpad4, Create(ScanCodeShort.NUMPAD4, VirtualKeyShort.NUMPAD4)},
					{KeyCode.Numpad5, Create(ScanCodeShort.NUMPAD5, VirtualKeyShort.NUMPAD5)},
					{KeyCode.Numpad6, Create(ScanCodeShort.NUMPAD6, VirtualKeyShort.NUMPAD6)},
					{KeyCode.Numpad7, Create(ScanCodeShort.NUMPAD7, VirtualKeyShort.NUMPAD7)},
					{KeyCode.Numpad8, Create(ScanCodeShort.NUMPAD8, VirtualKeyShort.NUMPAD8)},
					{KeyCode.Numpad9, Create(ScanCodeShort.NUMPAD9, VirtualKeyShort.NUMPAD9)},
					{KeyCode.Oem1, Create(ScanCodeShort.OEM_1, VirtualKeyShort.OEM_1)},
					{KeyCode.Oem102, Create(ScanCodeShort.OEM_102, VirtualKeyShort.OEM_102)},
					{KeyCode.Oem2, Create(ScanCodeShort.OEM_2, VirtualKeyShort.OEM_2)},
					{KeyCode.Oem3, Create(ScanCodeShort.OEM_3, VirtualKeyShort.OEM_3)},
					{KeyCode.Oem4, Create(ScanCodeShort.OEM_4, VirtualKeyShort.OEM_4)},
					{KeyCode.Oem5, Create(ScanCodeShort.OEM_5, VirtualKeyShort.OEM_5)},
					{KeyCode.Oem6, Create(ScanCodeShort.OEM_6, VirtualKeyShort.OEM_6)},
					{KeyCode.Oem7, Create(ScanCodeShort.OEM_7, VirtualKeyShort.OEM_7)},
					{KeyCode.Oem8, Create(ScanCodeShort.OEM_8, VirtualKeyShort.OEM_8)},
					{KeyCode.OemClear, Create(ScanCodeShort.OEM_CLEAR, VirtualKeyShort.OEM_CLEAR)},
					{KeyCode.OemComma, Create(ScanCodeShort.OEM_COMMA, VirtualKeyShort.OEM_COMMA)},
					{KeyCode.OemMinus, Create(ScanCodeShort.OEM_MINUS, VirtualKeyShort.OEM_MINUS)},
					{KeyCode.OemPoint, Create(ScanCodeShort.OEM_PERIOD, VirtualKeyShort.OEM_PERIOD)},
					{KeyCode.OemPlus, Create(ScanCodeShort.OEM_PLUS, VirtualKeyShort.OEM_PLUS)},
					{KeyCode.Pa1, Create(ScanCodeShort.PA1, VirtualKeyShort.PA1)},
					{KeyCode.Packet, Create(ScanCodeShort.PA1, VirtualKeyShort.PACKET)},
					{KeyCode.PageDown, Create(ScanCodeShort.PRIOR, VirtualKeyShort.PRIOR)},
					{KeyCode.PageUp, Create(ScanCodeShort.NEXT, VirtualKeyShort.NEXT)},
					{KeyCode.Pause, Create(ScanCodeShort.PAUSE, VirtualKeyShort.PAUSE)},
					{KeyCode.Play, Create(ScanCodeShort.PLAY, VirtualKeyShort.PLAY)},
					{KeyCode.Print, Create(ScanCodeShort.PRINT, VirtualKeyShort.PRINT)},
					{KeyCode.Processkey, Create(ScanCodeShort.PROCESSKEY, VirtualKeyShort.PROCESSKEY)},
					{KeyCode.Right, Create(ScanCodeShort.RIGHT, VirtualKeyShort.RIGHT)},
					{KeyCode.RightControl, Create(ScanCodeShort.RCONTROL, VirtualKeyShort.RCONTROL)},
					{KeyCode.RightMenu, Create(ScanCodeShort.RMENU, VirtualKeyShort.RMENU)},
					{KeyCode.RightShift, Create(ScanCodeShort.RSHIFT, VirtualKeyShort.RSHIFT)},
					{KeyCode.RightWin, Create(ScanCodeShort.RWIN, VirtualKeyShort.RWIN)},
					{KeyCode.ScrollLock, Create(ScanCodeShort.SCROLL, VirtualKeyShort.SCROLL)},
					{KeyCode.Select, Create(ScanCodeShort.SELECT, VirtualKeyShort.SELECT)},
					{KeyCode.Separator, Create(ScanCodeShort.SEPARATOR, VirtualKeyShort.SEPARATOR)},
					{KeyCode.Shift, Create(ScanCodeShort.SHIFT, VirtualKeyShort.SHIFT)},
					{KeyCode.Sleep, Create(ScanCodeShort.SLEEP, VirtualKeyShort.SLEEP)},
					{KeyCode.Snapshot, Create(ScanCodeShort.SNAPSHOT, VirtualKeyShort.SNAPSHOT)},
					{KeyCode.Space, Create(ScanCodeShort.SPACE, VirtualKeyShort.SPACE)},
					{KeyCode.Subtract, Create(ScanCodeShort.SUBTRACT, VirtualKeyShort.SUBTRACT)},
					{KeyCode.Tab, Create(ScanCodeShort.TAB, VirtualKeyShort.TAB)},
					{KeyCode.Up, Create(ScanCodeShort.UP, VirtualKeyShort.UP)},
					{KeyCode.VolumeDown, Create(ScanCodeShort.VOLUME_DOWN, VirtualKeyShort.VOLUME_DOWN)},
					{KeyCode.VolumeMute, Create(ScanCodeShort.VOLUME_MUTE, VirtualKeyShort.VOLUME_MUTE)},
					{KeyCode.VolumeUp, Create(ScanCodeShort.VOLUME_UP, VirtualKeyShort.VOLUME_UP)},
					{KeyCode.Zoom, Create(ScanCodeShort.ZOOM, VirtualKeyShort.ZOOM)}
				};
			}

			private InternalKeyCode Create(ScanCodeShort scanCodeShort, VirtualKeyShort virtualKeyShort) {
				var internalKeyCode = new InternalKeyCode();
				internalKeyCode.ScanCodeShort = scanCodeShort;
				internalKeyCode.VirtualKeyShort = virtualKeyShort;
				return internalKeyCode;
			}

			#region Implementation of IMapping<in KeyCode,out InternalKeyCode>
			public InternalKeyCode Map(KeyCode source) {
				InternalKeyCode result;
				if (!_content.TryGetValue(source, out result)) throw new ArgumentException(string.Format("invalid KeyCode enum value {0}", source), "source");
				return result;
			}
			#endregion
		}
		
		class KeyEventFlagsMapping : IMapping<FrameworkServices.KeyEventFlags, KeyEventFlags> {
			private readonly IDictionary<FrameworkServices.KeyEventFlags, KeyEventFlags> _content;

			public KeyEventFlagsMapping() {
				_content = new Dictionary<FrameworkServices.KeyEventFlags, KeyEventFlags>
				{
					{FrameworkServices.KeyEventFlags.Down, 0},
					{FrameworkServices.KeyEventFlags.Up, KeyEventFlags.KEYUP}
				};
			}

			#region Implementation of IMapping<in KeyEventFlags,out KeyEventFlags>
			public KeyEventFlags Map(FrameworkServices.KeyEventFlags source) {
				KeyEventFlags result;
				if (!_content.TryGetValue(source, out result)) throw new ArgumentException(string.Format("invalid KeyEventFlags enum value {0}", source), "source");
				return result;
			}
			#endregion
		}

		[StructLayout(LayoutKind.Sequential)]
		struct MouseInput {
			public int DX;
			public int DY;
			public int MouseData;
			public MouseEventFlags MouseEventFlags;
			public int Time;
			public IntPtr DwExtraInfo;
		}

		[StructLayout(LayoutKind.Sequential)]
		struct KeyboardInput {
			public VirtualKeyShort wVk;
			public ScanCodeShort wScan;
			public KeyEventFlags dwFlags;
			public int time;
			public UIntPtr dwExtraInfo;
		}

		struct Input {
			public EventType DwType;
			public InputUnion InputUnion;
		}

		[StructLayout(LayoutKind.Explicit)]
		struct InputUnion {
			[FieldOffset(0)] public MouseInput MouseInput;
			[FieldOffset(0)] public KeyboardInput KeyboardInput;
		}

		enum EventType : uint {
			MouseEvent = 0,
			KeyboardEvent = 1
		}

		[Flags]
		enum MouseEventFlags : uint {
			Absolute = 0x8000,
			HWheel = 0x01000,
			Move = 0x0001,
			MoveNocoalesce = 0x2000,
			LeftDown = 0x0002,
			LeftUp = 0x0004,
			RightDown = 0x0008,
			RightUp = 0x0010,
			MiddleDown = 0x0020,
			MiddleUp = 0x0040,
			VirtualDesk = 0x4000,
			Wheel = 0x0800
		}

		struct InternalKeyCode {
			public VirtualKeyShort VirtualKeyShort;
			public ScanCodeShort ScanCodeShort;
		}

		[Flags]
		enum KeyEventFlags : uint
		{
			EXTENDEDKEY = 0x0001,
			KEYUP = 0x0002,
			SCANCODE = 0x0008,
			UNICODE = 0x0004
		}

		enum VirtualKeyShort : short {
			///<summary>
			///Left mouse button
			///</summary>
			LBUTTON = 0x01,
			///<summary>
			///Right mouse button
			///</summary>
			RBUTTON = 0x02,
			///<summary>
			///Control-break processing
			///</summary>
			CANCEL = 0x03,
			///<summary>
			///Middle mouse button (three-button mouse)
			///</summary>
			MBUTTON = 0x04,
			///<summary>
			///Windows 2000/XP: X1 mouse button
			///</summary>
			XBUTTON1 = 0x05,
			///<summary>
			///Windows 2000/XP: X2 mouse button
			///</summary>
			XBUTTON2 = 0x06,
			///<summary>
			///BACKSPACE key
			///</summary>
			BACK = 0x08,
			///<summary>
			///TAB key
			///</summary>
			TAB = 0x09,
			///<summary>
			///CLEAR key
			///</summary>
			CLEAR = 0x0C,
			///<summary>
			///ENTER key
			///</summary>
			RETURN = 0x0D,
			///<summary>
			///SHIFT key
			///</summary>
			SHIFT = 0x10,
			///<summary>
			///CTRL key
			///</summary>
			CONTROL = 0x11,
			///<summary>
			///ALT key
			///</summary>
			MENU = 0x12,
			///<summary>
			///PAUSE key
			///</summary>
			PAUSE = 0x13,
			///<summary>
			///CAPS LOCK key
			///</summary>
			CAPITAL = 0x14,
			///<summary>
			///Input Method Editor (IME) Kana mode
			///</summary>
			KANA = 0x15,
			///<summary>
			///IME Hangul mode
			///</summary>
			HANGUL = 0x15,
			///<summary>
			///IME Junja mode
			///</summary>
			JUNJA = 0x17,
			///<summary>
			///IME final mode
			///</summary>
			FINAL = 0x18,
			///<summary>
			///IME Hanja mode
			///</summary>
			HANJA = 0x19,
			///<summary>
			///IME Kanji mode
			///</summary>
			KANJI = 0x19,
			///<summary>
			///ESC key
			///</summary>
			ESCAPE = 0x1B,
			///<summary>
			///IME convert
			///</summary>
			CONVERT = 0x1C,
			///<summary>
			///IME nonconvert
			///</summary>
			NONCONVERT = 0x1D,
			///<summary>
			///IME accept
			///</summary>
			ACCEPT = 0x1E,
			///<summary>
			///IME mode change request
			///</summary>
			MODECHANGE = 0x1F,
			///<summary>
			///SPACEBAR
			///</summary>
			SPACE = 0x20,
			///<summary>
			///PAGE UP key
			///</summary>
			PRIOR = 0x21,
			///<summary>
			///PAGE DOWN key
			///</summary>
			NEXT = 0x22,
			///<summary>
			///END key
			///</summary>
			END = 0x23,
			///<summary>
			///HOME key
			///</summary>
			HOME = 0x24,
			///<summary>
			///LEFT ARROW key
			///</summary>
			LEFT = 0x25,
			///<summary>
			///UP ARROW key
			///</summary>
			UP = 0x26,
			///<summary>
			///RIGHT ARROW key
			///</summary>
			RIGHT = 0x27,
			///<summary>
			///DOWN ARROW key
			///</summary>
			DOWN = 0x28,
			///<summary>
			///SELECT key
			///</summary>
			SELECT = 0x29,
			///<summary>
			///PRINT key
			///</summary>
			PRINT = 0x2A,
			///<summary>
			///EXECUTE key
			///</summary>
			EXECUTE = 0x2B,
			///<summary>
			///PRINT SCREEN key
			///</summary>
			SNAPSHOT = 0x2C,
			///<summary>
			///INS key
			///</summary>
			INSERT = 0x2D,
			///<summary>
			///DEL key
			///</summary>
			DELETE = 0x2E,
			///<summary>
			///HELP key
			///</summary>
			HELP = 0x2F,
			///<summary>
			///0 key
			///</summary>
			KEY_0 = 0x30,
			///<summary>
			///1 key
			///</summary>
			KEY_1 = 0x31,
			///<summary>
			///2 key
			///</summary>
			KEY_2 = 0x32,
			///<summary>
			///3 key
			///</summary>
			KEY_3 = 0x33,
			///<summary>
			///4 key
			///</summary>
			KEY_4 = 0x34,
			///<summary>
			///5 key
			///</summary>
			KEY_5 = 0x35,
			///<summary>
			///6 key
			///</summary>
			KEY_6 = 0x36,
			///<summary>
			///7 key
			///</summary>
			KEY_7 = 0x37,
			///<summary>
			///8 key
			///</summary>
			KEY_8 = 0x38,
			///<summary>
			///9 key
			///</summary>
			KEY_9 = 0x39,
			///<summary>
			///A key
			///</summary>
			KEY_A = 0x41,
			///<summary>
			///B key
			///</summary>
			KEY_B = 0x42,
			///<summary>
			///C key
			///</summary>
			KEY_C = 0x43,
			///<summary>
			///D key
			///</summary>
			KEY_D = 0x44,
			///<summary>
			///E key
			///</summary>
			KEY_E = 0x45,
			///<summary>
			///F key
			///</summary>
			KEY_F = 0x46,
			///<summary>
			///G key
			///</summary>
			KEY_G = 0x47,
			///<summary>
			///H key
			///</summary>
			KEY_H = 0x48,
			///<summary>
			///I key
			///</summary>
			KEY_I = 0x49,
			///<summary>
			///J key
			///</summary>
			KEY_J = 0x4A,
			///<summary>
			///K key
			///</summary>
			KEY_K = 0x4B,
			///<summary>
			///L key
			///</summary>
			KEY_L = 0x4C,
			///<summary>
			///M key
			///</summary>
			KEY_M = 0x4D,
			///<summary>
			///N key
			///</summary>
			KEY_N = 0x4E,
			///<summary>
			///O key
			///</summary>
			KEY_O = 0x4F,
			///<summary>
			///P key
			///</summary>
			KEY_P = 0x50,
			///<summary>
			///Q key
			///</summary>
			KEY_Q = 0x51,
			///<summary>
			///R key
			///</summary>
			KEY_R = 0x52,
			///<summary>
			///S key
			///</summary>
			KEY_S = 0x53,
			///<summary>
			///T key
			///</summary>
			KEY_T = 0x54,
			///<summary>
			///U key
			///</summary>
			KEY_U = 0x55,
			///<summary>
			///V key
			///</summary>
			KEY_V = 0x56,
			///<summary>
			///W key
			///</summary>
			KEY_W = 0x57,
			///<summary>
			///X key
			///</summary>
			KEY_X = 0x58,
			///<summary>
			///Y key
			///</summary>
			KEY_Y = 0x59,
			///<summary>
			///Z key
			///</summary>
			KEY_Z = 0x5A,
			///<summary>
			///Left Windows key (Microsoft Natural keyboard) 
			///</summary>
			LWIN = 0x5B,
			///<summary>
			///Right Windows key (Natural keyboard)
			///</summary>
			RWIN = 0x5C,
			///<summary>
			///Applications key (Natural keyboard)
			///</summary>
			APPS = 0x5D,
			///<summary>
			///Computer Sleep key
			///</summary>
			SLEEP = 0x5F,
			///<summary>
			///Numeric keypad 0 key
			///</summary>
			NUMPAD0 = 0x60,
			///<summary>
			///Numeric keypad 1 key
			///</summary>
			NUMPAD1 = 0x61,
			///<summary>
			///Numeric keypad 2 key
			///</summary>
			NUMPAD2 = 0x62,
			///<summary>
			///Numeric keypad 3 key
			///</summary>
			NUMPAD3 = 0x63,
			///<summary>
			///Numeric keypad 4 key
			///</summary>
			NUMPAD4 = 0x64,
			///<summary>
			///Numeric keypad 5 key
			///</summary>
			NUMPAD5 = 0x65,
			///<summary>
			///Numeric keypad 6 key
			///</summary>
			NUMPAD6 = 0x66,
			///<summary>
			///Numeric keypad 7 key
			///</summary>
			NUMPAD7 = 0x67,
			///<summary>
			///Numeric keypad 8 key
			///</summary>
			NUMPAD8 = 0x68,
			///<summary>
			///Numeric keypad 9 key
			///</summary>
			NUMPAD9 = 0x69,
			///<summary>
			///Multiply key
			///</summary>
			MULTIPLY = 0x6A,
			///<summary>
			///Add key
			///</summary>
			ADD = 0x6B,
			///<summary>
			///Separator key
			///</summary>
			SEPARATOR = 0x6C,
			///<summary>
			///Subtract key
			///</summary>
			SUBTRACT = 0x6D,
			///<summary>
			///Decimal key
			///</summary>
			DECIMAL = 0x6E,
			///<summary>
			///Divide key
			///</summary>
			DIVIDE = 0x6F,
			///<summary>
			///F1 key
			///</summary>
			F1 = 0x70,
			///<summary>
			///F2 key
			///</summary>
			F2 = 0x71,
			///<summary>
			///F3 key
			///</summary>
			F3 = 0x72,
			///<summary>
			///F4 key
			///</summary>
			F4 = 0x73,
			///<summary>
			///F5 key
			///</summary>
			F5 = 0x74,
			///<summary>
			///F6 key
			///</summary>
			F6 = 0x75,
			///<summary>
			///F7 key
			///</summary>
			F7 = 0x76,
			///<summary>
			///F8 key
			///</summary>
			F8 = 0x77,
			///<summary>
			///F9 key
			///</summary>
			F9 = 0x78,
			///<summary>
			///F10 key
			///</summary>
			F10 = 0x79,
			///<summary>
			///F11 key
			///</summary>
			F11 = 0x7A,
			///<summary>
			///F12 key
			///</summary>
			F12 = 0x7B,
			///<summary>
			///F13 key
			///</summary>
			F13 = 0x7C,
			///<summary>
			///F14 key
			///</summary>
			F14 = 0x7D,
			///<summary>
			///F15 key
			///</summary>
			F15 = 0x7E,
			///<summary>
			///F16 key
			///</summary>
			F16 = 0x7F,
			///<summary>
			///F17 key  
			///</summary>
			F17 = 0x80,
			///<summary>
			///F18 key  
			///</summary>
			F18 = 0x81,
			///<summary>
			///F19 key  
			///</summary>
			F19 = 0x82,
			///<summary>
			///F20 key  
			///</summary>
			F20 = 0x83,
			///<summary>
			///F21 key  
			///</summary>
			F21 = 0x84,
			///<summary>
			///F22 key, (PPC only) Key used to lock device.
			///</summary>
			F22 = 0x85,
			///<summary>
			///F23 key  
			///</summary>
			F23 = 0x86,
			///<summary>
			///F24 key  
			///</summary>
			F24 = 0x87,
			///<summary>
			///NUM LOCK key
			///</summary>
			NUMLOCK = 0x90,
			///<summary>
			///SCROLL LOCK key
			///</summary>
			SCROLL = 0x91,
			///<summary>
			///Left SHIFT key
			///</summary>
			LSHIFT = 0xA0,
			///<summary>
			///Right SHIFT key
			///</summary>
			RSHIFT = 0xA1,
			///<summary>
			///Left CONTROL key
			///</summary>
			LCONTROL = 0xA2,
			///<summary>
			///Right CONTROL key
			///</summary>
			RCONTROL = 0xA3,
			///<summary>
			///Left MENU key
			///</summary>
			LMENU = 0xA4,
			///<summary>
			///Right MENU key
			///</summary>
			RMENU = 0xA5,
			///<summary>
			///Windows 2000/XP: Browser Back key
			///</summary>
			BROWSER_BACK = 0xA6,
			///<summary>
			///Windows 2000/XP: Browser Forward key
			///</summary>
			BROWSER_FORWARD = 0xA7,
			///<summary>
			///Windows 2000/XP: Browser Refresh key
			///</summary>
			BROWSER_REFRESH = 0xA8,
			///<summary>
			///Windows 2000/XP: Browser Stop key
			///</summary>
			BROWSER_STOP = 0xA9,
			///<summary>
			///Windows 2000/XP: Browser Search key 
			///</summary>
			BROWSER_SEARCH = 0xAA,
			///<summary>
			///Windows 2000/XP: Browser Favorites key
			///</summary>
			BROWSER_FAVORITES = 0xAB,
			///<summary>
			///Windows 2000/XP: Browser Start and Home key
			///</summary>
			BROWSER_HOME = 0xAC,
			///<summary>
			///Windows 2000/XP: Volume Mute key
			///</summary>
			VOLUME_MUTE = 0xAD,
			///<summary>
			///Windows 2000/XP: Volume Down key
			///</summary>
			VOLUME_DOWN = 0xAE,
			///<summary>
			///Windows 2000/XP: Volume Up key
			///</summary>
			VOLUME_UP = 0xAF,
			///<summary>
			///Windows 2000/XP: Next Track key
			///</summary>
			MEDIA_NEXT_TRACK = 0xB0,
			///<summary>
			///Windows 2000/XP: Previous Track key
			///</summary>
			MEDIA_PREV_TRACK = 0xB1,
			///<summary>
			///Windows 2000/XP: Stop Media key
			///</summary>
			MEDIA_STOP = 0xB2,
			///<summary>
			///Windows 2000/XP: Play/Pause Media key
			///</summary>
			MEDIA_PLAY_PAUSE = 0xB3,
			///<summary>
			///Windows 2000/XP: Start Mail key
			///</summary>
			LAUNCH_MAIL = 0xB4,
			///<summary>
			///Windows 2000/XP: Select Media key
			///</summary>
			LAUNCH_MEDIA_SELECT = 0xB5,
			///<summary>
			///Windows 2000/XP: Start Application 1 key
			///</summary>
			LAUNCH_APP1 = 0xB6,
			///<summary>
			///Windows 2000/XP: Start Application 2 key
			///</summary>
			LAUNCH_APP2 = 0xB7,
			///<summary>
			///Used for miscellaneous characters; it can vary by keyboard.
			///</summary>
			OEM_1 = 0xBA,
			///<summary>
			///Windows 2000/XP: For any country/region, the '+' key
			///</summary>
			OEM_PLUS = 0xBB,
			///<summary>
			///Windows 2000/XP: For any country/region, the ',' key
			///</summary>
			OEM_COMMA = 0xBC,
			///<summary>
			///Windows 2000/XP: For any country/region, the '-' key
			///</summary>
			OEM_MINUS = 0xBD,
			///<summary>
			///Windows 2000/XP: For any country/region, the '.' key
			///</summary>
			OEM_PERIOD = 0xBE,
			///<summary>
			///Used for miscellaneous characters; it can vary by keyboard.
			///</summary>
			OEM_2 = 0xBF,
			///<summary>
			///Used for miscellaneous characters; it can vary by keyboard. 
			///</summary>
			OEM_3 = 0xC0,
			///<summary>
			///Used for miscellaneous characters; it can vary by keyboard. 
			///</summary>
			OEM_4 = 0xDB,
			///<summary>
			///Used for miscellaneous characters; it can vary by keyboard. 
			///</summary>
			OEM_5 = 0xDC,
			///<summary>
			///Used for miscellaneous characters; it can vary by keyboard. 
			///</summary>
			OEM_6 = 0xDD,
			///<summary>
			///Used for miscellaneous characters; it can vary by keyboard. 
			///</summary>
			OEM_7 = 0xDE,
			///<summary>
			///Used for miscellaneous characters; it can vary by keyboard.
			///</summary>
			OEM_8 = 0xDF,
			///<summary>
			///Windows 2000/XP: Either the angle bracket key or the backslash key on the RT 102-key keyboard
			///</summary>
			OEM_102 = 0xE2,
			///<summary>
			///Windows 95/98/Me, Windows NT 4.0, Windows 2000/XP: IME PROCESS key
			///</summary>
			PROCESSKEY = 0xE5,
			///<summary>
			///Windows 2000/XP: Used to pass Unicode characters as if they were keystrokes.
			///The VK_PACKET key is the low word of a 32-bit Virtual Key value used for non-keyboard input methods. For more information,
			///see Remark in KEYBDINPUT, SendInput, WM_KEYDOWN, and WM_KEYUP
			///</summary>
			PACKET = 0xE7,
			///<summary>
			///Attn key
			///</summary>
			ATTN = 0xF6,
			///<summary>
			///CrSel key
			///</summary>
			CRSEL = 0xF7,
			///<summary>
			///ExSel key
			///</summary>
			EXSEL = 0xF8,
			///<summary>
			///Erase EOF key
			///</summary>
			EREOF = 0xF9,
			///<summary>
			///Play key
			///</summary>
			PLAY = 0xFA,
			///<summary>
			///Zoom key
			///</summary>
			ZOOM = 0xFB,
			///<summary>
			///Reserved 
			///</summary>
			NONAME = 0xFC,
			///<summary>
			///PA1 key
			///</summary>
			PA1 = 0xFD,
			///<summary>
			///Clear key
			///</summary>
			OEM_CLEAR = 0xFE
		}

		enum ScanCodeShort : short {
			LBUTTON = 0,
			RBUTTON = 0,
			CANCEL = 70,
			MBUTTON = 0,
			XBUTTON1 = 0,
			XBUTTON2 = 0,
			BACK = 14,
			TAB = 15,
			CLEAR = 76,
			RETURN = 28,
			SHIFT = 42,
			CONTROL = 29,
			MENU = 56,
			PAUSE = 0,
			CAPITAL = 58,
			KANA = 0,
			HANGUL = 0,
			JUNJA = 0,
			FINAL = 0,
			HANJA = 0,
			KANJI = 0,
			ESCAPE = 1,
			CONVERT = 0,
			NONCONVERT = 0,
			ACCEPT = 0,
			MODECHANGE = 0,
			SPACE = 57,
			PRIOR = 73,
			NEXT = 81,
			END = 79,
			HOME = 71,
			LEFT = 75,
			UP = 72,
			RIGHT = 77,
			DOWN = 80,
			SELECT = 0,
			PRINT = 0,
			EXECUTE = 0,
			SNAPSHOT = 84,
			INSERT = 82,
			DELETE = 83,
			HELP = 99,
			KEY_0 = 11,
			KEY_1 = 2,
			KEY_2 = 3,
			KEY_3 = 4,
			KEY_4 = 5,
			KEY_5 = 6,
			KEY_6 = 7,
			KEY_7 = 8,
			KEY_8 = 9,
			KEY_9 = 10,
			KEY_A = 30,
			KEY_B = 48,
			KEY_C = 46,
			KEY_D = 32,
			KEY_E = 18,
			KEY_F = 33,
			KEY_G = 34,
			KEY_H = 35,
			KEY_I = 23,
			KEY_J = 36,
			KEY_K = 37,
			KEY_L = 38,
			KEY_M = 50,
			KEY_N = 49,
			KEY_O = 24,
			KEY_P = 25,
			KEY_Q = 16,
			KEY_R = 19,
			KEY_S = 31,
			KEY_T = 20,
			KEY_U = 22,
			KEY_V = 47,
			KEY_W = 17,
			KEY_X = 45,
			KEY_Y = 21,
			KEY_Z = 44,
			LWIN = 91,
			RWIN = 92,
			APPS = 93,
			SLEEP = 95,
			NUMPAD0 = 82,
			NUMPAD1 = 79,
			NUMPAD2 = 80,
			NUMPAD3 = 81,
			NUMPAD4 = 75,
			NUMPAD5 = 76,
			NUMPAD6 = 77,
			NUMPAD7 = 71,
			NUMPAD8 = 72,
			NUMPAD9 = 73,
			MULTIPLY = 55,
			ADD = 78,
			SEPARATOR = 0,
			SUBTRACT = 74,
			DECIMAL = 83,
			DIVIDE = 53,
			F1 = 59,
			F2 = 60,
			F3 = 61,
			F4 = 62,
			F5 = 63,
			F6 = 64,
			F7 = 65,
			F8 = 66,
			F9 = 67,
			F10 = 68,
			F11 = 87,
			F12 = 88,
			F13 = 100,
			F14 = 101,
			F15 = 102,
			F16 = 103,
			F17 = 104,
			F18 = 105,
			F19 = 106,
			F20 = 107,
			F21 = 108,
			F22 = 109,
			F23 = 110,
			F24 = 118,
			NUMLOCK = 69,
			SCROLL = 70,
			LSHIFT = 42,
			RSHIFT = 54,
			LCONTROL = 29,
			RCONTROL = 29,
			LMENU = 56,
			RMENU = 56,
			BROWSER_BACK = 106,
			BROWSER_FORWARD = 105,
			BROWSER_REFRESH = 103,
			BROWSER_STOP = 104,
			BROWSER_SEARCH = 101,
			BROWSER_FAVORITES = 102,
			BROWSER_HOME = 50,
			VOLUME_MUTE = 32,
			VOLUME_DOWN = 46,
			VOLUME_UP = 48,
			MEDIA_NEXT_TRACK = 25,
			MEDIA_PREV_TRACK = 16,
			MEDIA_STOP = 36,
			MEDIA_PLAY_PAUSE = 34,
			LAUNCH_MAIL = 108,
			LAUNCH_MEDIA_SELECT = 109,
			LAUNCH_APP1 = 107,
			LAUNCH_APP2 = 33,
			OEM_1 = 39,
			OEM_PLUS = 13,
			OEM_COMMA = 51,
			OEM_MINUS = 12,
			OEM_PERIOD = 52,
			OEM_2 = 53,
			OEM_3 = 41,
			OEM_4 = 26,
			OEM_5 = 43,
			OEM_6 = 27,
			OEM_7 = 40,
			OEM_8 = 0,
			OEM_102 = 86,
			PROCESSKEY = 0,
			PACKET = 0,
			ATTN = 0,
			CRSEL = 0,
			EXSEL = 0,
			EREOF = 93,
			PLAY = 0,
			ZOOM = 98,
			NONAME = 0,
			PA1 = 0,
			OEM_CLEAR = 0,
		}
		#endregion

		#region Extern
		[DllImport("user32.dll", SetLastError = true)]
		private static extern uint SendInput(uint cInputs, Input input, int size);
		#endregion
	}
}