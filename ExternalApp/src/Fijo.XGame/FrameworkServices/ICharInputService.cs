using System.Globalization;
using JetBrains.Annotations;

namespace Fijo.XGame.FrameworkServices {
	public interface ICharInputService {
		void SendKeys(char key, [NotNull] CultureInfo cultureInfo);
	}
}