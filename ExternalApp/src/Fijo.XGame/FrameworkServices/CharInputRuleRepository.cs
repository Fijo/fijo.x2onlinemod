using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using FijoCore.Infrastructure.LightContrib.Extentions.Char;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using JetBrains.Annotations;

namespace Fijo.XGame.FrameworkServices {
	[UsedImplicitly]
	public class CharInputRuleRepository : ICharInputRuleRepository {
		private readonly IDictionary<char, IList<SendKey>> _additionalCharKeys;

		public CharInputRuleRepository() {
			_additionalCharKeys = new Dictionary<char, IList<SendKey>>
			                      {
				                      {'.', CreateSendKeyList(KeyCode.OemPoint)},
			                      };
		}

		public virtual IEnumerable<CharInputRule> Get(CultureInfo cultureInfo) {
			return GetAllChars().AsParallel().Select(y => GetCharInputRule(y, cultureInfo))
				.Where(x => x.SendKeys.Any()); // ToDo remove the empty ones (the unknown stuff)
		}

		private CharInputRule GetCharInputRule(char c, CultureInfo cultureInfo) {
			return CreateCharInputRule(GetSendKeys(c, cultureInfo).Execute(), c);
		}

		private CharInputRule CreateCharInputRule(ICollection<SendKey> sendKeys, char c) {
			return new CharInputRule(sendKeys, c);
		}

		private IEnumerable<SendKey> GetSendKeys(char c, CultureInfo cultureInfo) {
			var uppered = c.ToUpper(cultureInfo);
			var enumKey = string.Format("Key{0}", uppered);
			KeyCode keyCode;
			if(!Enum.TryParse(enumKey, out keyCode)) {
				IList<SendKey> result;
				if (_additionalCharKeys.TryGetValue(c, out result)) foreach (var sendKey in result) yield return sendKey;
				yield break; // ToDo just ignore unknown stuff look after this later
			}
			var isUpper = c.IsUpper(cultureInfo);
			if (isUpper) yield return CreateSendKey(KeyCode.Shift, KeyEventFlags.Down);
			yield return CreateSendKey(keyCode, KeyEventFlags.Down);
			yield return CreateSendKey(keyCode, KeyEventFlags.Up);
			if (isUpper) yield return CreateSendKey(KeyCode.Shift, KeyEventFlags.Up);
		}

		private SendKey CreateSendKey(KeyCode keyCode, KeyEventFlags keyEventFlags) {
			return new SendKey(keyCode, keyEventFlags);
		}

		private IEnumerable<char> GetAllChars() {
			return Enumerable.Range(char.MinValue, char.MaxValue).Select(x => (char) x);
		}

		private IList<SendKey> CreateSendKeyList(KeyCode keyCode) {
			return new List<SendKey> {CreateSendKey(keyCode, KeyEventFlags.Down), CreateSendKey(keyCode, KeyEventFlags.Up)};
		}
	}
}