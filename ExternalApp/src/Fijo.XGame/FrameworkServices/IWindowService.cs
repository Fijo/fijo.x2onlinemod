using System;
using System.Drawing;
using System.Runtime.InteropServices;

namespace Fijo.XGame.FrameworkServices {
	public interface IWindowService {
		bool FocusWindow(IntPtr windowHandle);
		IntPtr SetActive(IntPtr windowHandle);
		bool TryGetWindowRect(HandleRef windowHandleRef, out Rectangle rectangle);
	}
}