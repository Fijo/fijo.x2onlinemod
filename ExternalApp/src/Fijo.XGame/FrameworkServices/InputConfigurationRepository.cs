using Fijo.Infrastructure.DesignPattern.Repository;
using FijoCore.Infrastructure.LightContrib.Default.Service.CheckName;
using FijoCore.Infrastructure.LightContrib.Module.Configuration;

namespace Fijo.XGame.FrameworkServices {
	public class InputConfigurationRepository : SingletonRepositoryBase<InputConfiguration> {
		private readonly IConfigurationService _configurationService;

		public InputConfigurationRepository(IConfigurationService configurationService) {
			_configurationService = configurationService;
		}

		#region Overrides of SingletonRepositoryBase<InputConfiguration>
		protected override InputConfiguration Create() {
			return new InputConfiguration
			       {
				       DelayBetweenSendInput = _configurationService.Get<int, InputConfiguration>(CN.Get<InputConfiguration, int>(x => x.DelayBetweenSendInput))
			       };
		}
		#endregion
	}
}