using System.Threading;
using Fijo.Infrastructure.DesignPattern.Repository;

namespace Fijo.XGame.FrameworkServices {
	public class InputHelper : IInputHelper {
		private readonly InputConfiguration _inputConfiguration;

		public InputHelper(IRepository<InputConfiguration> inputConfigurationRepository) {
			_inputConfiguration = inputConfigurationRepository.Get();
		}

		#region Implementation of IInputHelper
		public void WaitBetweenSendInput() {
			Thread.Sleep(_inputConfiguration.DelayBetweenSendInput);
		}
		#endregion
	}
}