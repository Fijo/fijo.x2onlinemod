namespace Fijo.XGame.FrameworkServices {
	public enum ScrollDirection : byte {
		Horizontal = 0,
		Vertical = 1
	}
}