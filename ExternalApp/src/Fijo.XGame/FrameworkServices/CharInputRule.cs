using System;
using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace Fijo.XGame.FrameworkServices {
	[Dto]
	public class CharInputRule {
		public readonly ICollection<SendKey> SendKeys;
		public readonly char Char;

		public CharInputRule(ICollection<SendKey> sendKeys, Char c) {
			SendKeys = sendKeys;
			Char = c;
		}
	}
}