using System.Collections.Generic;
using System.Globalization;
using JetBrains.Annotations;

namespace Fijo.XGame.FrameworkServices {
	public interface ICharInputRuleRepository {
		[NotNull]
		IEnumerable<CharInputRule> Get([NotNull] CultureInfo cultureInfo);
	}
}