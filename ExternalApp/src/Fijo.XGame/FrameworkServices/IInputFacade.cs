using System.Drawing;
using System.Globalization;
using JetBrains.Annotations;

namespace Fijo.XGame.FrameworkServices {
	public interface IInputFacade {
		void SetPosition(Point target);
		Point GetPosition();
		void SendClick(MouseFlags mouseFlags);
		void SendScroll(ScrollDirection scrollDirection, int movement, bool sendOneEvent = false);
		void SendKey(KeyCode keyCode, KeyEventFlags keyEventFlags);
		void SendKey(KeyCode keyCode);
		void SendKey(string value, [NotNull] CultureInfo cultureInfo);
		void SendKeysInvariant(string keys);
	}
}