using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using Fijo.XGame.Enums;

namespace Fijo.XGame.Dto {
	[Dto]
	public class Game {
		public readonly GameEnvironment Environment;
		public readonly Process Process;
		public GameState State;

		public Game(GameEnvironment environment, Process process, GameState state) {
			Environment = environment;
			Process = process;
			State = state;
		}
	}
}