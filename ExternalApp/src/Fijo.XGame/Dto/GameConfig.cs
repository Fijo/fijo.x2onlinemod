using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace Fijo.XGame.Dto {
	[Dto]
	public class GameConfig {
		public readonly ProcessPriorityClass GamePriority;
		public readonly WaitRules WaitRules;

		public GameConfig(ProcessPriorityClass gamePriority, WaitRules waitRules) {
			GamePriority = gamePriority;
			WaitRules = waitRules;
		}
	}
}