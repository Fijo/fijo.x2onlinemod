using System.Collections.Generic;
using System.Text;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace Fijo.XGame.Dto {
	[Dto]
	public class GameEnvironment {
		public readonly string GameDir;
		public readonly string X2BinaryRelativePath;
		public readonly IList<string> AdditionalVirtualGameDirs;
		public readonly Encoding LogFileEncoding;
		public readonly string GameDirForWrite;
		public readonly string GameDirForRead;
		public readonly Encoding ScriptEncoding;
		public IList<string> LoadedScripts;
		public readonly IList<string> AdditionalResourcesFolders;
		public readonly GameConfig Config;

		public GameEnvironment(string gameDir, IList<string> additionalVirtualGameDirs, string gameDirForWrite, string gameDirForRead, string x2BinaryRelativePath, Encoding logFileEncoding, Encoding scriptEncoding, IList<string> additionalResourcesFolders, GameConfig config) {
			GameDir = gameDir;
			X2BinaryRelativePath = x2BinaryRelativePath;
			AdditionalVirtualGameDirs = additionalVirtualGameDirs;
			LogFileEncoding = logFileEncoding;
			ScriptEncoding = scriptEncoding;
			GameDirForWrite = gameDirForWrite;
			GameDirForRead = gameDirForRead;
			AdditionalResourcesFolders = additionalResourcesFolders;
			Config = config;
		}
	}
}