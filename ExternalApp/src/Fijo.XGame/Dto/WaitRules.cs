using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using Fijo.XGame.Provider;

namespace Fijo.XGame.Dto {
	[Dto]
	public class WaitRules {
		private readonly IDictionary<WaitRule, int> _content;

		public WaitRules(IDictionary<WaitRule, int> content) {
			_content = content;
		}

		public int Get(WaitRule rule) {
			return _content[rule];
		}
	}
}