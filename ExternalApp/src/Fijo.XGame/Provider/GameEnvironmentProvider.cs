using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.XGame.Dto;
using Fijo.XGame.Interfaces;
using FijoCore.Infrastructure.LightContrib.Default.Service.CheckName;
using FijoCore.Infrastructure.LightContrib.Module.Configuration;

namespace Fijo.XGame.Provider {
	public class GameEnvironmentProvider : SingletonRepositoryBase<GameEnvironment> {
		private readonly IGameFactory _gameFactory;
		private readonly IConfigurationService _configurationService;
		private readonly IRepository<GameConfig> _gameConfigRepository;

		public GameEnvironmentProvider(IGameFactory gameFactory, IConfigurationService configurationService, IRepository<GameConfig> gameConfigRepository) {
			_gameFactory = gameFactory;
			_configurationService = configurationService;
			_gameConfigRepository = gameConfigRepository;
		}

		#region Overrides of SingletonRepositoryBase<GameEnvironment>
		protected override GameEnvironment Create() {
			return _gameFactory.Create(_configurationService.Get<string, GameEnvironment>("X2InstallationDir"),
			                           _configurationService.Get<string, GameEnvironment>("AdditionalVirtualGameDirs").Split(';'),
			                           _configurationService.Get<string, GameEnvironment>("GameDirForWrite"),
			                           _configurationService.Get<string, GameEnvironment>("GameDirForRead"),
			                           _configurationService.Get<string, GameEnvironment>("StartBinaryRelativePath"),
			                           _configurationService.Get<Encoding, GameEnvironment>("LogFileEncoding"),
			                           _configurationService.Get<Encoding, GameEnvironment>("ScriptEncoding"),
			                           _configurationService.Get<string, GameEnvironment>("AdditionalResourcesFolders").Split(';'),
			                           _gameConfigRepository.Get());
		}
		#endregion
	}

	
	public enum WaitRule {
		AfterGameStartedBeforeSkipVideos,//7000
		SkipVideosToGameMenu,//2500
		ForFocusingWindow, //100
		BetweenMainMenuActions, //1000
		MainMenuAnimation,//1000
		StartingGame,//10000
		LoadingSavegame,//7000
		BetweenMenuCommand, //100
		BeforeCommandInScriptEditor,//3000
		AfterExecuteScript,//3000
		DeletingScript,//300
		AfterLoadingScript,//300
		AfterUnlockingScriptEditor,//3000
		UnlockScriptEditorTyping//100
	}

	public class GameConfigProvider : RepositoryBase<GameConfig> {
		private readonly IConfigurationService _configurationService;
		private readonly IRepository<WaitRules> _waitRulesRepository;

		public GameConfigProvider(IConfigurationService configurationService, IRepository<WaitRules> waitRulesRepository) {
			_configurationService = configurationService;
			_waitRulesRepository = waitRulesRepository;
		}
		#region Overrides of RepositoryBase<GameConfig>
		public override GameConfig Get() {
			return new GameConfig(_configurationService.Get<ProcessPriorityClass, GameConfig>("GamePriority"),
			                      _waitRulesRepository.Get());
		}
		#endregion
	}

	public class WaitRulesProvider : RepositoryBase<WaitRules> {
		private readonly IConfigurationService _configurationService;
		private readonly string _waitRuleName = CN.Get<WaitRule>();

		public WaitRulesProvider(IConfigurationService configurationService) {
			_configurationService = configurationService;
		}

		#region Overrides of RepositoryBase<GameConfig>
		public override WaitRules Get() {
			return new WaitRules(GetWaitRules());
		}

		private IDictionary<WaitRule, int> GetWaitRules() {
			return Enum.GetValues(typeof (WaitRule)).Cast<WaitRule>().AsParallel().ToDictionary(x => x, WaitRuleValueSelector);
		}

		private int WaitRuleValueSelector(WaitRule x) {
			return _configurationService.Get<int, GameConfig>(string.Format("{0}.{1}", _waitRuleName, Enum.GetName(typeof (WaitRule), x)));
		}
		#endregion
	}
}