using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Fijo.XGame.Dto;
using Fijo.XGame.Interfaces;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic.IntoCollection;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Module.FileTools;
using FijoCore.Infrastructure.LightContrib.Module.Path;
using FijoCore.Infrastructure.LightContrib.Repositories;
using JetBrains.Annotations;
using KeyNotFoundException = FijoCore.Infrastructure.LightContrib.Default.Exceptions.KeyNotFoundException;

namespace Fijo.XGame.Services {
	public class GameEnvironmentService : IGameEnvironmentService {
		private readonly IAllFileRepository _allFileRepository;
		private readonly IFileFilterService _fileFilterService;
		private readonly IPathService _pathService;

		public GameEnvironmentService(IAllFileRepository allFileRepository, IFileFilterService fileFilterService, IPathService pathService) {
			_allFileRepository = allFileRepository;
			_fileFilterService = fileFilterService;
			_pathService = pathService;
		}

		public int GetScriptPosition(GameEnvironment gameEnvironment, string name) {
			var loadedScripts = gameEnvironment.LoadedScripts;
			int scriptPosition;
			lock(loadedScripts) scriptPosition = loadedScripts.OrderBy(x => x).IndexOf(name);
			if(scriptPosition == -1) throw new ScriptNotFoundException(loadedScripts, name, "The script with the name you used is not loaded");
			return scriptPosition;
		}

		public void UpdateLoadedScripts(GameEnvironment gameEnvironment) {
			gameEnvironment.LoadedScripts = GetScriptDirs(gameEnvironment).AsParallel()
				.SelectMany(x => _fileFilterService.FilterByExt(_allFileRepository.Get(x), "xml"))
				.Select(Path.GetFileNameWithoutExtension)
				.ToList();
		}

		public void AddLoadedScript(GameEnvironment gameEnvironment, string name) {
			if (GetScriptFilepaths(gameEnvironment, name).None(File.Exists)) return;
			var loadedScripts = gameEnvironment.LoadedScripts;
			lock(loadedScripts) loadedScripts.Add(name);
		}

		private IEnumerable<string> GetScriptFilepaths(GameEnvironment gameEnvironment, string name) {
			return GetScriptDirs(gameEnvironment).Select(x => Path.Combine(x, string.Format("{0}.xml", name)));
		}

		public void RemoveLoadedScript(GameEnvironment gameEnvironment, string name) {
			var loadedScripts = gameEnvironment.LoadedScripts;
			lock(loadedScripts) loadedScripts.Remove(name);
		}

		public IEnumerable<string> GetScriptDirs(GameEnvironment gameEnvironment) {
			return GetGameDirs(gameEnvironment).Select(GetScriptDir);
		}

		private string GetScriptDir(string x) {
			return Path.Combine(x, "scripts");
		}

		public string GetScriptDirForWrite(GameEnvironment gameEnvironment) {
			return GetScriptDir(gameEnvironment.GameDirForWrite);
		}

		public IEnumerable<string> GetGameDirs(GameEnvironment gameEnvironment) {
			return gameEnvironment.GameDir.IntoEnumerable().Concat(gameEnvironment.AdditionalVirtualGameDirs).Distinct();
		}

		public void InstallScript(GameEnvironment gameEnvironment, string scriptFilePath) {
			var scriptName = Path.GetFileName(scriptFilePath);
			File.Copy(scriptFilePath,
			          Path.Combine(GetScriptDirForWrite(gameEnvironment), scriptName), true);
		}

		public void CleanUpScript(GameEnvironment gameEnvironment, string scriptName) {
			File.Delete(Path.Combine(GetScriptDirForWrite(gameEnvironment), string.Format("{0}.xml", scriptName)));
		}

		#region IncludeResources
		class ResourceLoadInfo {
			public string SourcePath { get; set; }
			public string RelativePath { get; set; }
		}

		private ResourceLoadInfo CreateResourceLoadInfo(string sourcePath, string relativePath) {
			return new ResourceLoadInfo {SourcePath = sourcePath, RelativePath = relativePath};
		}

		public void IncludeResources(GameEnvironment gameEnvironment, string folderName) {
			Parallel.ForEach(Get(gameEnvironment, folderName),
			                 x => IncludeResource(gameEnvironment, x));
		}

		private void IncludeResource(GameEnvironment gameEnvironment, ResourceLoadInfo x) {
			File.Copy(x.SourcePath, Path.Combine(gameEnvironment.GameDirForWrite, x.RelativePath), true);
		}

		private IEnumerable<ResourceLoadInfo> Get(GameEnvironment gameEnvironment, string folderName) {
			return gameEnvironment.AdditionalResourcesFolders
				.AsParallel()
				.SelectMany(basePath => Directory.GetDirectories(basePath)
								.AsParallel()
								.SelectMany(subDict => {
								var relativeBasePath = _pathService.GetPathRelativeTo(subDict, basePath);
									            return Directory.GetDirectories(subDict)
										            .AsParallel()
										            .Where(pathWithFolderName => _pathService.GetPathRelativeTo(pathWithFolderName, subDict) == folderName)
										            .SelectMany(fullPath => Directory.GetFiles(fullPath).AsParallel()
											                                    .Select(file => CreateResourceLoadInfo(file, Path.Combine(relativeBasePath, Path.GetFileName(file)))));
								            }));
		}
		#endregion
	}

	[Serializable, PublicAPI]
	public class ScriptNotFoundException : KeyNotFoundException {
		public ScriptNotFoundException(object source, string key, string message = default(string), Exception innerException = null) : base(source, (object)key, message, innerException) {}
	}
}