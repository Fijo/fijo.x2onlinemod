using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.XGame.Dto;
using Fijo.XGame.Enums;
using Fijo.XGame.Exceptions;
using Fijo.XGame.FrameworkServices;
using Fijo.XGame.Interfaces;
using Fijo.XGame.Provider;
using FijoCore.Infrastructure.LightContrib.Default.Service.CheckName;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using JetBrains.Annotations;

namespace Fijo.XGame.Services {
	public class GameAccessSerivce : IGameAccessSerivce {
		private readonly IWindowService _windowService;
		private readonly IInputFacade _inputFacade;
		private readonly IInputHelper _inputHelper;
		private readonly IGameEnvironmentService _gameEnvironmentService;
		private readonly string _startInvalidStateMessage = string.Format("Please call {0} only and only once after creating the Game using {1}.{2}()", CN.Get<IGameAccessSerivce>(x => x.Start(null)), CN.Get(typeof(IGameFactory)), CN.Get<IGameFactory>(x => x.Create(null)));
		private readonly string _loadSavegameInvalidStateMessage = string.Format("Please call {0}.{1}() before calling this method.", CN.Get(typeof(IGameAccessSerivce)), CN.Get<IGameAccessSerivce>(x => x.Start(null)));
		private readonly string _ensureAllClosedInvalidStatusMessage = string.Format("Please call {0}.{1}() before calling this method.", CN.Get(typeof(IGameAccessSerivce)), CN.Get<IGameAccessSerivce>(x => x.LoadSavegame(null, default(int))));
		private readonly GameState[] _allowedGameStatesForExit = Enum.GetValues(typeof (GameState)).Cast<GameState>().Where(x => !x.IsIn(GameState.NotStarted, GameState.Closed)).ToArray();

		public GameAccessSerivce(IWindowService windowService, IInputFacade inputFacade, IInputHelper inputHelper, IGameEnvironmentService gameEnvironmentService) {
			_windowService = windowService;
			_inputFacade = inputFacade;
			_inputHelper = inputHelper;
			_gameEnvironmentService = gameEnvironmentService;
		}

		#region Implementation of IGameAccessSerivce
		#region Start
		public void Start(Game game) {
			lock(game) {
				#region PreCondition
				ValidateGameState(game, _startInvalidStateMessage, GameState.NotStarted);
				#endregion
				var process = game.Process;
				process.Start();
				process.PriorityClass = game.Environment.Config.GamePriority;
				StartGameWindow(process);
				SkipVideos(game);
				game.State = GameState.InMenu;
			}
		}

		private void StartGameWindow(Process process) {
			EnsureFocus(process);
			_inputFacade.SendKey(KeyCode.Enter);
		}

		private void SkipVideos(Game game) {
			var waitRules = game.Environment.Config.WaitRules;
			var process = game.Process;
			Thread.Sleep(waitRules.Get(WaitRule.AfterGameStartedBeforeSkipVideos));
			_windowService.FocusWindow(process.MainWindowHandle);
			Thread.Sleep(waitRules.Get(WaitRule.ForFocusingWindow));
			SendClick(process);
			_inputFacade.SendKey(KeyCode.Escape);
			WaitInputIdel(process);
			Thread.Sleep(waitRules.Get(WaitRule.SkipVideosToGameMenu));
		}

		private void SendClick(Process process) {
			// the cursor is always in the window when it is focused in x2 so we dont have to move it
			process.ForceWaitForInputIdle();
			_inputFacade.SendClick(MouseFlags.Left);
		}
		#endregion

		#region Exit
		public void Exit(Game game) {
			lock(game) {
				#region PreCondition
				ValidateGameState(game, _startInvalidStateMessage, _allowedGameStatesForExit);
				#endregion
				game.Process.Kill();
				game.State = GameState.Closed;
			}
		}
		#endregion

		#region GotoShipCockpit
		public void GotoShipCockpit(Game game) {
			lock(game) {
				#region PreCondition
				ValidateGameState(game, _ensureAllClosedInvalidStatusMessage, GameState.InGame);
				#endregion
				SendKey(game, KeyCode.Backspace);
				for(var i = 0; i < 8; i++) SendKey(game, KeyCode.Escape);
				SendKey(game, KeyCode.KeyF);
				for(var i = 0; i < 2; i++) SendKey(game, KeyCode.Escape);
				SendKey(game, KeyCode.KeyF);
				SendKey(game, KeyCode.Escape);
			}
		}
		#endregion

		#region GotoScriptEditor
		public void GotoScriptEditor(Game game) {
			lock(game) {
				GotoShipCockpit(game);
				SendKey(game, KeyCode.KeyS);
				SendKey(game, KeyCode.KeyC);
				SendKey(game, KeyCode.KeyS);
				SendKey(game, KeyCode.Enter);
			}
		}
		#endregion

		#region RunScript
		[Note("you have to call this after GotoScriptEditor() having the gane locked at least before calling GotoScriptEditor() and do not free the lock before the execution of this function has ended. You should altought Call this func as soon as possible after calling GotoScriptEditor().")]
		public void RunScript(Game game, string name) {
			lock(game) {
				#region PreCondition
				ValidateGameState(game, _ensureAllClosedInvalidStatusMessage, GameState.InGame);
				#endregion
				RunScript(game, _gameEnvironmentService.GetScriptPosition(game.Environment, name));
			}
		}

		private void RunScript(Game game, int index) {
			var process = game.Process;
			WaitInputIdel(process);
			SendScroll(game, ScrollDirection.Vertical, -(index + 1));
			Wait(game, WaitRule.BeforeCommandInScriptEditor);
			SendKey(game, KeyCode.KeyR);
			SendKey(game, KeyCode.Enter);
			SendKey(game, KeyCode.Enter);
			Wait(game, WaitRule.AfterExecuteScript);
			SendScroll(game, ScrollDirection.Vertical, index + 1);
		}
		#endregion

		#region DeleteScript
		[Note("you have to call this after GotoScriptEditor() having the gane locked at least before calling GotoScriptEditor() and do not free the lock before the execution of this function has ended. You should altought Call this func as soon as possible after calling GotoScriptEditor().")]
		public void DeleteScript(Game game, string name) {
			lock(game) {
				#region PreCondition
				ValidateGameState(game, _ensureAllClosedInvalidStatusMessage, GameState.InGame);
				#endregion
				var environment = game.Environment;
				DeleteScript(game, _gameEnvironmentService.GetScriptPosition(environment, name));
				_gameEnvironmentService.RemoveLoadedScript(environment, name);
				Wait(game, WaitRule.DeletingScript);
			}
		}

		private void DeleteScript(Game game, int index) {
			var process = game.Process;
			WaitInputIdel(process);
			SendScroll(game, ScrollDirection.Vertical, -(index + 1));
			Wait(game, WaitRule.BeforeCommandInScriptEditor);
			SendClick(game, MouseFlags.Right);
			SendScroll(game, ScrollDirection.Vertical, -1);
			SendKey(game, KeyCode.Enter);
		}
		#endregion

		#region LoadScript
		[Note("you have to call this after GotoScriptEditor() having the gane locked at least before calling GotoScriptEditor() and do not free the lock before the execution of this function has ended. You should altought Call this func as soon as possible after calling GotoScriptEditor().")]
		public void LoadScript(Game game, string name, CultureInfo cultureInfo) {
			lock(game) {
				#region PreCondition
				ValidateGameStateInGame(game);
				#endregion
				var process = game.Process;
				WaitInputIdel(process);
				SendKey(game, KeyCode.KeyL);
				WaitInputIdel(process);
				SendKey(game, name, cultureInfo);
				Wait(game, WaitRule.BeforeCommandInScriptEditor);
				SendKey(game, KeyCode.Enter);
				_gameEnvironmentService.AddLoadedScript(game.Environment, name);
				Wait(game, WaitRule.AfterLoadingScript);
			}
		}
		#endregion

		#region LoadSavegame
		public void LoadSavegame(Game game, int index) {
			lock(game) {
				#region PreCondition
				ValidateGameState(game, _loadSavegameInvalidStateMessage, GameState.InMenu);
				#endregion
				var process = game.Process;
				EnsureFocus(process);
				_inputFacade.SendScroll(ScrollDirection.Vertical, -1);
				WaitMenuActionInputIdel(game);
				_inputFacade.SendKey(KeyCode.Enter);
				WaitMenuActionInputIdel(game);
				_inputFacade.SendScroll(ScrollDirection.Vertical, 22);
				WaitMenuActionInputIdel(game);
				_inputFacade.SendScroll(ScrollDirection.Vertical, -index);
				WaitMenuActionInputIdel(game);
				_inputFacade.SendKey(KeyCode.Enter);
				StartGame(game);
				WaitInputIdel(process);
				Wait(game, WaitRule.LoadingSavegame);
				game.State = GameState.InGame;
			}
		}
		#endregion
		
		#region StartGame
		public void StartGame(Game game, int index) {
			lock(game) {
				#region PreCondition
				ValidateGameState(game, _loadSavegameInvalidStateMessage, GameState.InMenu);
				#endregion
				var process = game.Process;
				EnsureFocus(process);
				_inputFacade.SendKey(KeyCode.Enter);
				Wait(game, WaitRule.MainMenuAnimation);
				WaitMenuActionInputIdel(game);
				_inputFacade.SendScroll(ScrollDirection.Vertical, -3);
				WaitMenuActionInputIdel(game);
				_inputFacade.SendKey(KeyCode.Enter);
				WaitMenuActionInputIdel(game);
				_inputFacade.SendScroll(ScrollDirection.Vertical, -index);
				WaitMenuActionInputIdel(game);
				_inputFacade.SendKey(KeyCode.Enter);
				WaitMenuActionInputIdel(game);
				_inputFacade.SendKey(KeyCode.Enter);
				StartGame(game);
				process.ForceWaitForInputIdle();
				Wait(game, WaitRule.StartingGame);
				game.State = GameState.InGame;
			}
		}
		#endregion

		#region UnlockScriptEditor
		public void UnlockScriptEditor(Game game) {
			lock(game) {
				GotoShipCockpit(game);
				var process = game.Process;
				EnsureFocus(process);
				"Thereshallbewings".ForEachAndBetween(x => SendKey(game, new string(x, 1), CultureInfo.InvariantCulture), () => Wait(game, WaitRule.UnlockScriptEditorTyping));
				Wait(game, WaitRule.AfterUnlockingScriptEditor);
				GotoScriptEditor(game);
				SendKey(game, KeyCode.Enter);
				GotoShipCockpit(game);
			}
		}
		#endregion
		#endregion

		protected void StartGame(Game game) {
			_gameEnvironmentService.UpdateLoadedScripts(game.Environment);
		}

		private void ValidateGameStateInGame(Game game) {
			ValidateGameState(game, _ensureAllClosedInvalidStatusMessage, GameState.InGame);
		}

		private void ValidateGameState(Game game, string errorMessage, [NotNull] params GameState[] allowedGameStates) {
			if (!game.State.IsIn(allowedGameStates)) throw new InvalidGameStateException(allowedGameStates, game, errorMessage);
		}

		private void SendKey(Game game, KeyCode keyCode) {
			WaitBetweenMenuCommand(game.Environment.Config.WaitRules);
			WaitInputIdel(game.Process);
			_inputFacade.SendKey(keyCode);
		}

		private void WaitBetweenMenuCommand(WaitRules waitRules) {
			Thread.Sleep(waitRules.Get(WaitRule.BetweenMenuCommand));
			_inputHelper.WaitBetweenSendInput();
		}

		private void SendKey(Game game, string keys, CultureInfo cultureInfo) {
			WaitBetweenMenuCommand(game.Environment.Config.WaitRules);
			WaitInputIdel(game.Process);
			_inputFacade.SendKey(new string(keys.Select(CorrectYZChar).ToArray()), cultureInfo);
		}

		//todo try to solve this better this is hacky
		private char CorrectYZChar(char c) {
			switch (c) {
				case 'y':
					return 'z';
				case 'Y':
					return 'Z';
				case 'z':
					return 'y';
				case 'Z':
					return 'Y';
				default:
					return c;
			}
		}

		private void SendClick(Game game, MouseFlags mouseFlags) {
			WaitBetweenMenuCommand(game.Environment.Config.WaitRules);
			WaitInputIdel(game.Process);
			_inputFacade.SendClick(mouseFlags);
		}
		
		private void SendScroll(Game game, ScrollDirection scrollDirection, int movement, bool sendOneEvent = false) {
			WaitBetweenMenuCommand(game.Environment.Config.WaitRules);
			WaitInputIdel(game.Process);
			_inputFacade.SendScroll(scrollDirection, movement, sendOneEvent);
		}

		private void WaitMenuActionInputIdel(Game game) {
			Wait(game, WaitRule.BetweenMainMenuActions);
			WaitInputIdel(game.Process);
		}

		private void WaitInputIdel(Process process) {
			process.ForceWaitForInputIdle();
		}

		private void EnsureFocus(Process process) {
			process.ForceWaitForInputIdle();
			_windowService.FocusWindow(process.MainWindowHandle);
			process.ForceWaitForInputIdle();
		}

		private void Wait(Game game, WaitRule waitRule) {
			Thread.Sleep(game.Environment.Config.WaitRules.Get(waitRule));
		}
	}
}