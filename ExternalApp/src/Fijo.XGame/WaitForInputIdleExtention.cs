using System;
using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace Fijo.XGame {
	[PublicAPI]
	public static class WaitForInputIdleExtention {
		[PublicAPI, RelatedLink("http://msdn.microsoft.com/de-de/library/8d7363e2(v=vs.80).aspx")]
		public static void ForceWaitForInputIdle([NotNull] this Process me) {
			if(!me.WaitForInputIdle()) throw new InvalidOperationException("the process do not support WaitForInputIdle().");
		}
	}
}