using System;
using System.Collections.Generic;
using Fijo.XGame.Dto;
using Fijo.XGame.Enums;
using JetBrains.Annotations;

namespace Fijo.XGame.Exceptions {
	[Serializable]
	public class InvalidGameStateException : InvalidOperationException {
		public IList<GameState> ValidGameState { get; private set; }
		public GameState ActualGameState { get { return Game.State; } }
		public Game Game { get; private set; }

		public InvalidGameStateException([NotNull] IList<GameState> validGameStates, [NotNull] Game game, [NotNull] string message, Exception innerException = null) : base(message, innerException) {
			ValidGameState = validGameStates;
			Game = game;
		}
	}
}