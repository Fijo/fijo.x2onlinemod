using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using Fijo.XGame.Dto;
using Fijo.XGame.Enums;
using Fijo.XGame.Interfaces;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;

namespace Fijo.XGame.Factories {
	public class GameFactory : IGameFactory {
		private readonly IGameEnvironmentService _gameEnvironmentService;
		private const string ReadWriteNotContainedArgExceptionMessage = "must be one of the game dirs (´gameDir´, or one entry of ´additionalVirtualGameDirs´)";

		public GameFactory(IGameEnvironmentService gameEnvironmentService) {
			_gameEnvironmentService = gameEnvironmentService;
		}

		public GameEnvironment Create(string gameDir, IList<string> additionalVirtualGameDirs, string gameDirForWrite, string gameDirForRead, string x2BinaryRelativePath, Encoding logFileEncoding, Encoding scriptEncoding, IList<string> foldersToLoadAdditionalResources, GameConfig config) {
			var gameEnvironment = new GameEnvironment(gameDir, additionalVirtualGameDirs, gameDirForWrite, gameDirForRead, x2BinaryRelativePath, logFileEncoding, scriptEncoding, foldersToLoadAdditionalResources, config);
			var gameDirs = _gameEnvironmentService.GetGameDirs(gameEnvironment).Execute();
			if(!gameDirs.Contains(gameDirForWrite)) throw new ArgumentException(ReadWriteNotContainedArgExceptionMessage, "gameDirForWrite");
			if(!gameDirs.Contains(gameDirForRead)) throw new ArgumentException(ReadWriteNotContainedArgExceptionMessage, "gameDirForRead");
			return gameEnvironment;
		}

		public Game Create(GameEnvironment gameEnvironment) {
			var workingDirectory = gameEnvironment.GameDir;
			var proc = CreateProcess(workingDirectory, Path.Combine(workingDirectory, gameEnvironment.X2BinaryRelativePath));
			return new Game(gameEnvironment, proc, GameState.NotStarted);
		}
		
		private Process CreateProcess(string workingDirectory, string command) {
			var procStartInfo = GetProcessStartInfo(workingDirectory, command);
			var proc = new Process {StartInfo = procStartInfo};
			return proc;
		}

		private ProcessStartInfo GetProcessStartInfo(string workingDirectory, string command) {
			return new ProcessStartInfo(command) {RedirectStandardOutput = false, UseShellExecute = false, CreateNoWindow = false, WorkingDirectory = workingDirectory};
		}
	}
}