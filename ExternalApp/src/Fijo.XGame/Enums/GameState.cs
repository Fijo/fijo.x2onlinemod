namespace Fijo.XGame.Enums {
	public enum GameState {
		NotStarted,
		InMenu,
		InGame,
		Closed
	}
}