using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.XGame.Dto;
using Fijo.XGame.Factories;
using Fijo.XGame.Interfaces;
using Fijo.XGame.Provider;
using Fijo.XGame.Services;
using FijoCore.Infrastructure.DependencyInjection.Extentions.IKernel;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Base;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;

namespace Fijo.XGame.Properties {
	public class XGameInjectionModule : ExtendedNinjectModule {
		public override void AddModule(IKernel kernel) {
			kernel.Load(new GuiComInjectionModule());
		}

		public override void OnLoad(IKernel kernel) {
			kernel.Bind<IGameAccessSerivce>().To<GameAccessSerivce>().InSingletonScope();
			kernel.Bind<IGameFactory>().To<GameFactory>().InSingletonScope();

			kernel.Bind<IRepository<WaitRules>>().To<WaitRulesProvider>().InSingletonScope();
			kernel.Bind<IRepository<GameConfig>>().To<GameConfigProvider>().InSingletonScope();
			kernel.Bind<IRepository<GameEnvironment>>().To<GameEnvironmentProvider>().InSingletonScope();

			kernel.Bind<IGameEnvironmentService>().To<GameEnvironmentService>().InSingletonScope();
		}
	}
}