using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.XGame.FrameworkServices;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Base;
using FijoCore.Infrastructure.LightContrib.Properties;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;
using FijoCore.Infrastructure.DependencyInjection.Extentions.IKernel;

namespace Fijo.XGame.Properties {
	public class GuiComInjectionModule : ExtendedNinjectModule {
		public override void AddModule(IKernel kernel) {
			kernel.Load(new LightContribInjectionModule());
		}

		public override void OnLoad(IKernel kernel) {
			kernel.Bind<IWindowService>().To<WindowService>().InSingletonScope();

			kernel.Bind<IInputAdapter>().To<InputAdapter>().InSingletonScope();

			kernel.Bind<IRepository<InputConfiguration>>().To<InputConfigurationRepository>().InSingletonScope();

			kernel.Bind<IInputHelper>().To<InputHelper>().InSingletonScope();
			kernel.Bind<IInputService>().To<InputService>().InSingletonScope();

			kernel.Bind<ICharInputRuleRepository>().To<CharInputRuleRepository>().InSingletonScope();

			kernel.Bind<ICharInputProvider>().To<CharInputProvider>().InSingletonScope();

			kernel.Bind<ICharInputService>().To<CharInputService>().InSingletonScope();
			kernel.Bind<IStringInputService>().To<StringInputService>().InSingletonScope();

			kernel.Bind<IInputFacade>().To<InputFacade>().InSingletonScope();
		}
	}
}