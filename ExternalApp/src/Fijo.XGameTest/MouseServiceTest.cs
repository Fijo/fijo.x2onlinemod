using Fijo.XGame.FrameworkServices;
using NUnit.Framework;

namespace Fijo.XGameTest {
	[TestFixture]
	public class InputServiceTest {
		[Test, Ignore]
		public void ClickTest() {
			var inputService = new InputAdapter();
			inputService.SendClick(MouseFlags.Right | MouseFlags.Down);
			inputService.SendClick(MouseFlags.Right | MouseFlags.Up);
		}

		[Test, Ignore]
		public void KeyTest() {
			var inputService = new InputAdapter();
			inputService.SendKey(KeyCode.KeyJ, KeyEventFlags.Down);
			inputService.SendKey(KeyCode.KeyJ, KeyEventFlags.Up);
		}

		[Test, Ignore]
		public void ScrollTest() {
			var inputService = new InputAdapter();
			inputService.SendScroll(ScrollDirection.Vertical, -1);
		}
	}
}