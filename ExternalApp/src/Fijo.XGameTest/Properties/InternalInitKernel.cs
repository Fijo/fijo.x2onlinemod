using Fijo.XGame.Properties;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Init;

namespace Fijo.XGameTest.Properties {
	public class InternalInitKernel : ExtendedInitKernel {
		public override void PreInit() {
			LoadModules(new XGameInjectionModule());
		}
	}
}