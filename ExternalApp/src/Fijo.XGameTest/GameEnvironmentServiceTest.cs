using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.XGame.Dto;
using Fijo.XGame.Interfaces;
using Fijo.XGameTest.Mocks;
using Fijo.XGameTest.Properties;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Repositories;
using NUnit.Framework;

namespace Fijo.XGameTest {
	[TestFixture]
	public class GameEnvironmentServiceTest {
		private IGameEnvironmentService _gameEnvironmentService;
		private GameEnvironment _gameEnvironment;

		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
			Kernel.Inject.Rebind<IAllFileRepository>().To<AllFileRepositoryMock>().InSingletonScope();
			_gameEnvironment = Kernel.Resolve<IRepository<GameEnvironment>>().Get();
			_gameEnvironmentService = Kernel.Resolve<IGameEnvironmentService>();
		}
		
		[TestCase("Fijo.Repos.GetConf", 0)]
		[TestCase("Fijo.Repos.GetSectors", 1)]
		[TestCase("Fijo.Repos.GetShip", 2)]
		[TestCase("Fijo.Repos.GetShips", 3)]
		[TestCase("Fijo.Repos.GetStation", 4)]
		[TestCase("Fijo.Repos.GetStations", 5)]
		[TestCase("Fijo.Repos.InternGetSectors", 6)]
		[TestCase("Fijo.Services.Array.Append", 7)]
		[TestCase("Fijo.Services.Gate.GetNearest", 8)]
		[TestCase("Fijo.Services.Gates.Get", 9)]
		[TestCase("Fijo.Services.Gates.ToJump", 10)]
		[TestCase("Fijo.Services.InternMove", 11)]
		[TestCase("Fijo.Services.Jump", 12)]
		[TestCase("Fijo.Services.Move", 13)]
		[TestCase("Fijo.Services.Move.ToPosition", 14)]
		[TestCase("Fijo.Services.Move.ToSector", 15)]
		[TestCase("Fijo.Services.Move.ToStation", 16)]
		[TestCase("Fijo.Services.Sector.GetRandom", 17)]
		[TestCase("Fijo.Services.Sectors.GetWithout", 18)]
		[TestCase("Fijo.Services.Ship.Serialize", 19)]
		[TestCase("Fijo.Services.Ships.Serialize", 20)]
		[TestCase("Fijo.Services.Tmp.RaceLogic.Off", 21)]
		[TestCase("Fijo.Services.Tmp.RaceLogic.On", 22)]
		[TestCase("Fijo.Services.Tmp.Tune.Off", 23)]
		[TestCase("Fijo.Services.Tmp.Tune.On", 24)]
		[TestCase("Fijo.Store.Conf.Get", 25)]
		[TestCase("Fijo.Store.Conf.InternGetKey", 26)]
		[TestCase("Fijo.Store.Conf.Set", 27)]
		[TestCase("Fijo.Store.Global.Get", 28)]
		[TestCase("Fijo.Store.Global.Set", 29)]
		[TestCase("Fijo.Store.Ship.Get", 30)]
		[TestCase("Fijo.Store.Ship.InternGetKey", 31)]
		[TestCase("Fijo.Store.Ship.Set", 32)]
		[TestCase("FijoTest.LogAssertationResult", 33)]
		[TestCase("FijoTest.Repos.GetShip", 34)]
		[TestCase("FijoTest.Repos.GetStation", 35)]
		[TestCase("FijoTest.Services.Move.ToPos", 36)]
		[TestCase("FijoTest.Services.Move.ToSector", 37)]
		[TestCase("FijoTest.Services.Move.ToStation", 38)]
		[TestCase("FijoTest.Services.Tmp.RL.Off", 39)]
		[TestCase("FijoTest.Services.Tmp.RL.On", 40)]
		[TestCase("FijoTest.Services.Tmp.T.OffBase", 41)]
		[TestCase("FijoTest.Services.Tmp.T.OnBase", 42)]
		[TestCase("FijoTest.Services.Tmp.Tune.Off0", 43)]
		[TestCase("FijoTest.Services.Tmp.Tune.Off1", 44)]
		[TestCase("FijoTest.Services.Tmp.Tune.On0", 45)]
		[TestCase("FijoTest.Services.Tmp.Tune.On1", 46)]
		[TestCase("FijoTest.Store.Conf", 47)]
		[TestCase("FijoTest.Store.Global", 48)]
		[TestCase("FijoTest.Store.Ship", 49)]
		[TestCase("FijoTest.TestData.Ship", 50)]
		[TestCase("FijoTest.TestData.Station", 51)]
		[TestCase("FijoTest.TestData.StationInSec", 52)]
		[TestCase("galaxy.Example.initplayership", 53)]
		[TestCase("io.correct.ship", 54)]
		[TestCase("io.register.outInterval", 55)]
		[TestCase("lib.delivery.wares", 56)]
		[TestCase("lib.detectenemy.trader", 57)]
		[TestCase("lib.fly.to.repdock", 58)]
		[TestCase("lib.move.jumptostation", 59)]
		[TestCase("lib.setup.product.placement", 60)]
		[TestCase("lib.ship.sh.attack.trader.flee", 61)]
		[TestCase("lib.ship.signal.attacked.trader", 62)]
		[TestCase("lib.ship.signal.killed.trader", 63)]
		[TestCase("lib.ship.var.setpilotname", 64)]
		[TestCase("lib.sprintf.format.gametime", 65)]
		[TestCase("lib.Station.money.stat2station", 66)]
		[TestCase("plugin.aj.svp.check1", 67)]
		[TestCase("plugin.aj.svp.check2", 68)]
		[TestCase("plugin.aj.svp.check3", 69)]
		[TestCase("plugin.aj.svp.config", 70)]
		[TestCase("plugin.aj.svp.debt", 71)]
		[TestCase("plugin.aj.svp.deluxe", 72)]
		[TestCase("plugin.aj.svp.end", 73)]
		[TestCase("plugin.aj.svp.eoptionen", 74)]
		[TestCase("plugin.aj.svp.getmoney", 75)]
		[TestCase("plugin.aj.svp.hbships", 76)]
		[TestCase("plugin.aj.svp.lite", 77)]
		[TestCase("plugin.aj.svp.log.global", 78)]
		[TestCase("plugin.aj.svp.log.local", 79)]
		[TestCase("plugin.aj.svp.money", 80)]
		[TestCase("plugin.aj.svp.name", 81)]
		[TestCase("plugin.aj.svp.numbers", 82)]
		[TestCase("plugin.aj.svp.paydebt", 83)]
		[TestCase("plugin.aj.svp.pricecheck", 84)]
		[TestCase("plugin.aj.svp.time", 85)]
		[TestCase("plugin.autotrade.buyupgrades", 86)]
		[TestCase("plugin.autotrade.checksector", 87)]
		[TestCase("plugin.autotrade.delivery", 88)]
		[TestCase("plugin.autotrade.delivery.end", 89)]
		[TestCase("plugin.autotrade.delivery.equip", 90)]
		[TestCase("plugin.autotrade.detectenemy", 91)]
		[TestCase("plugin.autotrade.exceptions", 92)]
		[TestCase("plugin.autotrade.fuel", 93)]
		[TestCase("plugin.autotrade.galaxy", 94)]
		[TestCase("plugin.autotrade.galaxycheck", 95)]
		[TestCase("plugin.autotrade.jumptostation", 96)]
		[TestCase("plugin.autotrade.lockmaster", 97)]
		[TestCase("plugin.autotrade.lockrequest", 98)]
		[TestCase("plugin.autotrade.logging", 99)]
		[TestCase("plugin.autotrade.movepilot", 100)]
		[TestCase("plugin.autotrade.movepilot.start", 101)]
		[TestCase("plugin.autotrade.moveware", 102)]
		[TestCase("plugin.autotrade.profit", 103)]
		[TestCase("plugin.autotrade.sector", 104)]
		[TestCase("plugin.autotrade.sectorcheck", 105)]
		[TestCase("plugin.autotrade.versionupdater", 106)]
		[TestCase("plugin.KriMe.SDS.init", 107)]
		[TestCase("plugin.KriMe.SDSone", 108)]
		[TestCase("plugin.KriMe.SDSthree", 109)]
		[TestCase("plugin.KriMe.SDStwo", 110)]
		public void CorrectScriptName_GetScriptPosition_ReturnsCorrectIndex(string name, int index) {
			var gameFactory = Kernel.Resolve<IGameFactory>();
			var game = gameFactory.Create(_gameEnvironment);
			var gameEnvironment = game.Environment;
			_gameEnvironmentService.UpdateLoadedScripts(gameEnvironment);
			var actual = _gameEnvironmentService.GetScriptPosition(gameEnvironment, name);
			Assert.AreEqual(index, actual);
		}
	}
}