using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using Fijo.Infrastructure.DesignPattern.Mapping;
using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.X2OnlineMod;
using Fijo.X2OnlineMod.Commands;
using Fijo.X2OnlineMod.Dto;
using Fijo.X2OnlineMod.Enums;
using Fijo.X2OnlineMod.Enums.Constants;
using Fijo.X2OnlineMod.Properties;
using Fijo.XGame.Dto;
using Fijo.XGame.Interfaces;
using Fijo.XGame.Script.Commands;
using Fijo.XGame.Script.Dto;
using Fijo.XGame.Script.Enums;
using Fijo.XGame.Script.Interfaces;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using NUnit.Framework;

namespace Fijo.X2OnlineModTest {
	[TestFixture]
	public class LolXScriptTest {
		private IGameFactory _gameFactory;
		private GameEnvironment _gameEnvironment;
		private IGameAccessSerivce _gameAccessSerivce;
		private IRepository<IEnumerable<string[]>> _inputRepository;
		private IMapping<string[], Ship> _shipMapping;
		private IScriptStoreService _scriptStoreService;
		private IScriptService _scriptService;
		private IScriptFactory _scriptFactory;

		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
			_gameFactory = Kernel.Resolve<IGameFactory>();
			_gameEnvironment = Kernel.Resolve<IRepository<GameEnvironment>>().Get();
			_gameAccessSerivce = Kernel.Resolve<IGameAccessSerivce>();
			_inputRepository = Kernel.Resolve<IRepository<IEnumerable<string[]>>>();
			_shipMapping = Kernel.Resolve<IMapping<string[], Ship>>();
			_scriptStoreService = Kernel.Resolve<IScriptStoreService>();
			_scriptService = Kernel.Resolve<IScriptService>();
			_scriptFactory = Kernel.Resolve<IScriptFactory>();
		}

		[Test]
		public void Test() {
			var game = _gameFactory.Create(_gameEnvironment);
			_gameAccessSerivce.Start(game);
			_gameAccessSerivce.LoadSavegame(game, 4);
			lock (game) {
				_gameAccessSerivce.GotoScriptEditor(game);
				_gameAccessSerivce.RunScript(game, "Fijo.Services.Ships.Serialize");
				Thread.Sleep(20000);
				var ships = _inputRepository.Get().Select(_shipMapping.Map).ToList();
				var playerShip = ships.Single(x => x.Race == Race.Player);
				var targetSector = playerShip.Sector;
				var script = GetScript(ships, targetSector, new Position(100000, 100000, 100000));
				_scriptStoreService.Store(script);
				_gameAccessSerivce.LoadScript(game, "FijoTest.Dyn.LolUpdateShips", CultureInfo.InvariantCulture);
				_gameAccessSerivce.RunScript(game, "FijoTest.Dyn.LolUpdateShips");
				Thread.Sleep(60000);

				_gameAccessSerivce.DeleteScript(game, "FijoTest.Dyn.LolUpdateShips");
				Thread.Sleep(360000);
			}
		}

		private Script GetScript(IEnumerable<Ship> ships, SectorCoordinate targetSector, Position wantedPosition) {
			var script = _scriptFactory.CreateScript("FijoTest.Dyn.LolUpdateShips", "testDesc", 1);
			var shipVar = _scriptService.DefineVariable(script, "ship");
			var taskIdVar = _scriptService.DefineVariable(script, "taskId");
			var sectorVar = _scriptService.DefineVariable(script, "sector");
			var targetPosVar = _scriptService.DefineVariable(script, "targetPos");
			_scriptService.AddCommand(script, new CreateArrayCommand {Arg1 = targetPosVar, Arg2 = 4});
			_scriptService.AddCommand(script, new SetArrayPositionToValueCommand {Arg1 = targetPosVar, Arg2 = 0, Arg3 = wantedPosition.X});
			_scriptService.AddCommand(script, new SetArrayPositionToValueCommand {Arg1 = targetPosVar, Arg2 = 1, Arg3 = wantedPosition.Y});
			_scriptService.AddCommand(script, new SetArrayPositionToValueCommand {Arg1 = targetPosVar, Arg2 = 2, Arg3 = wantedPosition.Z});
			_scriptService.AddCommand(script, new GetSectorByCoordinatesCommand {Arg1 = sectorVar, Arg2 = targetSector});
			_scriptService.AddCommand(script, new SetArrayPositionToVariableCommand {Arg1 = targetPosVar, Arg2 = 3, Arg3 = sectorVar});
			foreach (var ship in ships) {
				_scriptService.AddCommand(script, new GetShipByIdCommand {Arg1 = shipVar, Arg2 = ship.OnlineId});
				_scriptService.AddCommand(script, new RandomValueCommand {Arg1 = taskIdVar, Arg2 = 0, Arg3 = 99999});
				_scriptService.AddCommand(script, new StartTaskCommand {Arg1 = shipVar, Arg2 = ScriptsNames.JumpToPosition, Arg3 = taskIdVar, Arg4 = 0, Arg5 = targetPosVar});
			}
			return script;
		}
	}
}