using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Fijo.Infrastructure.DesignPattern.Mapping;
using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.X2OnlineMod;
using Fijo.X2OnlineMod.Dto;
using Fijo.X2OnlineMod.Enums;
using Fijo.X2OnlineMod.Properties;
using Fijo.XGame.Dto;
using Fijo.XGame.Interfaces;
using Fijo.XGame.Script.Enums;
using Fijo.XGame.Script.Interfaces;
using Fijo.XGame.Services;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;

namespace Fijo.X2OnlineModTest {
	public abstract class ShipTypeImportTestBase {
		protected IGameFactory GameFactory;
		protected GameEnvironment GameEnvironment;
		protected IGameAccessSerivce GameAccessSerivce;
		protected IRepository<IEnumerable<string[]>> InputRepository;
		protected IMapping<string[], GameObject> ShipMapping;
		protected IScriptStoreService ScriptStoreService;
		protected IScriptService ScriptService;
		protected IScriptFactory ScriptFactory;

		public virtual void SetUp() {
			new InternalInitKernel().Init();
			GameFactory = Kernel.Resolve<IGameFactory>();
			GameEnvironment = Kernel.Resolve<IRepository<GameEnvironment>>().Get();
			GameAccessSerivce = Kernel.Resolve<IGameAccessSerivce>();
			InputRepository = Kernel.Resolve<IRepository<IEnumerable<string[]>>>();
			ShipMapping = Kernel.Resolve<IMapping<string[], GameObject>>();
			ScriptStoreService = Kernel.Resolve<IScriptStoreService>();
			ScriptService = Kernel.Resolve<IScriptService>();
			ScriptFactory = Kernel.Resolve<IScriptFactory>();
		}

		protected void ImportFiles(List<ShipType> shipTypes) {
			var shipTypeInfos = ReadWrittenLog(shipTypes).Where(x => !(x.ShipType == 0 && x.Name == "Unbekanntes Objekt")).DistinctBy(x => x.ShipType).ToList();
			var shipTypeEnumContent = GetEnumContent(shipTypeInfos, GetFormatedShipTypeEnum);
			Console.WriteLine("shipTypeEnumContent");
			Console.WriteLine(shipTypeEnumContent);
		}

		private string GetEnumContent(IEnumerable<ShipTypeInfo> shipTypeInfos, Func<ShipTypeInfo, string> func) {
			return string.Join(", ", shipTypeInfos.Select(x => GetFormated(x, func)));
		}

		private string GetFormatedShipTypeEnum(ShipTypeInfo x) {
			return  ((int) x.ShipType).ToString();
		}

		private string GetFormated(ShipTypeInfo x, Func<ShipTypeInfo, string> func) {
			return string.Format("{0} = {1}", GetFormatedName(x), func(x));
		}

		private string GetFormatedName(ShipTypeInfo x) {
			return GetFormatedName(x.Name);
		}

		protected string GetFormatedName(string x) {
			return x.Replace(" ", "").Replace("-", "_").Replace("�", "ae").Replace("�", "oe").Replace("�", "ue");
		}

		protected class ShipTypeInfo {
			public string Name { get; set; }
			public ShipType ShipType { get; set; }
		}

		private IEnumerable<ShipTypeInfo> ReadWrittenLog(IList<ShipType> shipTypes) {
			var fileContents = GetAll(shipTypes.Count());
			return fileContents.Select((writtenLogLines, index) => Map(writtenLogLines, shipTypes[index]));
		}

		private ShipTypeInfo Map(IList<string> writtenLogLines, ShipType shipType) {
			return new ShipTypeInfo
			       {
				       Name = Get(writtenLogLines, InputShip.ShipName),
				       ShipType = shipType
			       };
		}

		private string Get(IList<string> writtenLogLines, InputShip wareSubType) {
			return writtenLogLines[(int) wareSubType];
		}

		private IEnumerable<string[]> GetAll(int count) {
			return Enumerable.Range(0, count).Select(GetFileName).Select(File.ReadAllLines);
		}
		
		protected string GetFileName(int index) {
			return Path.Combine(GameEnvironment.GameDirForRead, string.Format("log{0}.txt", GetNumberAtPos(index)));
		}

		private string GetNumberAtPos(int num) {
			return string.Format("{0:00000}", num);
		}
	}
}