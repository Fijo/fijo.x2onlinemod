using Fijo.X2OnlineMod;
using Fijo.X2OnlineMod.Commands;
using Fijo.X2OnlineMod.Dto;
using Fijo.X2OnlineMod.Properties;
using Fijo.XGame.Script.Commands;
using Fijo.XGame.Script.Dto;
using Fijo.XGame.Script.Interfaces;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using NUnit.Framework;

namespace Fijo.X2OnlineModTest {
	[TestFixture]
	public class ScriptSerializerTest {
		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
		}

		[Test]
		public void CorrectScript_Serialize_MustReturnCorrectString() {
			var scriptSerializer = Kernel.Resolve<IScriptSerializer>();
			var scriptFactory = Kernel.Resolve<IScriptFactory>();
			var script = scriptFactory.CreateScript("TestScript", "Description", 1);
			var variables = script.Variables;
			variables.Add(scriptFactory.CreateVariable("sector", 0));
			script.Commands.Add(new GetSectorByCoordinatesCommand {Arg1 = variables[0], Arg2 = new SectorCoordinate(1, 1)});
			var result = scriptSerializer.Serialize(script);
			Assert.AreEqual(@"<?xml version=""1.0"" standalone=""yes"" ?>
<?xml-stylesheet href=""x2script.xsl"" type=""text/xsl"" ?>
<script>
<name>TestScript</name>
<version>1</version>
<engineversion>25</engineversion>
<description>Description</description>
<arguments>
</arguments>
<sourcetext>
</sourcetext>
<codearray>
<sval type=""array"" size=""10""><sval type=""string"" val=""TestScript"" /><sval type=""int"" val=""25"" /><sval type=""string"" val=""Description"" /><sval type=""int"" val=""1"" /><sval type=""int"" val=""0"" /><sval type=""array"" size=""1""><sval type=""string"" val=""sector"" /></sval><sval type=""array"" size=""2""><sval type=""array"" size=""6""><sval type=""int"" val=""800"" /><sval type=""int"" val=""0"" /><sval type=""int"" val=""4"" /><sval type=""int"" val=""1"" /><sval type=""int"" val=""4"" /><sval type=""int"" val=""1"" /></sval><sval type=""array"" size=""3""><sval type=""int"" val=""103"" /><sval type=""int"" val=""0"" /><sval type=""int"" val=""0"" /></sval></sval><sval type=""int"" val=""0"" /><sval type=""int"" val=""0"" /><sval type=""int"" val=""0"" /></sval>
</codearray>
</script>".Replace("\r", ""), result.Replace("\r", ""));
		}
	}
}