using System;
using System.Collections.Generic;
using Fijo.Infrastructure.DesignPattern.Stock;
using Fijo.X2OnlineMod.Dto;
using Fijo.X2OnlineMod.Properties;
using Fijo.XGame.Script.Dto;
using Fijo.XGame.Script.Enums;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using NUnit.Framework;
using System.Linq;

namespace Fijo.X2OnlineModTest {
	[TestFixture]
	public class XOnlineStockTest {
		private IStock<Ship> _stock;

		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
			_stock = Kernel.Resolve<IStock<Ship>>();
		}

		[Test, Ignore("database intigration test")]
		public void Test() {
			var ship = GetDockedShip();
			_stock.Add(ship);
			var onlineId = ship.OnlineId;
			Assert.AreEqual(_stock.GetAll().Single(x => x.OnlineId == onlineId).Name, ship.Name);
		}

		private Ship GetDockedShip() {
			var ship = GetShip();
			var hangerShip = GetHangerShip();
			hangerShip.LandedShipIds = new List<string> {ship.InGameId};
			ship.DockedTo = hangerShip;
			return ship;
		}

		private Ship GetShip() {
			return new Ship
			{
				Name = "Buster",
				Position = new SpacePosition(-10000, 2000, 0, 0, 0, 0),
				Sector = new SectorCoordinate(1, 2),
				Type = ShipType.ArgonBuster,
				Race = Race.Player,
				OnlineId = Guid.NewGuid().ToString(),
				Wares = new Wares(new List<Ware>
				{
					new Ware(new WareType(WareMainType.Tech, (int) Tech.BoostExtension), 1),
					new Ware(new WareType(WareMainType.Tech, (int) Tech.Freightscanner), 1),
					new Ware(new WareType(WareMainType.Tech, (int) Tech.MineralScanner), 1),
					new Ware(new WareType(WareMainType.Tech, (int) Tech.DuplexScanner), 1),
					new Ware(new WareType(WareMainType.Tech, (int) Tech.Cargobayextension), 5),
					new Ware(new WareType(WareMainType.Tech, (int) Tech.EngineTuning), 20),
					new Ware(new WareType(WareMainType.Shields, (int) Shields.Shield5MW), 3),
					new Ware(new WareType(WareMainType.Weapons, (int) Weapons.BetaParticleAcceleratorCannon), 2),
					new Ware(new WareType(WareMainType.Rockets, (int) Rockets.DragonflyMissile), 10),
				})
			};
		}

		private Ship GetHangerShip() {
			return new Ship
			{
				Name = "Oddy",
				IsActive = true,
				Position = new SpacePosition(-10000, 0, 0, 0, 0, 0),
				Sector = new SectorCoordinate(1, 2),
				Type = ShipType.ParanidOdysseus,
				Race = Race.Player,
				OnlineId = Guid.NewGuid().ToString(),
				Wares = new Wares(new List<Ware>
				{
					new Ware(new WareType(WareMainType.Tech, (int) Tech.BoostExtension), 1),
					new Ware(new WareType(WareMainType.Tech, (int) Tech.Freightscanner), 1),
					new Ware(new WareType(WareMainType.Tech, (int) Tech.MineralScanner), 1),
					new Ware(new WareType(WareMainType.Tech, (int) Tech.DuplexScanner), 1),
					new Ware(new WareType(WareMainType.Tech, (int) Tech.RudderOptimisation), 10),
					new Ware(new WareType(WareMainType.Tech, (int) Tech.Cargobayextension), 5),
					new Ware(new WareType(WareMainType.Tech, (int) Tech.EngineTuning), 8),
					new Ware(new WareType(WareMainType.Shields, (int) Shields.Shield125MW), 6),
					new Ware(new WareType(WareMainType.Weapons, (int) Weapons.BetaPhasedShockwaveGenerator), 18),
					new Ware(new WareType(WareMainType.Tech, (int) Tech.NavigationRelaySatellite), 8),
					new Ware(new WareType(WareMainType.Energy, (int) Energy.EnergyCells), 10),
					new Ware(new WareType(WareMainType.Bio, (int) Bio.Argnubeef), 4),
					new Ware(new WareType(WareMainType.Bio, (int) Bio.Plankton), 2),
					new Ware(new WareType(WareMainType.Bio, (int) Bio.SojaBeans), 1),
					new Ware(new WareType(WareMainType.Bio, (int) Bio.DelexianWheat), 10),
					new Ware(new WareType(WareMainType.Natural, (int) Natural.Water), 10),
					new Ware(new WareType(WareMainType.Bio, (int) Bio.ScruffinFruits), 10),
					new Ware(new WareType(WareMainType.Minerals, (int) Minerals.Ore), 10),
					new Ware(new WareType(WareMainType.Minerals, (int) Minerals.SiliconWafers), 10),
					new Ware(new WareType(WareMainType.Rockets, (int) Rockets.HornetMissile), 10),
					new Ware(new WareType(WareMainType.Rockets, (int) Rockets.DragonflyMissile), 10),
				})
			};
		}
	}
}