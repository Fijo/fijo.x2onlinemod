using Fijo.Infrastructure.DesignPattern.Mapping;
using Fijo.X2OnlineMod.Dto;
using Fijo.X2OnlineMod.Properties;
using Fijo.XGame.Script.Enums;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using NUnit.Framework;

namespace Fijo.X2OnlineModTest {
	[TestFixture]
	public class GameObjectsMappingTest {
		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
		}

		[Test]
		public void Test() {
			var source = new[]
			{
				"28937",
				"220",
				"-1942",
				"Argon Merkur",
				"53900",
				"Argonen",
				"ANAME-34",
				"0",
				"5",
				"0",
				"20000",
				"100",
				"COMMAND_GET_WARE"
			};
			var gameMapping = Kernel.Resolve<IMapping<string[], Ship>>();
			var ship = gameMapping.Map(source);
			Assert.AreEqual(28937, ship.Position.X);
			Assert.AreEqual(220, ship.Position.Y);
			Assert.AreEqual(-1942, ship.Position.Z);
			Assert.AreEqual("Argon Merkur", ship.Name);
			Assert.AreEqual(ShipType.ArgonMerkur, ship.Type);
			Assert.AreEqual(Race.Argonen, ship.Race);
			Assert.AreEqual("ANAME-34", ship.HomebaseId);
			Assert.AreEqual(0, ship.Sector.X);
			Assert.AreEqual(5, ship.Sector.Y);
			Assert.AreEqual(0, ship.ShipHangerLanded);
			Assert.AreEqual(20000, ship.Hull);
			Assert.AreEqual(100, ship.Shield);
			Assert.AreEqual("COMMAND_GET_WARE", ship.Command);
		}
	}
}