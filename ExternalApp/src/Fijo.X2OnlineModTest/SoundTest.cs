using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IList.Generic;
using NUnit.Framework;
using WinMM;

namespace Fijo.X2OnlineModTest {


	public class Sound {
		public void Process() {
		}
	}


	[TestFixture]
	public class SoundTest {
		[Test]
		public void Test() {
			
            // We are using a hard-coded format that should be supported by all consumer sound devices available today.
            WaveFormat format = WaveFormat.Pcm44Khz16BitMono;

            // We intialize the devices to the use the wave mapper devices, provided by Microsoft.
            // This device is built-in, and know to support the above specified format.
            this.waveIn = new WaveIn(WaveIn.WaveInMapperDeviceId);
            this.waveOut = new WaveOut(WaveOut.WaveOutMapperDeviceId);

            // We must start the wave-out device before we start the wave-in device, otherwise, the wave-in device may have data available
            // before the wave-out device is ready to play it.
            this.waveOut.Open(format);

            // Tweaking these values affects the internal buffering thread.
            // Setting too small of a QueueSize or too small of a BufferSize will cause buffer underruns, which will sound like choppy audio.
            // Setting too large of a BufferSize will increase the audio latency.
            // Setting a larger QueueSize increases memory usage.
            this.waveIn.BufferQueueSize = 200;
            this.waveIn.BufferSize = 64;
            this.waveIn.DataReady += new EventHandler<DataReadyEventArgs>(this.WaveIn_DataReady);
            this.waveIn.Open(format);
            this.waveIn.Start();
			Thread.Sleep(50);
			waveIn.Stop();
			File.WriteAllLines(@"R:\audio.txt", _content.Select(x => x.ToString()).ToArray());
		}
		
		[Test]
		public void Test2() {
			
			var needle = File.ReadAllLines(@"R:\audio.txt").Select(int.Parse).ToList();

            // We are using a hard-coded format that should be supported by all consumer sound devices available today.
            WaveFormat format = WaveFormat.Pcm44Khz16BitMono;

            // We intialize the devices to the use the wave mapper devices, provided by Microsoft.
            // This device is built-in, and know to support the above specified format.
            this.waveIn = new WaveIn(WaveIn.WaveInMapperDeviceId);
            this.waveOut = new WaveOut(WaveOut.WaveOutMapperDeviceId);

            // We must start the wave-out device before we start the wave-in device, otherwise, the wave-in device may have data available
            // before the wave-out device is ready to play it.
            this.waveOut.Open(format);

            // Tweaking these values affects the internal buffering thread.
            // Setting too small of a QueueSize or too small of a BufferSize will cause buffer underruns, which will sound like choppy audio.
            // Setting too large of a BufferSize will increase the audio latency.
            // Setting a larger QueueSize increases memory usage.
            this.waveIn.BufferQueueSize = 200;
            this.waveIn.BufferSize = 64;
            this.waveIn.DataReady += new EventHandler<DataReadyEventArgs>(this.WaveIn_DataReady);
            this.waveIn.Open(format);
            this.waveIn.Start();
			Thread.Sleep(30000);
			waveIn.Stop();
			
			if(_content.Count < needle.Count) return;
			var contains = Enumerable.Range(0, _content.Count - needle.Count).AsParallel().Any(x => Enumerable.Range(x, needle.Count).All(y => _content[y].Equals(needle[y - x])));
			Assert.IsTrue(contains);
		}
		
        /// <summary>
        /// Hold a locking object for disposing and writing to the waveIn and waveOut objects.
        /// </summary>
        private object actionLock = new object();

        /// <summary>
        /// Holds the object used to read data from the microphone.
        /// </summary>
        private WaveIn waveIn;

        /// <summary>
        /// Holds the object used to write data to the speakers.
        /// </summary>
        private WaveOut waveOut;


        /// <summary>
        /// Describes a delegate used to update the level-indicator bar.
        /// </summary>
        /// <param name="max">The value to which the bar should be set.</param>
        private delegate void SetLevelDelegate(int max);

		private IList<int> _content = new List<int>();
        /// <summary>
        /// The callback function given to the <see cref="WinMM.MainView" /> device, to notify us when data is ready and pass that data back to us.
        /// </summary>
        /// <param name="sender">The object calling this event.</param>
        /// <param name="e">The arguments to the event.  This includes the data returned from the <see cref="WinMM.MainView" /> device.</param>
        private void WaveIn_DataReady(object sender, DataReadyEventArgs e)
        {
            // Then, we read the data as a binary stream, and find the maximum level of the sample.

            _content.AddRange(GetFreqs(e));

            // Finally, we instruct the progress bar to show the new maximum value.
        }

		private static IEnumerable<int> GetFreqs(DataReadyEventArgs e) {
			using (var ms = new MemoryStream(e.Data)) {
				using (var br = new BinaryReader(ms)) {
					while (ms.Position != ms.Length) {
						yield return ((int) br.ReadInt16());
					}
				}
			}
		}
	}
}