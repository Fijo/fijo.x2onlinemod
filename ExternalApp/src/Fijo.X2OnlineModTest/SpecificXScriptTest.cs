using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.X2OnlineMod.Enums;
using Fijo.X2OnlineMod.Properties;
using Fijo.XGame.Dto;
using Fijo.XGame.Interfaces;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using NUnit.Framework;

namespace Fijo.X2OnlineModTest {
	[TestFixture]
	public class SpecificXScriptTest {
		private IGameFactory _gameFactory;
		private GameEnvironment _gameEnvironment;
		private IGameAccessSerivce _gameAccessSerivce;
		private IRepository<IEnumerable<string[]>> _inputRepository;

		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
			_gameFactory = Kernel.Resolve<IGameFactory>();
			_gameEnvironment = Kernel.Resolve<IRepository<GameEnvironment>>().Get();
			_gameAccessSerivce = Kernel.Resolve<IGameAccessSerivce>();
			_inputRepository = Kernel.Resolve<IRepository<IEnumerable<string[]>>>();
		}

		[Test]
		public void Test() {
			var game = _gameFactory.Create(_gameEnvironment);
			_gameAccessSerivce.Start(game);
			_gameAccessSerivce.LoadSavegame(game, 1);
			lock (game) {
				_gameAccessSerivce.GotoScriptEditor(game);
				_gameAccessSerivce.RunScript(game, "Fijo.Services.Ships.Serialize");
				Thread.Sleep(10000);
				_gameAccessSerivce.Exit(game);
			}
			var testResult = ReadWrittenLog();
			var actual = testResult.Single(x => x[(int) InputShip.ShipRace] == "Player")[(int) InputShip.ShipName];
			Assert.AreEqual("Ihr A. Discoverer", actual);
		}
		
		private IEnumerable<string[]> ReadWrittenLog() {
			return _inputRepository.Get();
		}
	}
}