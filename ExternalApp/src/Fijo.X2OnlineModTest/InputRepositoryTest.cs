﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.X2OnlineMod.Properties;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Module.Configuration;
using NUnit.Framework;

namespace Fijo.X2OnlineModTest
{
	[TestFixture]
    public class InputRepositoryTest
    {
		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
		}

		[Test] 
		public void Correct2FilesInFolder_Get_Returns2Entries() {
			InitTestData();
			var inputRepository = Kernel.Resolve<IRepository<IEnumerable<string[]>>>();
			var input = inputRepository.Get().ToList();
			Assert.AreEqual(2, input.Count);
		}

		[Test] 
		public void CorrectFilesInFolder_Get_ReturnsCorrectEntry() {
			InitTestData();
			var inputRepository = Kernel.Resolve<IRepository<IEnumerable<string[]>>>();
			var input = inputRepository.Get().ToList();
			var wanted = new[]
			{
				"RTSTM-20",
				"-54857",
				"7194",
				"48487",
				"Piraten Schiff",
				"32416",
				"Piraten",
				"RHASM-90",
				"0",
				"1",
				"0",
				"0",
				"20000",
				"100",
				"COMMAND_GET_WARE"
			};
			var actual = input.Second();
			CollectionAssert.AreEqual(wanted, actual);
		}

		private void InitTestData() {
			var configurationService = Kernel.Resolve<ConfigurationService>();
			var dir = configurationService.Get<string, InputRepositoryTest>("X2InstallationDir");
			if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);
			CreateFile(dir, "log00000.txt", GetFirstTestFileContent());
			CreateFile(dir, "log00001.txt", GetSecondTestFileContent());
		}

		private static string GetSecondTestFileContent() {
			return @"RTSTM-20-54857719448487Piraten Schiff32416PiratenRHASM-90010020000100COMMAND_GET_WARE";
		}

		private static string GetFirstTestFileContent() {
			return @"BTSLY-99
-14238
3552
-18207
Boron Delphin
1937
Boronen
BTELY-92
1
0
0
0
15000
100
COMMAND_GET_WARE";
		}

		private void CreateFile(string dir, string name, string content) {
			File.WriteAllText(Path.Combine(dir, name), content);
		}
    }
}
