using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using Fijo.X2OnlineMod.Commands;
using Fijo.X2OnlineMod.Dto;
using Fijo.X2OnlineMod.Enums;
using Fijo.XGame.Script.Commands;
using Fijo.XGame.Script.Dto;
using Fijo.XGame.Script.Enums;
using NUnit.Framework;

namespace Fijo.X2OnlineModTest {
	[TestFixture]
	public class RangeShipImportTest : ShipTypeImportTestBase  {
		[SetUp]
		public override void SetUp() {
			base.SetUp();
		}

		[Test, Ignore("import")]
		public void RangeImporter() {
			var min = Enum.GetValues(typeof (ShipType)).Cast<object>().Select(x => (int) x).Min(x => x);
			var shipTypes = Enumerable.Range(min, 99999).Select(x => (ShipType) x).ToList();
			var game = GameFactory.Create(GameEnvironment);
			GameAccessSerivce.Start(game);
			GameAccessSerivce.LoadSavegame(game, 1);
			lock (game) {
				GameAccessSerivce.GotoScriptEditor(game);
				var script = GetScript(shipTypes);
				ScriptStoreService.Store(script);
				GameAccessSerivce.LoadScript(game, "FijoTest.Dyn.ShipTypeImport", CultureInfo.InvariantCulture);
				GameAccessSerivce.RunScript(game, "FijoTest.Dyn.ShipTypeImport");
				Thread.Sleep(160000);
				GameAccessSerivce.DeleteScript(game, "FijoTest.Dyn.ShipTypeImport");
			}
			GameAccessSerivce.Exit(game);
			ImportFiles(shipTypes);
		}

		private Script GetScript(IEnumerable<ShipType> shipTypes) {
			var position = new Position(100000, 100000, 100000);
			var targetSector = new SectorCoordinate(0, 0);
			var script = ScriptFactory.CreateScript("FijoTest.Dyn.ShipTypeImport", "testDesc", 1);
			var shipVar = ScriptService.DefineVariable(script, "ship");
			var sectorVar = ScriptService.DefineVariable(script, "sector");
			var index = 0;
			ScriptService.AddCommand(script, new GetSectorByCoordinatesCommand {Arg1 = sectorVar, Arg2 = targetSector});
			foreach (var shipType in shipTypes) {
				ScriptService.AddCommand(script, new CreateShipCommand {Arg1 = shipVar, Arg2 = shipType, Arg3 = Race.Argonen, Arg4 = sectorVar, Arg5 = position});
				//_scriptService.AddCommand(script, new WaitCommand {Arg1 = 10});
				ScriptService.AddCommand(script, new SerializeShipCommand {Arg1 = index, Arg2 = shipVar});
				index++;
			}
			return script;
		}
	}
}