using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using Fijo.X2OnlineMod.Enums.Constants;
using Fijo.X2OnlineModTest.Commands;
using Fijo.XGame.Interfaces;
using Fijo.XGame.Script.Commands;
using Fijo.XGame.Script.Dto;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using NUnit.Framework;

namespace Fijo.X2OnlineModTest {
	[TestFixture]
	public class SubWareTypeImportTest : ShipTypeImportTestBase  {
		private IGameEnvironmentService _gameEnvironmentService;

		[SetUp]
		public override void SetUp() {
			base.SetUp();
			_gameEnvironmentService = Kernel.Resolve<IGameEnvironmentService>();
		}

		[Ignore("import")]
		[TestCase(2)]
		[TestCase(3)]
		[TestCase(4)]
		[TestCase(5)]
		[TestCase(6)]
		[TestCase(7)]
		[TestCase(8)]
		[TestCase(9)]
		[TestCase(10)]
		[TestCase(11)]
		[TestCase(12)]
		[TestCase(13)]
		[TestCase(14)]
		[TestCase(15)]
		[TestCase(16)]
		[TestCase(17)]
		[TestCase(18)]
		[TestCase(20)]
		[TestCase(25)]
		[TestCase(30)]
		[TestCase(31)]
		public void Importer(int mainWareType) {
			var game = GameFactory.Create(GameEnvironment);
			_gameEnvironmentService.IncludeResources(GameEnvironment, ResourceFolderNames.Export);
			var script = GetScript(mainWareType);
			ScriptStoreService.Store(script);
			GameAccessSerivce.Start(game);
			GameAccessSerivce.LoadSavegame(game, 1);
			lock (game) {
				GameAccessSerivce.GotoScriptEditor(game);
				GameAccessSerivce.RunScript(game, "Fijo.Dyn.SubWareImport");
				Thread.Sleep(4000);
				GameAccessSerivce.DeleteScript(game, "Fijo.Dyn.SubWareImport");
			}
			GameAccessSerivce.Exit(game);
			WriteResult();
		}

		private void WriteResult() {
			Console.WriteLine(GetContent());
		}

		private string GetContent() {
			var hashSet = new HashSet<string>();
			return string.Join(", ", File.ReadAllLines(GetFileName(0))
				                         .Select(GetFormatedName)
				                         .Select((x, i) => string.IsNullOrEmpty(x) ? "Unknown" + i : x)
				                         .Select(x => {
					                                 var name = x;
					                                 var pos = 1;
					                                 while (!hashSet.Add(name)) name = string.Concat(x, pos++);
					                                 return name;
				                                 })
				                         .Select((x, i) => string.Format("{0} = {1}", x, i)));
		}

		private Script GetScript(int mainWareType) {
			var script = ScriptFactory.CreateScript("Fijo.Dyn.SubWareImport", "testDesc", 1);
			ScriptService.AddCommand(script, new ExportWareSubTypesCommand {Arg1 = mainWareType});
			return script;
		}
	}
}