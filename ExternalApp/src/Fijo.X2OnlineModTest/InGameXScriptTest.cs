using System.Collections.Generic;
using System.IO;
using System.Threading;
using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.X2OnlineMod.Properties;
using Fijo.XGame.Dto;
using Fijo.XGame.Interfaces;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using NUnit.Framework;

namespace Fijo.X2OnlineModTest {
	[TestFixture]
	public class InGameXScriptTest {
		private IGameFactory _gameFactory;
		private GameEnvironment _gameEnvironment;
		private IGameAccessSerivce _gameAccessSerivce;

		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
			_gameFactory = Kernel.Resolve<IGameFactory>();
			_gameEnvironment = Kernel.Resolve<IRepository<GameEnvironment>>().Get();
			_gameAccessSerivce = Kernel.Resolve<IGameAccessSerivce>();
		}
		
		[TestCase("FijoTest.Repos.GetShip", 1000)]
		[TestCase("FijoTest.Repos.GetStation", 1000)]
		[TestCase("FijoTest.Services.Move.ToPos", 60000)]
		[TestCase("FijoTest.Services.Move.ToSector", 25000)]
		[TestCase("FijoTest.Services.Move.ToStation", 60000)]
		[TestCase("FijoTest.Services.Tmp.RL.Off", 500)]
		[TestCase("FijoTest.Services.Tmp.Tune.Off0", 500)]
		[TestCase("FijoTest.Services.Tmp.Tune.Off1", 500)]
		[TestCase("FijoTest.Services.Tmp.Tune.On0", 500)]
		[TestCase("FijoTest.Services.Tmp.Tune.On1", 500)]
		[TestCase("FijoTest.Store.Conf", 500)]
		[TestCase("FijoTest.Store.Global", 500)]
		[TestCase("FijoTest.Store.Ship", 500)]
		[TestCase("FijoTest.Services.Ship.CanMove0", 1000)]
		[TestCase("FijoTest.Services.Ship.CanMove1", 1000)]
		public void Test(string scriptName, int time) {
			var game = _gameFactory.Create(_gameEnvironment);
			_gameAccessSerivce.Start(game);
			_gameAccessSerivce.LoadSavegame(game, 1);
			lock (game) {
				_gameAccessSerivce.GotoScriptEditor(game);
				_gameAccessSerivce.RunScript(game, scriptName);
				Thread.Sleep(time);
				_gameAccessSerivce.Exit(game);
			}
			var testResult = ReadWrittenLog();
			Assert.AreEqual("Test Script", testResult.FileType);
			Assert.AreEqual(scriptName, testResult.Name);
			Assert.True(testResult.IsSuccessful);
		}

		class TestResult {
			public string FileType;
			public string Name;
			public bool IsSuccessful;
		}

		private TestResult ReadWrittenLog() {
			var writtenLogLines = File.ReadAllLines(Path.Combine(_gameEnvironment.GameDirForRead, "log00000.txt"));
			return new TestResult {FileType = writtenLogLines[0], Name = writtenLogLines[1], IsSuccessful = GetBool(writtenLogLines, 2)};
		}

		private bool GetBool(IList<string> writtenLogLines, int index) {
			return writtenLogLines[index] == "1";
		}
	}
}