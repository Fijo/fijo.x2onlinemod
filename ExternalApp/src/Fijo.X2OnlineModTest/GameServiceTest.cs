using System;
using Fijo.X2OnlineMod;
using Fijo.X2OnlineMod.Interfaces;
using Fijo.X2OnlineMod.Properties;
using Fijo.XGame.Script.ScriptParamSerializers;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using NUnit.Framework;

namespace Fijo.X2OnlineModTest {
	[TestFixture]
	public class GameServiceTest {
		private IGameService _gameService;

		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
			_gameService = Kernel.Resolve<IGameService>();
		}

		[Test]
		public void Test() {
			_gameService.Start();
		}
	}
}