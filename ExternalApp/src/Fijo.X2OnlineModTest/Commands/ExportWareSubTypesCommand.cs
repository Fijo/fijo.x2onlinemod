﻿using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.X2OnlineMod.Enums.Constants;
using Fijo.XGame.Script.Commands;
using Fijo.XGame.Script.Enums;
using Fijo.XGame.Script.Enums.Constants;

namespace Fijo.X2OnlineModTest.Commands {
	public class ExportWareSubTypesCommand : ICommand<int> {
		[About("WareMainType")]
		public int Arg1 { get; set; }

		#region Implementation of ICommand
		public IEnumerable<object> ToObjects() {
			yield return ScriptMethodes.CallScript;
			yield return ScriptsNames.ExportWareSubTypes;
			yield return -2147483647;
			yield return 131075;
			yield return ScriptsConstants.ScriptParam;
			yield return 1;
			yield return ScriptsConstants.IntParam;
			yield return Arg1;
		}
		#endregion
	}
}