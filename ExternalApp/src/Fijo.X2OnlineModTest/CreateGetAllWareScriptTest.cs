using System;
using System.Collections.Generic;
using System.Linq;
using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.X2OnlineMod.Properties;
using Fijo.XGame.Dto;
using Fijo.XGame.Interfaces;
using Fijo.XGame.Script.Commands;
using Fijo.XGame.Script.Dto;
using Fijo.XGame.Script.Enums;
using Fijo.XGame.Script.Interfaces;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using NUnit.Framework;

namespace Fijo.X2OnlineModTest {
	[TestFixture]
	public class CreateGetAllWareScriptTest {
		private IScriptStoreService _scriptStoreService;
		private IScriptService _scriptService;
		private IScriptFactory _scriptFactory;

		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
			_scriptStoreService = Kernel.Resolve<IScriptStoreService>();
			_scriptService = Kernel.Resolve<IScriptService>();
			_scriptFactory = Kernel.Resolve<IScriptFactory>();
		}

		[Test, Ignore("script creation task")]
		public void CreateScript() {
			_scriptStoreService.Store(GetScript());
		}

		private Script GetScript() {
			var script = _scriptFactory.CreateScript("Fijo.Services.Ware.GetAll", "Returns An Array of all wares", 1);
			var wares = _scriptService.DefineVariable(script, "wares");
			var wareType = _scriptService.DefineVariable(script, "wareType");

			var wareTypes = GetAllWareTypes().ToList();
			_scriptService.AddCommand(script, new CreateArrayCommand {Arg1 = wares, Arg2 = wareTypes.Count});
			wareTypes.ForEach((x, i) => {
				                  _scriptService.AddCommand(script, new GetWareByTypes {Arg1 = wareType, Arg2 = x});
				                  _scriptService.AddCommand(script, new SetArrayPositionToVariableCommand {Arg1 = wares, Arg2 = i, Arg3 = wareType});
			                  });
			_scriptService.AddCommand(script, new ReturnCommand {Arg1 = wares});
			return script;
		}

		private IEnumerable<WareType> GetAllWareTypes() {
			return GetEnumValues<SectorBackgrounds>(WareMainType.SectorBackgrounds)
				.Concat(GetEnumValues<Suns>(WareMainType.Suns),
				        GetEnumValues<Planets>(WareMainType.Planets),
				        GetEnumValues<Docks>(WareMainType.Docks),
				        GetEnumValues<Factories>(WareMainType.Factories),
				        GetEnumValues<ShipType>(WareMainType.Ship),
				        GetEnumValues<Weapons>(WareMainType.Weapons),
				        GetEnumValues<Shields>(WareMainType.Shields),
				        GetEnumValues<Rockets>(WareMainType.Rockets),
				        GetEnumValues<Energy>(WareMainType.Energy),
				        GetEnumValues<Natural>(WareMainType.Natural),
				        GetEnumValues<Bio>(WareMainType.Bio),
				        GetEnumValues<Food>(WareMainType.Food),
				        GetEnumValues<Minerals>(WareMainType.Minerals),
				        GetEnumValues<Tech>(WareMainType.Tech),
				        GetEnumValues<Asteroids>(WareMainType.Asteroids),
				        GetEnumValues<Gates>(WareMainType.Gates),
				        GetEnumValues<SpecialObjects>(WareMainType.SpecialObjects),
				        GetEnumValues<Cockpits>(WareMainType.Cockpits));
		}

		private static IEnumerable<WareType> GetEnumValues<T>(WareMainType mainType) where T : IConvertible {
			return Enum.GetValues(typeof(T))
				.Cast<T>()
				.Select(x => Convert.ToInt32(x))
				.Select(x => new WareType(mainType, x));
		}
	}
}