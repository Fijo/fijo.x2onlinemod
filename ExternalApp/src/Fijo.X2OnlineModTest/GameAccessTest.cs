using System.Globalization;
using System.IO;
using System.Threading;
using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.X2OnlineMod.Commands;
using Fijo.X2OnlineMod.Properties;
using Fijo.XGame.Dto;
using Fijo.XGame.Interfaces;
using Fijo.XGame.Script.Commands;
using Fijo.XGame.Script.Dto;
using Fijo.XGame.Script.Interfaces;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using NUnit.Framework;

namespace Fijo.X2OnlineModTest {
	[TestFixture]
	public class GameAccessTest {
		private IGameFactory _gameFactory;
		private GameEnvironment _gameEnvironment;
		private IGameAccessSerivce _gameAccessSerivce;
		private IScriptStoreService _scriptStoreService;
		private IGameEnvironmentService _gameEnvironmentService;
		private IScriptService _scriptService;
		private IScriptFactory _scriptFactory;

		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
			_gameFactory = Kernel.Resolve<IGameFactory>();
			_gameEnvironment = Kernel.Resolve<IRepository<GameEnvironment>>().Get();
			_gameEnvironmentService = Kernel.Resolve<IGameEnvironmentService>();
			_gameAccessSerivce = Kernel.Resolve<IGameAccessSerivce>();
			_scriptStoreService = Kernel.Resolve<IScriptStoreService>();
			_scriptService = Kernel.Resolve<IScriptService>();
			_scriptFactory = Kernel.Resolve<IScriptFactory>();
		}

		[Test]
		public void ValidGame_GameAccessMoveShip_ShipHasCorrectPosition() {
			var wantedPosition = new Position(10000, 10000, 10000);
			CopyTestGetShipInfoScript();
			var game = _gameFactory.Create(_gameEnvironment);
			_gameAccessSerivce.Start(game);
			_gameAccessSerivce.LoadSavegame(game, 1);
			lock(game) {
				_gameAccessSerivce.GotoScriptEditor(game);
				_gameAccessSerivce.RunScript(game, "FijoTest.GetShipInfo");
				Thread.Sleep(500);

				var shipInfo = ReadWrittenLog();
				var script = GetScript(shipInfo, wantedPosition);

				DeleteScript(_gameAccessSerivce, game, "FijoTest.GetShipInfo");
				_scriptStoreService.Store(script);
				_gameAccessSerivce.LoadScript(game, "FijoTest.Dyn.MoveToPos", CultureInfo.InvariantCulture);
				_gameAccessSerivce.RunScript(game, "FijoTest.Dyn.MoveToPos");
				Thread.Sleep(8000);

				DeleteScript(_gameAccessSerivce, game, "FijoTest.Dyn.MoveToPos");

				CopyTestGetShipInfoScript();
				_gameAccessSerivce.LoadScript(game, "FijoTest.GetShipInfo", CultureInfo.InvariantCulture);
				_gameAccessSerivce.RunScript(game, "FijoTest.GetShipInfo");
				Thread.Sleep(1500);
				_gameAccessSerivce.Exit(game);

				var movedShipInfo = ReadWrittenLog();
				var position = movedShipInfo.Position;
				Assert.AreNotEqual(shipInfo.Position, position);
				Assert.AreEqual(wantedPosition.X, position.X);
				Assert.AreEqual(wantedPosition.Y, position.Y);
				Assert.AreEqual(wantedPosition.Z, position.Z);
			}
		}

		private Script GetScript(ShipInfo shipInfo, Position wantedPosition) {
			var script = _scriptFactory.CreateScript("FijoTest.Dyn.MoveToPos", "testDesc", 1);
			var ship = _scriptService.DefineVariable(script, "ship");
			var targetPos = _scriptService.DefineVariable(script, "targetPos");
			_scriptService.AddCommand(script, new GetShipByInGameIdCommand {Arg1 = ship, Arg2 = shipInfo.ShipId});
			_scriptService.AddCommand(script, new CreateArrayCommand {Arg1 = targetPos, Arg2 = 3});
			_scriptService.AddCommand(script, new SetArrayPositionToValueCommand {Arg1 = targetPos, Arg2 = 0, Arg3 = wantedPosition.X});
			_scriptService.AddCommand(script, new SetArrayPositionToValueCommand {Arg1 = targetPos, Arg2 = 1, Arg3 = wantedPosition.Y});
			_scriptService.AddCommand(script, new SetArrayPositionToValueCommand {Arg1 = targetPos, Arg2 = 2, Arg3 = wantedPosition.Z});
			_scriptService.AddCommand(script, new JumpToPositionCommand {Arg1 = ship, Arg2 = targetPos});
			return script;
		}

		class ShipInfo {
			public string ShipId;
			public Position Position;
		}

		private ShipInfo ReadWrittenLog() {
			var writtenLogLines = File.ReadAllLines(Path.Combine(_gameEnvironment.GameDirForRead, "log00001.txt"));
			return new ShipInfo {ShipId = writtenLogLines[0], Position = new Position(int.Parse(writtenLogLines[1]), int.Parse(writtenLogLines[2]), int.Parse(writtenLogLines[3]))};
		}

		private void DeleteScript(IGameAccessSerivce gameAccessSerivce, Game game, string name) {
			gameAccessSerivce.DeleteScript(game, name);
			Thread.Sleep(500);
		}

		private void CopyTestGetShipInfoScript() {
			_gameEnvironmentService.InstallScript(_gameEnvironment, @"TestData\X2Scripts\FijoTest.GetShipInfo.xml");
		}
	}
}