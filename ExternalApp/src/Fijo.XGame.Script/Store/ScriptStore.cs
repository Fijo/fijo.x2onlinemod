using System.IO;
using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.XGame.Dto;
using Fijo.XGame.Interfaces;
using Fijo.XGame.Script.Interfaces;
using JetBrains.Annotations;

namespace Fijo.XGame.Script.Store {
	[UsedImplicitly]
	public class ScriptStore : IScriptStore {
		private readonly string _scriptDirForWrite;
		private readonly GameEnvironment _gameEnvironment;

		public ScriptStore(IRepository<GameEnvironment> gameEnvironmentRepository, IGameEnvironmentService gameEnvironmentService) {
			_gameEnvironment = gameEnvironmentRepository.Get();
			_scriptDirForWrite = gameEnvironmentService.GetScriptDirForWrite(_gameEnvironment);
		}

		public void Store(string name, string content) {
			var filename = Path.Combine(_scriptDirForWrite, string.Format("{0}.xml", name));
			File.WriteAllText(filename, content, _gameEnvironment.ScriptEncoding);
		}
	}
}