using Fijo.XGame.Script.Commands;
using Fijo.XGame.Script.Dto;

namespace Fijo.XGame.Script.Interfaces {
	public interface IScriptService {
		void AddCommand(Dto.Script script, ICommand command);
		Variable DefineVariable(Dto.Script script, string name);
	}
}