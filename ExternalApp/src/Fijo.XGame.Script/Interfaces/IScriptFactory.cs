using Fijo.XGame.Script.Dto;

namespace Fijo.XGame.Script.Interfaces {
	public interface IScriptFactory {
		Dto.Script CreateScript(string name, string description, int version);
		Variable CreateVariable(string name, int id);
	}
}