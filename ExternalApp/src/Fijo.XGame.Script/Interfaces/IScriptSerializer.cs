namespace Fijo.XGame.Script.Interfaces {
	public interface IScriptSerializer {
		string Serialize(Dto.Script script);
	}
}