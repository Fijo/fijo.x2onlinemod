namespace Fijo.XGame.Script.Interfaces {
	public interface IScriptStore {
		void Store(string name, string content);
	}
}