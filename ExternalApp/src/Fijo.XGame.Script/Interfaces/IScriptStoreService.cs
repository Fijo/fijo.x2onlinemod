namespace Fijo.XGame.Script.Interfaces {
	public interface IScriptStoreService {
		void Store(Dto.Script script);
	}
}