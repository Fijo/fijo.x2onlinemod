using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.XGame.Script.Enums {
	[ModificationNote("The names are the same as in the x2 export to file script. Only change them if you change the x2 export data script.")]
	[About("GameObjectMappingType")]
	public enum GameObjectType {
		Ship,
		Ware
	}
}