using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.XGame.Script.Enums {
	[ModificationNote("plase do not change the indexes - that are the original ones from the x2 script xml files/ output logs")]
	[RelatedLink("http://www.madxhawk.com/kb.php?mode=article&k=574")]
	public enum WareMainType {
		SectorBackgrounds = 2,
		Suns = 3,
		Planets = 4,
		Docks = 5,
		Factories = 6,
		Ship = 7,
		Weapons = 8,
		Shields = 9,
		Rockets = 10,
		Energy = 11,
		Natural = 12,
		Bio = 13,
		Food = 14,
		Minerals = 15,
		Tech = 16,
		Asteroids = 17,
		Gates = 18,
		SpecialObjects = 20,
		Cockpits = 25
	}
}