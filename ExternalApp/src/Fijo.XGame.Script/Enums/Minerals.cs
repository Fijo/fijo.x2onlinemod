using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.XGame.Script.Enums {
	[ModificationNote("plase do not change the indexes - that are the original ones from the x2 script xml files")]
	public enum Minerals {
		Ore = 0,
		SiliconWafers = 1,
		Nividium = 2
	}
}