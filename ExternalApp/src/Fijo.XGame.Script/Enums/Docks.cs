using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.XGame.Script.Enums {
	[ModificationNote("plase do not change the indexes - that are the original ones from the x2 script xml files")]
	public enum Docks {
		FreeArgonTradingStation = 0,
		SplitTradingPort = 1,
		ParanidTradingDock = 2,
		RoyalBoronTradingStation = 3,
		TeladiTradingStation = 4,
		XenonStation = 5,
		ArgonEquipmentDock = 6,
		SplitEquipmentDock = 7,
		ParanidEquipmentDock = 8,
		BoronEquipmentDock = 9,
		TeladiSpaceEquipmentDock = 10,
		XenonStation1 = 11,
		UnknownObject = 12,
		GonerTemple = 13,
		PirateBase = 14,
		ReadText17_4101 = 15,
		UnknownEnemyStation = 16
	}
}