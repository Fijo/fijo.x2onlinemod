using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.XGame.Script.Enums {
	[ModificationNote("plase do not change the indexes - that are the original ones from the x2 script xml files")]
	public enum Weapons {
		AlphaImpulseRayEmitter = 0,
		BetaImpulseRayEmitter = 1,
		GammaImpulseRayEmitter = 2,
		AlphaParticleAcceleratorCannon = 3,
		BetaParticleAcceleratorCannon = 4,
		GammaParticleAcceleratorCannon = 5,
		AlphaHighEnergyPlasmaThrower = 6,
		BetaHighEnergyPlasmaThrower = 7,
		GammaHighEnergyPlasmaThrower = 8,
		AlphaPhasedShockwaveGenerator = 9,
		BetaPhasedShockwaveGenerator = 10,
		GammaPhasedShockwaveGenerator = 11,
		MobileDrillingSystem = 12,
		UnknownObject = 13,
		UnknownObject1 = 14,
		UnknownObject2 = 15,
		MassDriver = 16,
		IonDisruptor = 17,
		AlphaPhotonPulseCannon = 18,
		BetaPhotonPulseCannon = 19,
		GammaPhotonPulseCannon = 20,
		LasertowerWeapon = 21,
		GammaKyonEmitter = 22,
		UnknownObject3 = 23
	}
}