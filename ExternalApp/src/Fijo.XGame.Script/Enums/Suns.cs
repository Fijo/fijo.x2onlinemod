using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.XGame.Script.Enums {
	[ModificationNote("plase do not change the indexes - that are the original ones from the x2 script xml files")]
	public enum Suns {
		ReadText17_884 = 0,
		ReadText17_8841 = 1,
		ReadText17_8842 = 2,
		ReadText17_8843 = 3,
		ReadText17_8844 = 4,
		ReadText17_8845 = 5,
		ReadText17_8846 = 6,
		ReadText17_8847 = 7,
		ReadText17_8848 = 8,
		ReadText17_8849 = 9,
		ReadText17_88410 = 10,
		ReadText17_88411 = 11,
		ReadText17_88412 = 12,
		ReadText17_88413 = 13,
		ReadText17_88414 = 14,
		ReadText17_88415 = 15,
		ReadText17_88416 = 16,
		ReadText17_88417 = 17,
		ReadText17_88418 = 18,
		ReadText17_88419 = 19,
		ReadText17_88420 = 20,
		ReadText17_88421 = 21,
		ReadText17_88422 = 22
	}
}