using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.XGame.Script.Enums {
	[ModificationNote("plase do not change the indexes - that are the original ones from the x2 script xml files")]
	public enum Food {
		ClothRimes = 0,
		MeatsteakCahoonas = 1,
		SpaceFuel = 2,
		StottSpices = 3,
		BoFu = 4,
		Massompowder = 5,
		RastarOil = 6,
		Majaglit = 7,
		SojaHusk = 8,
		NostropOil = 9,
		Spaceweed = 10
	}
}