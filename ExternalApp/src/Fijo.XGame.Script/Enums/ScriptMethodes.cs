namespace Fijo.XGame.Script.Enums {
	public enum ScriptMethodes {
		SectorByPos = 800,
		CreateShip = 739,
		CallScript = 102,
		CreateArray = 128,
		SetArrayPosition = 130,
		WriteToPlayerLogbook = 400,
		StartTask = 1017,
		Wait = 105,
		RandomValueFromTo = 110,
		Return = 103,
		SetName = 1029,
		SetRotation = 768,
		GetWareByMainAndSubType = 657
	}
}