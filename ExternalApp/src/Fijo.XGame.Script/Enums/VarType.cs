using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.XGame.Script.Enums {
	[About("X2XmlScriptVarType")]
	[ModificationNote("plase do not change the indexes - that are the original ones from the x2 script xml files")]
	public enum VarType {
		Interger = 1,
		String = 2
	}
}