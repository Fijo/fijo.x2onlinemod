using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.XGame.Script.Enums {
	[ModificationNote("plase do not change the indexes - that are the original ones from the x2 script xml files")]
	public enum Bio {
		DelexianWheat = 0,
		Argnubeef = 1,
		Plankton = 2,
		BoGas = 3,
		ScruffinFruits = 4,
		CheltsMeat = 5,
		SojaBeans = 6,
		MajaSnails = 7,
		SunriseFlowers = 8,
		Teladianium = 9,
		SwampPlant = 10
	}
}