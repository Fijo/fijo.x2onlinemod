using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.XGame.Script.Enums {
	[ModificationNote("plase do not change the indexes - that are the original ones from the x2 script xml files")]
	public enum Planets {
		ReadText17_770 = 0,
		ReadText17_771 = 1,
		ReadText17_772 = 2,
		ReadText17_773 = 3,
		ReadText17_774 = 4,
		ReadText17_775 = 5,
		ReadText17_776 = 6,
		ReadText17_777 = 7,
		ReadText17_778 = 8,
		ReadText17_779 = 9,
		ReadText17_780 = 10,
		ReadText17_781 = 11,
		ReadText17_782 = 12,
		ReadText17_7821 = 13,
		ReadText17_7822 = 14,
		ReadText17_7823 = 15,
		ReadText17_7824 = 16,
		ReadText17_7825 = 17,
		ReadText17_7826 = 18,
		ReadText17_7827 = 19
	}
}