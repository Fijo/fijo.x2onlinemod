using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.XGame.Script.Enums {
	[ModificationNote("plase do not change the indexes - that are the original ones from the x2 script xml files")]
	public enum Race {
		Argonen = 1,
		Boronen = 2,
		Split = 3,
		Paraniden = 4,
		Teladi = 5,
		Xenon = 6,
		Khaak = 7,
		Piraten = 8,
		Goner = 9,
		Player = 10,
		Feinde = 11,
		Neutrale = 12,
		Freunde = 13,
		Unbekannt = 14,
		Race1 = 15,
		Race2 = 16,
		Race3 = 17,
		Race4 = 18,
		Race5 = 19
	}
}