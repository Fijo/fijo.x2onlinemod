using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.XGame.Script.Enums {
	[ModificationNote("plase do not change the indexes - that are the original ones from the x2 script xml files")]
	public enum Rockets {
		MosquitoMissile = 0,
		WaspMissile = 1,
		DragonflyMissile = 2,
		SilkwormMissile = 3,
		HornetMissile = 4,
		UnknownObject = 5,
		UnknownObject1 = 6,
		UnknownObject2 = 7
	}
}