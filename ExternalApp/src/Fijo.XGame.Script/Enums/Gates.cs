using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.XGame.Script.Enums {
	[ModificationNote("plase do not change the indexes - that are the original ones from the x2 script xml files")]
	public enum Gates {
		Gate = 0,
		Gate1 = 1,
		Gate2 = 2,
		Gate3 = 3,
		Gate4 = 4
	}
}