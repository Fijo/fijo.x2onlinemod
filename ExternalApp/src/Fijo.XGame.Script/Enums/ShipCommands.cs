﻿using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.XGame.Script.Enums {
	[Note("in development")]
	[ModificationNote("plase do not change the indexes - that are the original ones from the x2 script xml files")]
	public enum ShipCommands {
		COMMAND_IDLE = 17,
		COMMAND_ACTION = 20,

	}
}