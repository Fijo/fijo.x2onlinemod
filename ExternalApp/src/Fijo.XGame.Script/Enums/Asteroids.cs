using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.XGame.Script.Enums {
	[ModificationNote("plase do not change the indexes - that are the original ones from the x2 script xml files")]
	public enum Asteroids {
		Asteroid = 0,
		Asteroid1 = 1,
		Asteroid2 = 2,
		Asteroid3 = 3,
		Asteroid4 = 4,
		Asteroid5 = 5,
		Asteroid6 = 6,
		Asteroid7 = 7,
		Asteroid8 = 8,
		Asteroid9 = 9
	}
}