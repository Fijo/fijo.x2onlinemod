using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.XGame.Script.Enums {
	[ModificationNote("plase do not change the indexes - that are the original ones from the x2 script xml files")]
	public enum Natural {
		Water = 0,
		Artefacts = 1,
		ArtificialFertilizer = 2,
		BiologicalMicroOrganisms = 3,
		CartographyChips = 4,
		ConstructionEquipment = 5,
		EngineComponents = 6,
		EntertainmentChips = 7,
		FoodRations = 8,
		HandWeapons = 9,
		LuxuryFoodstuffs = 10,
		MedicalEquipment = 11,
		MiningEquipment = 12,
		Nividium = 13,
		RadioactiveWaste = 14,
		TeladianiumPanelling = 15,
		WeaponInterfaceChips = 16,
		Narcotics = 17,
		SuperSlaveChips = 18,
		SpaceflyEggs = 19,
		PirateSidearms = 20,
		Hackerchips = 21,
		MilitaryPersonnel = 22,
		Passengers = 23,
		VeryImportantPassengers = 24
	}
}