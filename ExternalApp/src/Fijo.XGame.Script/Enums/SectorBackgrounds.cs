using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.XGame.Script.Enums {
	[ModificationNote("plase do not change the indexes - that are the original ones from the x2 script xml files")]
	public enum SectorBackgrounds {
		Shield1MW = 0,
		Shield5MW = 1,
		Shield25MW = 2,
		Shield125MW = 3
	}
}