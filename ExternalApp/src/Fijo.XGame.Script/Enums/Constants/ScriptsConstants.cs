namespace Fijo.XGame.Script.Enums.Constants {
	public static class ScriptsConstants {
		public const int IntParam = 4;
		public const int StringParam = 5;
		public const int ShipTypeParam = 9;
		public const int RaceParam = 10;
		public const int VariableParam = 131074;
		public const int ScriptParam = 65536;
		public const int This = -2147483647;
	}
}