using System;

namespace Fijo.XGame.Script.CustomTypeHandlings {
	public interface ICustomTypeHandling {
		Type Type { get; }
		object Handle(object obj);
	}
}