using System;
using FijoCore.Infrastructure.LightContrib.Module.AlgorithmProvider;

namespace Fijo.XGame.Script.CustomTypeHandlings {
	public class CustomTypeHandlingProvider : FallbackInjectionAlgorithmProvider<ICustomTypeHandling, Type> {
		#region Overrides of InjectionAlgorithmProviderBase<ICustomTypeHandling,Type>
		protected override Type KeySelector(ICustomTypeHandling algorithm) {
			return algorithm.Type;
		}
		#endregion
		#region Overrides of FallbackInjectionAlgorithmProvider<ICustomTypeHandling,Type>
		protected override bool IsFallback(ICustomTypeHandling algorithm) {
			return algorithm.Type == typeof(object);
		}
		#endregion
	}
}