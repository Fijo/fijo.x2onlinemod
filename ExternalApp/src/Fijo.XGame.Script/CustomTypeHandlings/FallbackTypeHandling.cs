using System;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Exception;
using FijoCore.Infrastructure.LightContrib.Default.Service.CheckName;

namespace Fijo.XGame.Script.CustomTypeHandlings {
	public class FallbackTypeHandling : ICustomTypeHandling  {
		#region Implementation of ICustomTypeHandling
		public Type Type { get { return typeof (object); } }

		public object Handle(object obj) {
			if(obj is IConvertible && obj is IFormattable) return Convert.ToInt32(obj);
			return obj;
		}
		#endregion
	}
}