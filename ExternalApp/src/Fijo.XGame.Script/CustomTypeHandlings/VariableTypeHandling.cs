using Fijo.XGame.Script.Dto;

namespace Fijo.XGame.Script.CustomTypeHandlings {
	public class VariableTypeHandling : CustomTypeHandling<Variable> {
		#region Overrides of CustomTypeHandling<Variable>
		protected override object Handle(Variable variable) {
			return variable.Id;
		}
		#endregion
	}
}