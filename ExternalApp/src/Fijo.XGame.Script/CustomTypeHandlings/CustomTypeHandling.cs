using System;

namespace Fijo.XGame.Script.CustomTypeHandlings {
	public abstract class CustomTypeHandling<T> : ICustomTypeHandling {
		#region Implementation of ICustomTypeHandling
		public Type Type { get { return typeof (T); } }

		public virtual object Handle(object obj) {
			return Handle((T) obj);
		}

		protected abstract object Handle(T obj);
		#endregion
	}
}