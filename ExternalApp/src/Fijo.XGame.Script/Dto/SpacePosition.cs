using System;
using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using FijoCore.Infrastructure.LightContrib.Default.Service.EqualityHelpers;
using FijoCore.Infrastructure.LightContrib.Extentions.Integer;

namespace Fijo.XGame.Script.Dto {
	[Dto, Serializable]
	public class SpacePosition : Position {
		public readonly Position Rotation;

		public SpacePosition(int x, int y, int z, int rotX, int rotY, int rotZ) : base(x, y, z) {
			Rotation = new Position(rotX, rotY, rotZ);
		}

		#region Equality
		protected bool Equals(SpacePosition other) {
			#region PreCondition
			Debug.Assert(Rotation != null);
			Debug.Assert(other != null);
			Debug.Assert(other.Rotation != null);
			#endregion
			return Equals((Position) other) && Equals(Rotation, other.Rotation);
		}

		public override bool Equals(object obj) {
			return Equality.Equals<SpacePosition>(obj, Equals);
		}

		public override int GetHashCode() {
			#region PreCondition
			Debug.Assert(Rotation != null);
			#endregion
			return base.GetHashCode().GetHashCodeAlgorithm(Rotation.GetHashCode());
		}
		#endregion

	}
}