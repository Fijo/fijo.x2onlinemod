using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using Fijo.XGame.Script.Commands;

namespace Fijo.XGame.Script.Dto {
	[Dto]
	public class Script {
		public readonly string Name;
		public readonly int Version;
		public readonly string Description;
		public IList<Variable> Variables = new List<Variable>();
		public IList<ICommand> Commands = new List<ICommand>();

		public Script(string name, string description, int version) {
			Name = name;
			Description = description;
			Version = version;
		}
	}
}