using System;
using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using FijoCore.Infrastructure.LightContrib.Default.Service.EqualityHelpers;
using FijoCore.Infrastructure.LightContrib.Extentions.Integer;

namespace Fijo.XGame.Script.Dto {
	[Dto, Serializable]
	public class Position {
		public readonly int X;
		public readonly int Y;
		public readonly int Z;

		public Position(int x, int y, int z) {
			X = x;
			Y = y;
			Z = z;
		}

		#region Equality
		protected bool Equals(Position other) {
			#region PreCondition
			Debug.Assert(other != null);
			#endregion
			return X == other.X && Y == other.Y && Z == other.Z;
		}

		public override bool Equals(object obj) {
			return Equality.Equals<Position>(obj, Equals);
		}

		public override int GetHashCode() {
			return X.GetHashCode().GetHashCodeAlgorithm(Y.GetHashCode(), Z.GetHashCode());
		}
		#endregion
	}
}