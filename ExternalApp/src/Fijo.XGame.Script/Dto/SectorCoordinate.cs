using System;
using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using FijoCore.Infrastructure.LightContrib.Default.Service.EqualityHelpers;
using FijoCore.Infrastructure.LightContrib.Extentions.Integer;

namespace Fijo.XGame.Script.Dto {
	[Dto, Serializable]
	public class SectorCoordinate {
		public readonly int X;
		public readonly int Y;

		public SectorCoordinate(int x, int y) {
			X = x;
			Y = y;
		}

		#region Equality
		protected bool Equals(SectorCoordinate other) {
			#region PreCondition
			Debug.Assert(other != null);
			#endregion
			return X == other.X && Y == other.Y;
		}

		public override bool Equals(object obj) {
			return Equality.Equals<SectorCoordinate>(obj, Equals);
		}

		public override int GetHashCode() {
			return X.GetHashCode().GetHashCodeAlgorithm(Y.GetHashCode());
		}
		#endregion
	}
}