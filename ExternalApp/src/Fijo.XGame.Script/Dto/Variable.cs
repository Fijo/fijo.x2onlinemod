using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace Fijo.XGame.Script.Dto {
	[Dto]
	public class Variable {
		public readonly string Name;
		public readonly int Id;
		public Variable(string name, int id) {
			Name = name;
			Id = id;
		}
	}
}