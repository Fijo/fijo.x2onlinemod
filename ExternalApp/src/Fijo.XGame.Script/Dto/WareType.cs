using System;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using Fijo.XGame.Script.Enums;

namespace Fijo.XGame.Script.Dto {
	[Dto, Serializable]
	public class WareType {
		public readonly WareMainType MainType;
		public readonly int SubType;

		public WareType(WareMainType mainType, int subType) {
			MainType = mainType;
			SubType = subType;
		}
	}
}