using System;
using System.Collections.Generic;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Controller;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Provider;
using Fijo.Infrastructure.DesignPattern.Mapping;
using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.Infrastructure.DesignPattern.Stock;
using Fijo.XGame.Properties;
using Fijo.XGame.Script.CustomTypeHandlings;
using Fijo.XGame.Script.Factories;
using Fijo.XGame.Script.Interfaces;
using Fijo.XGame.Script.ScriptParamSerializers;
using Fijo.XGame.Script.Services;
using Fijo.XGame.Script.Store;
using FijoCore.Infrastructure.DependencyInjection.Extentions.IKernel;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Base;
using FijoCore.Infrastructure.LightContrib.Module.MethodeManagers.InterruptibleMethodeManager.Interface;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;

namespace Fijo.X2OnlineMod.Properties {
	public class XGameScriptInjectionModule : ExtendedNinjectModule {

		public override void AddModule(IKernel kernel) {
			kernel.Load(new XGameInjectionModule());
		}

		public override void OnLoad(IKernel kernel) {
			kernel.Bind<IScriptParamSerializer>().To<Int32ScriptParamSerializer>().InSingletonScope();
			kernel.Bind<IScriptParamSerializer>().To<StringScriptParamSerializer>().InSingletonScope();
			kernel.Bind<IAlgorithmProvider<IScriptParamSerializer, Type>>().To<ScriptParamSerializerProvider>().InSingletonScope();
			kernel.Bind<IFinder<IScriptParamSerializer, Type>>().To<Finder<IScriptParamSerializer, Type>>().InSingletonScope();

			kernel.Bind<ICustomTypeHandling>().To<VariableTypeHandling>().InSingletonScope();
			kernel.Bind<ICustomTypeHandling>().To<FallbackTypeHandling>().InSingletonScope();
			kernel.Bind<IAlgorithmProvider<ICustomTypeHandling, Type>>().To<CustomTypeHandlingProvider>().InSingletonScope();
			kernel.Bind<IFinder<ICustomTypeHandling, Type>>().To<Finder<ICustomTypeHandling, Type>>().InSingletonScope();

			kernel.Bind<IScriptSerializer>().To<ScriptSerializer>().InSingletonScope();
			
			kernel.Bind<IScriptService>().To<ScriptService>().InSingletonScope();
			kernel.Bind<IScriptFactory>().To<ScriptFactory>().InSingletonScope();

			kernel.Bind<IScriptStoreService>().To<ScriptStoreService>().InSingletonScope();
			kernel.Bind<IScriptStore>().To<ScriptStore>().InSingletonScope();
		}
	}
}