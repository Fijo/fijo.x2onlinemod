using Fijo.XGame.Script.Dto;
using Fijo.XGame.Script.Interfaces;

namespace Fijo.XGame.Script.Factories {
	public class ScriptFactory : IScriptFactory {
		#region Implementation of IScriptFactory
		public Dto.Script CreateScript(string name, string description, int version) {
			return new Dto.Script(name, description, version);
		}

		public Variable CreateVariable(string name, int id) {
			return new Variable(name, id);
		}
		#endregion
	}
}