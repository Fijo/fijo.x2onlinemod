using System;
using System.Text;

namespace Fijo.XGame.Script.ScriptParamSerializers {
	public interface IScriptParamSerializer {
		Type Type { get; }
		void Serialize(StringBuilder sb, object obj);
	}
}