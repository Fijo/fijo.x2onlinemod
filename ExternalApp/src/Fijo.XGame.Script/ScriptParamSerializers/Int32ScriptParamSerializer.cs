namespace Fijo.XGame.Script.ScriptParamSerializers {
	public class Int32ScriptParamSerializer : SValScriptParamSerializer<int> {
		#region Overrides of SValScriptParamSerializer<int>
		protected override string GetTypeValue() {
			return "int";
		}
		#endregion
	}
}