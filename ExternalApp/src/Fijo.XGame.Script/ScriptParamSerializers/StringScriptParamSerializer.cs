namespace Fijo.XGame.Script.ScriptParamSerializers {
	public class StringScriptParamSerializer : SValScriptParamSerializer<string> {
		#region Overrides of SValScriptParamSerializer<int>
		protected override string GetTypeValue() {
			return "string";
		}
		#endregion
	}
}