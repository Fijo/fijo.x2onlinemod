using System;
using FijoCore.Infrastructure.LightContrib.Module.AlgorithmProvider;

namespace Fijo.XGame.Script.ScriptParamSerializers {
	public class ScriptParamSerializerProvider : InjectionAlgorithmProvider<IScriptParamSerializer, Type> {
		#region Overrides of InjectionAlgorithmProviderBase<IScriptParamSerializer,Type>
		protected override Type KeySelector(IScriptParamSerializer algorithm) {
			return algorithm.Type;
		}
		#endregion
	}
}