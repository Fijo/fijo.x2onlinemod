using System;
using System.Text;

namespace Fijo.XGame.Script.ScriptParamSerializers {
	public abstract class ScriptParamSerializer<T> : IScriptParamSerializer {
		#region Implementation of IScriptParamSerializer
		public Type Type { get { return typeof (T); } }

		public virtual void Serialize(StringBuilder sb, object obj) {
			Serialize(sb, (T) obj);
		}

		public abstract void Serialize(StringBuilder sb, T obj);
		#endregion
	}
}