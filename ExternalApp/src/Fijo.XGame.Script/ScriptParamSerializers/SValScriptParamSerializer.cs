using System.Text;

namespace Fijo.XGame.Script.ScriptParamSerializers {
	public abstract class SValScriptParamSerializer<T> : ScriptParamSerializer<T> {
		private readonly string _firstPart;

		public SValScriptParamSerializer() {
			_firstPart = string.Format("<sval type=\"{0}\" val=\"", GetTypeValue());
		}

		#region Implementation of IScriptParamSerializer
		public override void Serialize(StringBuilder sb, T obj) {
			sb.Append(_firstPart);
			sb.Append(obj);
			sb.Append("\" />");
		}

		protected abstract string GetTypeValue();
		#endregion
	}
}