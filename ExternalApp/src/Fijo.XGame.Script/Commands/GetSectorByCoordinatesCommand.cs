using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.XGame.Script.Dto;
using Fijo.XGame.Script.Enums;
using Fijo.XGame.Script.Enums.Constants;

namespace Fijo.XGame.Script.Commands {
	public class GetSectorByCoordinatesCommand : ICommand<Variable, SectorCoordinate> {
		[About("SetToVariable")]
		public Variable Arg1 { get; set; }
		[About("SectorCoordinate")]
		public SectorCoordinate Arg2 { get; set; }

		#region Implementation of ICommand
		public IEnumerable<object> ToObjects() {
			yield return ScriptMethodes.SectorByPos;
			yield return Arg1;
			yield return ScriptsConstants.IntParam;
			yield return Arg2.X;
			yield return ScriptsConstants.IntParam;
			yield return Arg2.Y;
		}
		#endregion
	}
}