using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.XGame.Script.Dto;
using Fijo.XGame.Script.Enums;
using Fijo.XGame.Script.Enums.Constants;

namespace Fijo.XGame.Script.Commands {
	public class WriteToPlayerLogbookCommand : ICommand<Variable> {
		[About("VariableToWrite")]
		public Variable Arg1 { get; set; }

		#region Implementation of ICommand
		public IEnumerable<object> ToObjects() {
			yield return ScriptMethodes.WriteToPlayerLogbook;
			yield return ScriptsConstants.VariableParam;
			yield return Arg1;
		}
		#endregion
	}
}