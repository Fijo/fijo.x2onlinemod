using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.XGame.Script.Dto;
using Fijo.XGame.Script.Enums;
using Fijo.XGame.Script.Enums.Constants;

namespace Fijo.XGame.Script.Commands {
	public class SetNameCommand : ICommand<Variable, string> {
		[About("Source")]
		public Variable Arg1 { get; set; }
		[About("Name")]
		public string Arg2 { get; set; }

		#region Implementation of ICommand
		public IEnumerable<object> ToObjects() {
			yield return ScriptMethodes.SetName;
			yield return 131074;
			yield return Arg1;
			yield return ScriptsConstants.StringParam;
			yield return Arg2;
		}
		#endregion
	}
}