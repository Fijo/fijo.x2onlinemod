using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.XGame.Script.Dto;
using Fijo.XGame.Script.Enums;
using Fijo.XGame.Script.Enums.Constants;

namespace Fijo.XGame.Script.Commands {
	public class CreateShipCommand : ICommand<Variable, ShipType, Race, Variable, Position> {
		[About("SetToVariable")]
		public Variable Arg1 { get; set; }
		[About("ShipType")]
		public ShipType Arg2 { get; set; }
		[About("Race")]
		public Race Arg3 { get; set; }
		[About("SectorVariable")]
		public Variable Arg4 { get; set; }
		[About("Position")]
		public Position Arg5 { get; set; }

		#region Implementation of ICommand
		public IEnumerable<object> ToObjects() {
			yield return ScriptMethodes.CreateShip;
			yield return Arg1;
			yield return ScriptsConstants.ShipTypeParam;
			yield return Arg2;
			yield return ScriptsConstants.RaceParam;
			yield return Arg3;
			yield return ScriptsConstants.VariableParam;
			yield return Arg4;
			yield return ScriptsConstants.IntParam;
			yield return Arg5.X;
			yield return ScriptsConstants.IntParam;
			yield return Arg5.Y;
			yield return ScriptsConstants.IntParam;
			yield return Arg5.Z;
		}
		#endregion
	}
}