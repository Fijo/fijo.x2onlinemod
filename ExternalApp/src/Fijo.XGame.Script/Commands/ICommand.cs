using System.Collections.Generic;

namespace Fijo.XGame.Script.Commands {
	public interface ICommand {
		IEnumerable<object> ToObjects();
	}

	public interface ICommand<T> : ICommand {
		T Arg1 { get; set; }
	}

	public interface ICommand<T1, T2> : ICommand<T1> {
		T2 Arg2 { get; set; }
	}

	public interface ICommand<T1, T2, T3> : ICommand<T1, T2> {
		T3 Arg3 { get; set; }
	}

	public interface ICommand<T1, T2, T3, T4> : ICommand<T1, T2, T3> {
		T4 Arg4 { get; set; }
	}

	public interface ICommand<T1, T2, T3, T4, T5> : ICommand<T1, T2, T3, T4> {
		T5 Arg5 { get; set; }
	}

	public interface ICommand<T1, T2, T3, T4, T5, T6> : ICommand<T1, T2, T3, T4, T5> {
		T6 Arg6 { get; set; }
	}

	public interface ICommand<T1, T2, T3, T4, T5, T6, T7> : ICommand<T1, T2, T3, T4, T5, T6> {
		T7 Arg7 { get; set; }
	}

	public interface ICommand<T1, T2, T3, T4, T5, T6, T7, T8> : ICommand<T1, T2, T3, T4, T5, T6, T7> {
		T8 Arg8 { get; set; }
	}

	public interface ICommand<T1, T2, T3, T4, T5, T6, T7, T8, T9> : ICommand<T1, T2, T3, T4, T5, T6, T7, T8> {
		T9 Arg9 { get; set; }
	}

	public interface ICommand<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> : ICommand<T1, T2, T3, T4, T5, T6, T7, T8, T9> {
		T10 Arg10 { get; set; }
	}

	public interface ICommand<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> : ICommand<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> {
		T11 Arg11 { get; set; }
	}

	public interface ICommand<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> : ICommand<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> {
		T12 Arg12 { get; set; }
	}
}