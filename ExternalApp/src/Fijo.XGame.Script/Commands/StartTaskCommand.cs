using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.XGame.Script.Dto;
using Fijo.XGame.Script.Enums;
using Fijo.XGame.Script.Enums.Constants;

namespace Fijo.XGame.Script.Commands {
	public class StartTaskCommand : ICommand<Variable, string, Variable, int, Variable, Variable, Variable, Variable, Variable> {
		[About("SourceVariable")]
		public Variable Arg1 { get; set; }
		[About("ScriptName")]
		public string Arg2 { get; set; }
		[About("TaskIdVariable"), Note("integer")]
		public Variable Arg3 { get; set; }
		[About("Priority")]
		public int Arg4 { get; set; }
		[About("Arg1")]
		public Variable Arg5 { get; set; }
		[About("Arg2")]
		public Variable Arg6 { get; set; }
		[About("Arg3")]
		public Variable Arg7 { get; set; }
		[About("Arg4")]
		public Variable Arg8 { get; set; }
		[About("Arg5")]
		public Variable Arg9 { get; set; }

		#region Implementation of ICommand
		public IEnumerable<object> ToObjects() {
			yield return ScriptMethodes.StartTask;
			yield return ScriptsConstants.VariableParam;
			yield return Arg1;
			yield return Arg2;
			yield return ScriptsConstants.VariableParam;
			yield return Arg3;
			yield return ScriptsConstants.IntParam;
			yield return Arg4;
			foreach(var arg in new[] {Arg5, Arg6, Arg7, Arg8, Arg9}) {
				if(arg == null) {
					yield return 0;
					yield return 0;
				}
				else {
					yield return ScriptsConstants.VariableParam;
					yield return arg;
				}
			}
		}
		#endregion
	}
}