using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.XGame.Script.Dto;
using Fijo.XGame.Script.Enums;
using Fijo.XGame.Script.Enums.Constants;

namespace Fijo.XGame.Script.Commands {
	[About("GetWareByMainAndSubType")]
	public class GetWareByTypes : ICommand<Variable, WareType> {
		[About("SetToVariable")]
		public Variable Arg1 { get; set; }
		[About("WareType")]
		public WareType Arg2 { get; set; }

		#region Implementation of ICommand
		public IEnumerable<object> ToObjects() {
			yield return ScriptMethodes.GetWareByMainAndSubType;
			yield return Arg1;
			yield return ScriptsConstants.IntParam;
			yield return Arg2.MainType;
			yield return ScriptsConstants.IntParam;
			yield return Arg2.SubType;
		}
		#endregion
	}
}