using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.XGame.Script.Dto;
using Fijo.XGame.Script.Enums;
using Fijo.XGame.Script.Enums.Constants;

namespace Fijo.XGame.Script.Commands {
	public class SetRotationCommand : ICommand<Variable, int, int, int> {
		[About("Source")]
		public Variable Arg1 { get; set; }
		[About("Alpha")]
		public int Arg2 { get; set; }
		[About("Beta")]
		public int Arg3 { get; set; }
		[About("Gamma")]
		public int Arg4 { get; set; }

		#region Implementation of ICommand
		public IEnumerable<object> ToObjects() {
			yield return ScriptMethodes.SetRotation;
			yield return 131074;
			yield return Arg1;
			yield return ScriptsConstants.IntParam;
			yield return Arg2;
			yield return ScriptsConstants.IntParam;
			yield return Arg3;
			yield return ScriptsConstants.IntParam;
			yield return Arg4;
		}
		#endregion
	}
}