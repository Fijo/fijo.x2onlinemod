using System.Collections.Generic;
using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.XGame.Script.Dto;
using Fijo.XGame.Script.Enums;
using Fijo.XGame.Script.Enums.Constants;

namespace Fijo.XGame.Script.Commands {
	public class RandomValueCommand : ICommand<Variable, int, int> {
		[About("SetToVariable")]
		public Variable Arg1 { get; set; }
		[About("From")]
		public int Arg2 { get; set; }
		[About("To"), Note("To -1")]
		public int Arg3 { get; set; }

		public IEnumerable<object> ToObjects() {
			#region PreCondition
			Debug.Assert(Arg2 <= Arg3 - 1);
			#endregion
			yield return ScriptMethodes.RandomValueFromTo;
			yield return Arg1;
			yield return ScriptsConstants.IntParam;
			yield return Arg2;
			yield return ScriptsConstants.IntParam;
			yield return Arg3;
		}
	}
}