using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.XGame.Script.Dto;
using Fijo.XGame.Script.Enums;
using Fijo.XGame.Script.Enums.Constants;

namespace Fijo.XGame.Script.Commands {
	public class ReturnCommand : ICommand<Variable> {
		[About("VariableToReturn")]
		public Variable Arg1 { get; set; }

		#region Implementation of ICommand
		public IEnumerable<object> ToObjects() {
			yield return ScriptMethodes.Return;
			yield return ScriptsConstants.VariableParam;
			yield return Arg1;
		}
		#endregion
	}
}