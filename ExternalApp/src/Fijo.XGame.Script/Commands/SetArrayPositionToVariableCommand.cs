using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.XGame.Script.Dto;
using Fijo.XGame.Script.Enums;
using Fijo.XGame.Script.Enums.Constants;

namespace Fijo.XGame.Script.Commands {
	public class SetArrayPositionToVariableCommand : ICommand<Variable, int, Variable> {
		[About("ArrayVariable")]
		public Variable Arg1 { get; set; }
		[About("Position")]
		public int Arg2 { get; set; }
		[About("Variable")]
		public Variable Arg3 { get; set; }

		#region Implementation of ICommand
		public IEnumerable<object> ToObjects() {
			yield return ScriptMethodes.SetArrayPosition;
			yield return ScriptsConstants.VariableParam;
			yield return Arg1;
			yield return ScriptsConstants.IntParam;
			yield return Arg2;
			yield return ScriptsConstants.VariableParam;
			yield return Arg3;
		}
		#endregion
	}
}