using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.XGame.Script.Enums;
using Fijo.XGame.Script.Enums.Constants;

namespace Fijo.XGame.Script.Commands {
	public class WaitCommand : ICommand<int> {
		[About("timeInMilliseconds")]
		public int Arg1 { get; set; }

		#region Implementation of ICommand
		public IEnumerable<object> ToObjects() {
			yield return ScriptMethodes.Wait;
			yield return ScriptsConstants.This;
			yield return ScriptsConstants.IntParam;
			yield return Arg1;
		}
		#endregion
	}
}