using System.Diagnostics;
using System.Linq;
using Fijo.XGame.Script.Commands;
using Fijo.XGame.Script.Dto;
using Fijo.XGame.Script.Interfaces;

namespace Fijo.XGame.Script.Services {
	public class ScriptService : IScriptService {
		private readonly IScriptFactory _scriptFactory;

		public ScriptService(IScriptFactory scriptFactory) {
			_scriptFactory = scriptFactory;
		}

		#region Implementation of IScriptService
		public void AddCommand(Dto.Script script, ICommand command) {
			script.Commands.Add(command);
		}

		public Variable DefineVariable(Dto.Script script, string name) {
			var variables = script.Variables;
			lock(variables) {
				#region PreCondition
				Debug.Assert(!variables.Select(x => x.Name).Contains(name), string.Format("A variable with the name �{0}� is already defined", name));
				#endregion
				var variable = _scriptFactory.CreateVariable(name, variables.Count);
				variables.Add(variable);
				return variable;
			}
		}
		#endregion
	}
}