using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Controller;
using Fijo.XGame.Script.Commands;
using Fijo.XGame.Script.CustomTypeHandlings;
using Fijo.XGame.Script.Dto;
using Fijo.XGame.Script.Interfaces;
using Fijo.XGame.Script.ScriptParamSerializers;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;

namespace Fijo.XGame.Script.Services {
	public class ScriptSerializer : IScriptSerializer {
		private readonly IFinder<IScriptParamSerializer, Type> _paramSerializerFinder;
		private readonly IFinder<ICustomTypeHandling, Type> _customTypeHandlingFinder;

		public ScriptSerializer(IFinder<IScriptParamSerializer, Type> paramSerializerFinder, IFinder<ICustomTypeHandling, Type> customTypeHandlingFinder) {
			_paramSerializerFinder = paramSerializerFinder;
			_customTypeHandlingFinder = customTypeHandlingFinder;
		}

		#region Implementation of IScriptSerializer
		public string Serialize(Dto.Script script) {
			var xScript = new StringBuilder();
			InternalSerialize(xScript, script);
			return xScript.ToString();
		}

		private void InternalSerialize(StringBuilder xScript, Dto.Script script) {
			Debug.Assert(script.Version >= 0);

			var name = script.Name;
			var variables = script.Variables;
			var commands = script.Commands;
			xScript.Append("<?xml version=\"1.0\" standalone=\"yes\" ?>\n");
			xScript.Append("<?xml-stylesheet href=\"x2script.xsl\" type=\"text/xsl\" ?>\n");
			xScript.Append("<script>\n");
			xScript.Append("<name>");
			xScript.Append(name);
			xScript.Append("</name>\n");
			xScript.Append("<version>");
			xScript.Append(script.Version);
			xScript.Append("</version>\n");
			xScript.Append("<engineversion>25</engineversion>\n");
			xScript.Append("<description>");
			xScript.Append(script.Description);
			xScript.Append("</description>\n");
			xScript.Append("<arguments>\n</arguments>\n");
			xScript.Append("<sourcetext>\n</sourcetext>\n");
			xScript.Append("<codearray>\n");
			xScript.Append("<sval type=\"array\" size=\"10\">");
			xScript.Append("<sval type=\"string\" val=\"");
			xScript.Append(name);
			xScript.Append("\" />");
			xScript.Append("<sval type=\"int\" val=\"25\" />");
			xScript.Append(string.Format("<sval type=\"string\" val=\"{0}\" />", script.Description));
			xScript.Append(string.Format("<sval type=\"int\" val=\"{0}\" />", script.Version));
			xScript.Append("<sval type=\"int\" val=\"0\" />");

			AddVariablesHeaders(variables, xScript);

			OpenArray(xScript, commands.Count + 1);
			AddCommands(xScript, commands);
			AddReturnArray(xScript);
			CloseArray(xScript);

			xScript.Append("<sval type=\"int\" val=\"0\" />");
			xScript.Append("<sval type=\"int\" val=\"0\" />");
			xScript.Append("<sval type=\"int\" val=\"0\" />");
			xScript.Append("</sval>\n");
			xScript.Append("</codearray>\n");
			xScript.Append("</script>");
		}

		private void OpenArray(StringBuilder xScript, int size) {
			xScript.Append(string.Format("<sval type=\"array\" size=\"{0}\">", size));
		}

		private void CloseArray(StringBuilder xScript) {
			xScript.Append("</sval>");
		}

		private void AddReturnArray(StringBuilder xScript) {
			OpenArray(xScript, 3);
			xScript.Append("<sval type=\"int\" val=\"103\" />");
			xScript.Append("<sval type=\"int\" val=\"0\" />");
			xScript.Append("<sval type=\"int\" val=\"0\" />");
			CloseArray(xScript);
		}

		private void AddCommands(StringBuilder xScript, IEnumerable<ICommand> commands) {
			foreach (var command in commands) {
				AddCommand(xScript, command);
			}
		}

		private void AddCommand(StringBuilder xScript, ICommand command) {
			var objects = command.ToObjects().Execute();
			OpenArray(xScript, objects.Count);
			AddCommandContent(xScript, objects);
			CloseArray(xScript);
		}

		private void AddCommandContent(StringBuilder xScript, ICollection<object> objects) {
			foreach (var param in objects) {
				SerializeParam(xScript, GetConverted(param));
			}
		}

		private void SerializeParam(StringBuilder xScript, object obj) {
			_paramSerializerFinder.Get(obj.GetType()).Serialize(xScript, obj);
		}

		private object GetConverted(object param) {
			var handling = _customTypeHandlingFinder.Get(param.GetType());
			var obj = handling.Handle(param);
			return obj;
		}

		private void AddVariablesHeaders(IList<Variable> variables, StringBuilder xScript) {
			OpenArray(xScript, variables.Count);

			foreach (var variableName in variables.Select(x => x.Name)) {
				xScript.Append("<sval type=\"string\" val=\"");
				xScript.Append(variableName);
				xScript.Append("\" />");
			}

			CloseArray(xScript);
		}
		#endregion
	}
}
