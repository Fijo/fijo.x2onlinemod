using Fijo.XGame.Script.Interfaces;
using JetBrains.Annotations;

namespace Fijo.XGame.Script.Services {
	[UsedImplicitly]
	public class ScriptStoreService : IScriptStoreService {
		private readonly IScriptSerializer _scriptSerializer;
		private readonly IScriptStore _scriptStore;

		public ScriptStoreService(IScriptSerializer scriptSerializer, IScriptStore scriptStore) {
			_scriptSerializer = scriptSerializer;
			_scriptStore = scriptStore;
		}

		public void Store(Dto.Script script) {
			var source =_scriptSerializer.Serialize(script);
			_scriptStore.Store(script.Name, source);
		}
	}
}