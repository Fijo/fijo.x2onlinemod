﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fijo.X2OnlineMod.Interfaces;
using Fijo.X2OnlineMod.Properties;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;

namespace Fijo.X2OnlineMod.ClientApp {
	class Program {
		static void Main(string[] args) {
			Console.WriteLine(@"Copyright (C) 2012 Jonas Fischer <fijo.com@googlemail.com>
This program comes with ABSOLUTELY NO WARRANTY;
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.");
			new InternalInitKernel().Init();
			Kernel.Resolve<IGameService>().Start();
		}
	}
}
